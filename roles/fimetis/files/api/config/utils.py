
def map_pandas_type_to_logstash_type(pandas_type: str):
    if pandas_type == 'int64':
        return 'integer'
    elif pandas_type == 'float64':
        return 'float'
    elif pandas_type == 'datetime64':
        return 'datetime'
    return 'text'
    