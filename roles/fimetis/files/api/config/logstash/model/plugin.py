from .attribute import Attribute

plugin_empty_str = '''\
  {plugin} {{ }}'''

plugin_str = '''\
  {plugin} {{
{attributes}
  }}'''

class Plugin:

    def __init__(self, name: str) -> None:
        self.name = name
        self.attributes = []

    def add_attribute(self, attribute: Attribute):
        self.attributes.append(attribute)

    def __str__(self) -> str:
        if len(self.attributes) == 0:
            return plugin_empty_str.format(plugin= self.name)
        return plugin_str.format(plugin = self.name, attributes = '\n'.join(map(str, self.attributes)))
