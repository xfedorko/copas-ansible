from .value import Value

attribute_str = '    {name} => {value}'

class Attribute:

    def __init__(self, name: str, value: Value) -> None:
        self.name = name
        self.value = value     

    def __str__(self) -> str:
        return attribute_str.format(name = self.name, value = self.value)
