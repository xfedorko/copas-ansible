
from .plugin import Plugin


plugin_section_str = '''\
{plugin_section} {{ 
{plugins}
}}
'''

plugin_section_empty_str = '''\
{plugin_section} {{  }}
'''

class PluginSection:

    def __init__(self, name: str) -> None:
        self.name = name
        self.plugins = []

    def add_plugin(self, plugin: Plugin):
        self.plugins.append(plugin)

    def __str__(self) -> str:
        if len(self.plugins) == 0:
            return plugin_section_empty_str.format(plugin_section = self.name)
        return plugin_section_str.format(plugin_section = self.name, plugins = '\n'.join(map(str, self.plugins)))
