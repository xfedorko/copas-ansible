from typing import Dict, List

array_empty_str = '[ ]'
hash_empty_str = '{{ }}'
array_str = '''[
      {value}    
    ]'''
hash_str = '''{{
      {value}
    }}'''
string_str = '"{value}"'
number_str = '{value}'

class Value:

    def __init__(self, value: str or int or float or bool or Dict or List) -> None:
        self.value = value     

    def __str__(self) -> str:
        if isinstance(self.value, bool):
            return 'true' if self.value else 'false'
        elif isinstance(self.value, int) or isinstance(self.value, float):
            return number_str.format(value=self.value)
        elif isinstance(self.value, str):
            return string_str.format(value=self.value)
        elif isinstance(self.value, dict):
            if len(self.value) == 0: return '{{}}'
            return hash_str.format(value='\n      '.join([f'{Value(k)} => {Value(v)}' for k, v in self.value.items()]))
        elif isinstance(self.value, list):
            if len(self.value) == 0: return '[]'
            return array_str.format(value=',\n      '.join([f'{Value(v)}' for v in self.value]))