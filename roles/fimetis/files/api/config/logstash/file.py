from typing import List
from api.config.logstash.model import Plugin, Attribute, Value


def file_plugin(files: List[str]):
    file_plugin = Plugin('file')
    file_plugin.add_attribute(Attribute('path', Value(files)))
    file_plugin.add_attribute(Attribute('exit_after_read', Value(True)))
    file_plugin.add_attribute(Attribute('start_position', Value('beginning')))
    file_plugin.add_attribute(Attribute('sincedb_path', Value('/dev/null')))
    file_plugin.add_attribute(Attribute('mode', Value('read')))
    file_plugin.add_attribute(Attribute('file_completed_action', Value('log')))
    file_plugin.add_attribute(Attribute('file_completed_log_path', Value('/dev/null')))
    return file_plugin