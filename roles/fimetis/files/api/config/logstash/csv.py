from typing import Dict, List
from api.config.logstash.model import Plugin, Attribute, Value


def csv_plugin(separator: str, columns: List[str], convert: Dict[str, str]):
    csv_plugin = Plugin('csv')
    csv_plugin.add_attribute(Attribute('separator', Value(separator)))
    csv_plugin.add_attribute(Attribute('columns', Value(columns)))
    csv_plugin.add_attribute(Attribute('convert', Value(convert)))
    return csv_plugin