from api.config.logstash.model import Plugin, Attribute, Value


def elasticsearch_plugin(host: str, index: str):
    elasticsearch_plugin = Plugin('elasticsearch')
    elasticsearch_plugin.add_attribute(Attribute('hosts', Value(host)))
    elasticsearch_plugin.add_attribute(Attribute('index', Value(index)))
    return elasticsearch_plugin