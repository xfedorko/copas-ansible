from api.config.logstash.model import PluginSection
from api.config.logstash.csv import csv_plugin
from api.config.logstash.elasticsearch import elasticsearch_plugin
from api.config.utils import map_pandas_type_to_logstash_type
from flask import request, current_app as app
from config import ELASTICSEARCH_URL
import pandas as pd
from .blueprint import config_blueprint

@config_blueprint.route('generate', methods=('GET',))
def generate_config_contents():
    template_key = request.args.get('key', None)
    paths = request.args.getlist('path', None)

    if template_key is None or len(template_key) == 0:
        return { 'error': 'no template key body provided' }, 400
    
    if paths is None or len(paths) == 0:
        return { 'error': 'no file paths provided' }, 400
    
    if template_key not in { 'editor.default', 'editor.filter' }:
        return { 'error': 'invalid template key provided' }, 400
    
    path = paths[0]

    try:
        df = pd.read_csv(path)
        separator = ','
        columns = [str(c) for c in df.columns]
        convert = {
            c: type for c, t in zip(columns, df.dtypes) if (type := map_pandas_type_to_logstash_type(str(t))) in \
                  { 'integer', 'float', 'datetime' }
        }

        filter_section = PluginSection('filter')
        filter_section.add_plugin(csv_plugin(separator, columns, convert))

        if template_key == 'editor.default':
            output_section = PluginSection('output')
            output_section.add_plugin(elasticsearch_plugin(ELASTICSEARCH_URL, 'copas'))
            # contents = add_output_plugin_section(contents, ELASTICSEARCH_URL, 'copas')
            return {
                'editor.default.logstash': f'{filter_section}\n{output_section}'
            }
        return {
            'editor.filter.logstash': str(filter_section),
            'editor.filter.index': 'copas'
        }
    except:
        app.logger.error('failed to generate logstash configuration', exc_info=True )
        return { 'error': 'failed to generate logstash configuration' }, 400