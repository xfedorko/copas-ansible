from .utils import run_logstash_command
from api.config.logstash.elasticsearch import elasticsearch_plugin
from api.config.logstash.file import file_plugin
from api.config.logstash.model import PluginSection
from flask import request, current_app as app
from .blueprint import import_blueprint
from config import ELASTICSEARCH_URL

@import_blueprint.route('', methods=('POST',))
def import_files():
    import_data = request.json

    if import_data is None:
        return { 'error': 'no json body provided' }, 400

    if 'files' not in import_data or 'config' not in import_data:
        return { 'error': 'invalid input format provided' }, 400

    files =  import_data['files']
    config = import_data['config']
    
    if 'editor.default.logstash' not in config and 'editor.filter.logstash' not in config:
        return { 'error': 'invalid configuration provided' }, 400

    provided_logstash_config = config['editor.default.logstash'] if 'editor.default.logstash' in config else config['editor.filter.logstash']
    
    input_section = PluginSection('input')
    input_section.add_plugin(file_plugin(files))
    
    if 'editor.filter.index' in config:
        output_section = PluginSection('output')
        output_section.add_plugin(elasticsearch_plugin(ELASTICSEARCH_URL, config['editor.filter.index']))
        logstash_config = str(input_section) + provided_logstash_config + str(output_section)
    else:
        logstash_config = str(input_section) + provided_logstash_config

    app.logger.info('''import to logstash 

files: 
%s

config:
%s

''', files, logstash_config)

    logstash_result = run_logstash_command(logstash_config)
    if 'error' in logstash_result:
        app.logger.error('logstash import failed - %s', logstash_result['error'])
        return { 'error': logstash_result['error'] }, 500

    app.logger.info('logstash import succeeded')

    return {}
