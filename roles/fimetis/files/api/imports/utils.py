import subprocess
import time
from api.status.elasticsearch import get_elasticsearch_status
from config import LOGSTASH_PATH

def run_logstash_command(config: str):
    elasticsearch = get_elasticsearch_status()
    if elasticsearch['status'] != 'up':
        return { 'error': 'elasticsearch seems to be unreachable' }
    logstash_process = subprocess.Popen([LOGSTASH_PATH, "-e", config])
    seconds_running = 0
    while True:
        return_code = logstash_process.poll()
        if return_code is not None:
            if return_code == 0:
                return { }
            return { 'error': f'logstash returned code {return_code}' }
        time.sleep(1)
        seconds_running += 1
        if seconds_running % 15 == 0:
            elasticsearch = get_elasticsearch_status()
            if elasticsearch['status'] != 'up':
                logstash_process.kill()
                return { 'error': 'elasticsearch seems to be unreachable' }
