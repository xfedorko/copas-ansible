

from api.action.delete_indices import delete_indices
from api.action.delete_indices import enable_delete
from api.status import get_elasticsearch_status
from flask import request, current_app as app
from .blueprint import action_blueprint

@action_blueprint.route('', methods=('POST',))
def execute_action():
    action = request.json

    if action is None:
        return { 'error': 'no json body provided' }, 400

    if 'key' not in action:
        return { 'error': 'missing property \'key\'' }, 400
    
    action_key = action['key']

    if action_key != 'clear-data':
        app.logger.error('unknown action with key \'%s\'', action_key)
        return { 'error': f'unknown action with key\'{action_key}\'' }, 400

    app.logger.info('executing action \'%s\'', action_key)
    
    elasticsearch_status = get_elasticsearch_status()
    
    if elasticsearch_status['status'] == 'down':
        return { 'error': f'elasticsearch seems to be unreachable' }, 503

    res = enable_delete()
    
    if 'error' in res:
        return res, 503

    res = delete_indices()

    if 'error' in res:
        return res, 503
    
    app.logger.info('successfully completed action \'%s\'', action_key)
    
    return { 'message': 'successfully removed elasticsearch indices' }


