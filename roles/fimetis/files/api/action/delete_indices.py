import requests
from config import ELASTICSEARCH_URL
from flask import current_app as app

def enable_delete():
    try:
        response = requests.put(f'{ELASTICSEARCH_URL}/_cluster/settings', 
        headers={
            'kbn-xsrf': 'reporting'
        },
        json={
            'transient': {
                'action.destructive_requires_name': False
            }
        })

        if response is None or 'acknowledged' not in (json := response.json()) or not json['acknowledged']:
            app.logger.error('can\'t update elasticsearch delete settings')
            return { 'error': 'can\'t update elasticsearch delete settings' }
        return { }
    except:
        app.logger.error('can\'t update elasticsearch delete settings', exc_info=True)
        return { 'error': 'can\'t update elasticsearch delete settings' }

def delete_indices():
    try:
        requests.delete(f'{ELASTICSEARCH_URL}/_all', headers={
            'kbn-xsrf': 'reporting'
        })
        return { }
    except:
        app.logger.error('can\'t delete indices', exc_info=True)
        return { 'error': 'can\'t delete indices' }