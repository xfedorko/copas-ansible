from .elasticsearch import get_elasticsearch_status
from .blueprint import status_blueprint

@status_blueprint.route('', methods=('GET',))
def get_status():
    return {
        'services': [
            get_elasticsearch_status()
        ]
    }
