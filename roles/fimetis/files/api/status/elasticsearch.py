import requests
from api.status.utils import convert_size, service_info_item
from config import ELASTICSEARCH_URL


def get_elasticsearch_status():
    try:
        res = requests.get(f'{ELASTICSEARCH_URL}/_cluster/health')
        json = res.json()
        status = 'up' if json['status'] == 'green' or json['status'] == 'yellow' else 'down'

        info = [ ]

        if status == 'up':
            if get_elasticsearch_status.elastic_version is None:
                get_elasticsearch_status.elastic_version = requests.get(ELASTICSEARCH_URL).json()['version']['number']
            info.append(service_info_item('Version', get_elasticsearch_status.elastic_version))
            res = requests.get(f'{ELASTICSEARCH_URL}/_nodes/stats/jvm').json()
            jvm = next(iter(res['nodes'].items()))[1]['jvm']
            info.append(service_info_item('Available memory', convert_size(jvm['mem']['heap_max_in_bytes'])))
            info.append(service_info_item('Used memory', convert_size(jvm['mem']['heap_used_in_bytes'])))

        info.append(service_info_item('Active shards', int(json['active_shards'])))
        info.append(service_info_item('Unassigned shards', int(json['unassigned_shards'])))
    except:
        status = 'down'
        info = []
    return {
        'name': 'elasticsearch',
        'status': status,
        'info': info
    }

get_elasticsearch_status.elastic_version = None # cache