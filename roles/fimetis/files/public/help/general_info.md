## General info
The Fimetis tool enables the examination of file metadata (timestamps, ownership, etc.) and streamlines the processing of large volumes of such data. Using the visual controls, Fimetis makes it straightforward to navigate through the dataset. The analysis can also benefit from a set of pre-defined configurations identifying common file patterns and operations (like crontab rules, files with weak permissions, signs of compilation on the spot, etc.). The tool also provides automatic detection of files likely connected to the investigation based on their metadata values.

Basic concepts are described in the following paper:

* M. Beran, F. Hrdina, D. Kouřil, R. Ošlejšek, K. Zákopčanová. Exploratory Analysis of File System Metadata for Rapid Investigation of Security Incidents. In 2020 IEEE Symposium on Visualization for Cyber Security (VizSec). doi:10.1109/VizSec51108.2020.00008.

The original repository of the standalone application can be found at https://github.com/CSIRT-MU/fimetis.