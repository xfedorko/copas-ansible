## Obecné info
Nástroj Fimetis umožňuje prohlížení souborových metadat (časové razítko, vlastnictví atd.) a usnadňuje zpracování velkých objemů takových dat. Pomocí vizuálních ovládacích prvků Fimetis usnadňuje navigaci v datasetu. Analýza může také využít sadu předdefinovaných konfigurací identifikujících běžné vzory a operace se soubory (jako pravidla crontabu, soubory se slabými oprávněními, znaky kompilace na místě atd.). Nástroj také poskytuje automatické detekce souborů pravděpodobně spojených s vyšetřováním na základě hodnot jejich metadat.

Základní koncepty jsou popsány v následujícím článku:

* M. Beran, F. Hrdina, D. Kouřil, R. Ošlejšek, K. Zákopčanová. Exploratory Analysis of File System Metadata for Rapid Investigation of Security Incidents. In 2020 IEEE Symposium on Visualization for Cyber Security (VizSec). doi:10.1109/VizSec51108.2020.00008.

Původní repozitář samostatné aplikace lze najít na adrese https://github.com/CSIRT-MU/fimetis.