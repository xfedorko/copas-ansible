from flask import Flask, current_app as app, g
import requests

class AnalysisSession(requests.Session):

    def prepare_request(self, request):
        accept_language = request.headers.get('Accept-Language')

        if accept_language:
            request.headers['Accept-Language'] = accept_language

        return super().prepare_request(request)

    def request(self, method, url, *args, **kwargs):
        if method == 'POST':
            kwargs.setdefault('headers', {})['Content-Type'] = 'application/json'
        return super().request(method, url, *args, **kwargs)


def initialize_analysis_session(app: Flask):
    analysis_session = AnalysisSession()

    analysis_session.headers.update({
        'Accept': 'application/json'
    })

    analysis_session.base_url = app.config['COPAS']['analysis']['api']['url']

    return analysis_session


def get_analysis_session() -> AnalysisSession:
    if 'analysis_session' not in g:
        g.analysis_session = initialize_analysis_session(app)
    return g.analysis_session