

import json
import os
from flask import Flask
import requests
from app.db.get import get_db
from app.core.watchdog.scheduler import watchdog_check
from app.core.data.watch import insert_watch_item


def test_success(app: Flask, temp_dir_1, mocker):
    expected_response = requests.Response()
    expected_response.status_code = 200
    expected_response.json = lambda : {}
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response)

    # register watch item
    insert_watch_item(temp_dir_1, json.dumps({'config.key1': 'val'}), template_contents_id = 1)

    # add a file
    with open(os.path.join(temp_dir_1, 'new_file.txt'), 'w') as f:
        f.write('something')
    
    watchdog_check(app)

    db = get_db()
    import_items = db.execute('SELECT origin, status FROM import_item').fetchall()

    # import stored to history
    assert len(import_items) == 1
    origin, status = import_items[0]
    assert origin == 'watchdog'
    assert status == 'success'

def test_failure(app: Flask, temp_dir_1, mocker):
    expected_response = requests.Response()
    expected_response.status_code = 500
    expected_response.json = lambda : { 'error': 'an issue occured' }
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response)

    # register watch item
    insert_watch_item(temp_dir_1, json.dumps({'config.key1': 'val'}), template_contents_id = 1)

    # add a file
    with open(os.path.join(temp_dir_1, 'new_file.txt'), 'w') as f:
        f.write('something')
    
    watchdog_check(app)

    db = get_db()
    import_items = db.execute('SELECT origin, status FROM import_item').fetchall()

    # import stored to history
    assert len(import_items) == 1
    origin, status = import_items[0]
    assert origin == 'watchdog'
    assert status == 'failure'
