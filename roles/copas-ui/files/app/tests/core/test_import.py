

import os
import requests
from app.core.imports import perform_import
from app.db.get import get_db


def test_success(app, temp_dir_1, mocker):
    expected_response = requests.Response()
    expected_response.status_code = 200
    expected_response.json = lambda : {}
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response)
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    file2 = os.path.join(temp_dir_1, 'file2.txt')
    res, status = perform_import(
        [file1, file2],
        [],
        { 'config.key1': 'test' },
        1,
        'manual'
    )

    assert status == 200
    assert 'error' not in res

    db = get_db()

    cursor = db.execute('SELECT origin, status FROM import_item')
    import_items = cursor.fetchall()

    # import stored to history
    assert len(import_items) == 1
    origin, status = import_items[0]
    assert origin == 'manual'
    assert status == 'success'


def test_failure(app, temp_dir_1, mocker):
    expected_response = requests.Response()
    expected_response.status_code = 500
    expected_response.json = lambda : { 'error': 'Unexpected error' }
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response)
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    res, status = perform_import(
        [file1],
        [],
        { 'config.key1': 'test' },
        1,
        'manual'
    )

    assert status == 500
    assert 'error' in res and res['error'] == 'Unexpected error'

    db = get_db()

    cursor = db.execute('SELECT origin, status FROM import_item')
    import_items = cursor.fetchall()

    # import stored to history
    assert len(import_items) == 1
    origin, status = import_items[0]
    assert origin == 'manual'
    assert status == 'failure'
