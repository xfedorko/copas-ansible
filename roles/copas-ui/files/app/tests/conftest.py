import shutil
import tempfile
from flask import Flask
import pytest
from app import create_app
import os
from app.db.db import initialize_db

@pytest.fixture()
def app():
    db_fd, db_path = tempfile.mkstemp()
    test_config_dir = os.path.join(os.path.dirname(__file__), 'example_config')
    
    os.environ['COPAS_MODULE_NAME'] = 'example_module_name'
    os.environ['COPAS_MODULE_VERSION'] = '1.0.0'
    os.environ['COPAS_CONTAINER_NAME'] = 'example_container_name'
    os.environ['COPAS_MODULE_AUTHOR_NAME'] = 'Author name'
    os.environ['COPAS_MODULE_AUTHOR_EMAIL'] = 'Author email'
    os.environ['COPAS_MODULE_AUTHOR_INSTITUTION'] = 'Author institution'
    os.environ['COPAS_ANALYSIS_UI_URL'] = 'analysis_url'
    os.environ['COPAS_ANALYSIS_API_URL'] = 'api_url'

    app = create_app(config_dir=test_config_dir, database_path=db_path)

    with app.app_context():
        initialize_db(app)
        yield app
        if os.path.exists(app.config['DATABASE_PATH']):
            os.remove(app.config['DATABASE_PATH'])
        for var in os.environ.copy():
            if var.startswith('COPAS'):
                del os.environ[var]

@pytest.fixture()
def client(app: Flask):
    return app.test_client()


@pytest.fixture
def temp_dir_1():
    temp_dir = tempfile.mkdtemp()
    file1_path = os.path.join(temp_dir, "file1.txt")
    with open(file1_path, "w") as file1:
        file1.write("This is file1")

    file2_path = os.path.join(temp_dir, "file2.txt")
    with open(file2_path, "w") as file2:
        file2.write("This is file2")

    os.mkdir(os.path.join(temp_dir, 'dir'))

    yield temp_dir

    if os.path.exists(temp_dir):
        shutil.rmtree(temp_dir)