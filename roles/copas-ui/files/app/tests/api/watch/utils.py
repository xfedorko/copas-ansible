

import datetime
import json
from flask.testing import FlaskClient
from app.db.get import get_db

def prepare_configuration():
    db = get_db()
    dt = str(datetime.datetime.utcnow())
    db.execute('INSERT INTO configuration(contents, template_contents_id, created, modified) VALUES (?,?,?,?)', 
            (json.dumps({ 'config.key1': 'val' }), 1, dt, dt))
    db.commit()

def get_stub_watch_item(tmp_dir):
    return {
        'directory_path': tmp_dir,
        'configuration_id': 1
    }

def save_watch_item(client: FlaskClient, data):
    return client.post(
    '/api/watch', 
    data=json.dumps(data), 
    content_type='application/json')

def get_watch_items():
    db = get_db()
    cursor = db.execute('''
    SELECT w.directory_path, c.contents
    FROM watch as w
    INNER JOIN configuration as c
    ON w.configuration_id == c.id''')
    return cursor.fetchall()