import json
from flask.testing import FlaskClient
from app.tests.api.watch.utils import get_stub_watch_item, get_watch_items, prepare_configuration, save_watch_item

def test_success(client: FlaskClient, temp_dir_1):
    prepare_configuration()
    data = get_stub_watch_item(temp_dir_1)

    response = save_watch_item(client, data)
    
    assert response.status_code == 201
    assert 'error' not in response.json

    saved_watch_items = get_watch_items()

    assert len(saved_watch_items) == 1
    directory_path, config_contents = saved_watch_items[0]
    assert directory_path == temp_dir_1
    assert config_contents == json.dumps({ 'config.key1': 'val' })

def test_missing_directory_path(client: FlaskClient, temp_dir_1):
    prepare_configuration()
    data = get_stub_watch_item(temp_dir_1)
    del data['directory_path']

    response = save_watch_item(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_watch_items = get_watch_items()
    assert len(saved_watch_items) == 0


def test_missing_configuration_id(client: FlaskClient, temp_dir_1):
    prepare_configuration()
    data = get_stub_watch_item(temp_dir_1)
    del data['configuration_id']

    response = save_watch_item(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_watch_items = get_watch_items()
    assert len(saved_watch_items) == 0


def test_invalid_configuration_id(client: FlaskClient, temp_dir_1):
    prepare_configuration()
    data = get_stub_watch_item(temp_dir_1)
    data['configuration_id'] = 1000

    response = save_watch_item(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_watch_items = get_watch_items()
    assert len(saved_watch_items) == 0

def test_invalid_directory_path(client: FlaskClient):
    prepare_configuration()
    data = get_stub_watch_item('/nonexistent/path')

    response = save_watch_item(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_watch_items = get_watch_items()
    assert len(saved_watch_items) == 0