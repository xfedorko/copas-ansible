import json
import os
from flask.testing import FlaskClient
import requests

from app.db.get import get_db

def test_success(client: FlaskClient, temp_dir_1, mocker):

    mocker.patch('app.core.imports.perform_import', 
                 return_value=({}, 200))
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    file2 = os.path.join(temp_dir_1, 'file2.txt')

    expected_response = requests.Response()
    expected_response.status_code = 200
    expected_response.json = lambda: {}
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response)

    data = {
        'files': [file1, file2],
        'archives': [],
        'config': { 'config.key1': 'test' },
        'template': {
            'type': 'static',
            'key': 'template1',
            'contents_id': 1
        },
        'watch': [temp_dir_1],
        'save': {
            'name': 'Saved name',
            'note': 'Saved note'
        }
    }

    response = client.post(
        '/api/import', 
        data=json.dumps(data), 
        content_type='application/json')
    
    assert response.status_code == 200
    assert 'error' not in response.json

    db = get_db()

    cursor = db.execute('SELECT name, note FROM saved_configuration_info')
    saved_configurations = cursor.fetchall()

    assert len(saved_configurations) == 1
    name, note = saved_configurations[0]
    assert name == 'Saved name'
    assert note == 'Saved note'

    
    cursor = db.execute('SELECT directory_path FROM watch')
    watched_paths = cursor.fetchall()

    assert len(watched_paths) == 1
    watched_path, = watched_paths[0]
    assert watched_path == temp_dir_1
