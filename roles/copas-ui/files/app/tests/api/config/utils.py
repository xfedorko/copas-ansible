

import json
from flask.testing import FlaskClient
from app.db.get import get_db

def get_stub_config():
    return {
        'name': 'config name',
        'contents': { 'config.key1': 'val' },
        'template_contents_id': 1,
        'note': 'note'
    }

def save_config(client: FlaskClient, data):
    return client.post(
    '/api/config/saved', 
    data=json.dumps(data), 
    content_type='application/json')

def get_saved_configurations():
    db = get_db()

    cursor = db.execute('''
    SELECT sci.name, sci.note, c.contents, c.template_contents_id 
    FROM saved_configuration_info as sci
    INNER JOIN configuration as c
    ON sci.configuration_id = c.id''')
    return cursor.fetchall()