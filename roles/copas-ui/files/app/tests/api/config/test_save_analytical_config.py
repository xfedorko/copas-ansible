import json
from flask.testing import FlaskClient

from app.tests.api.config.utils import get_saved_configurations, get_stub_config, save_config

def test_success(client: FlaskClient):
    data = get_stub_config()

    response = save_config(client, data)
    
    assert response.status_code == 201
    assert 'error' not in response.json

    saved_configurations = get_saved_configurations()

    assert len(saved_configurations) == 1
    name, note, config_contents, template_contents_id = saved_configurations[0]
    assert name == 'config name'
    assert note == 'note'
    assert config_contents == json.dumps({ 'config.key1': 'val' })
    assert template_contents_id == 1

def test_missing_template(client: FlaskClient):
    data = get_stub_config()
    del data['template_contents_id']

    response = save_config(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_configurations = get_saved_configurations()

    assert len(saved_configurations) == 0

def test_missing_name(client: FlaskClient):
    data = get_stub_config()
    del data['name']

    response = save_config(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_configurations = get_saved_configurations()

    assert len(saved_configurations) == 0

def test_missing_contents(client: FlaskClient):
    data = get_stub_config()
    del data['contents']

    response = save_config(client, data)
    
    assert response.status_code == 400
    assert 'error' in response.json

    saved_configurations = get_saved_configurations()

    assert len(saved_configurations) == 0

def test_missing_note(client: FlaskClient):
    data = get_stub_config()
    del data['note']

    response = save_config(client, data)
    
    assert response.status_code == 201
    assert 'error' not in response.json

    saved_configurations = get_saved_configurations()

    assert len(saved_configurations) == 1
    name, note, config_contents, template_contents_id = saved_configurations[0]
    assert note == ''