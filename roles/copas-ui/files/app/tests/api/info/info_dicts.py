section_left = {
    'image': 'logo.png',
    'lines': [
        {
            'type': 'text',
            'content': 'John Doe'
        },
        {
            'type': 'mail',
            'content': 'john@mail.com',
            'to': 'john@mail.com',
        },
        {
            'type': 'text',
            'content': 'Institution'
        },            
        {
            'type': 'link',
            'content': 'Website',
            'to': 'https://website.com',
        }
    ]
}

help_sections = [
    {
        'name': 'Section 1',
        'contents': '## Section 1'
    },
    {
        'name': 'Section 2',
        'contents': '## Section 2'
    }
]

analysis_actions = [
    {
        'key': 'action1',
        'label': 'Action 1',
    },
    {
        'key': 'action2',
        'label': 'Action 2',
    }
]

analysis_profile = {
   'options': [
        {
            'name': 'low',
            'label': 'Low'
        },
        {
            'name': 'normal',
            'label': 'Normal'
        },
        {
            'name': 'high',
            'label': 'High'
        },
        {
            'name': 'maximum',
            'label': 'Maximum'
        }
   ]
}
