from flask.testing import FlaskClient
from info_dicts import *

def test_success(client: FlaskClient):
    response = client.get('/api/info')
    assert response.status_code == 200
    body = response.json

    assert body['module_type'] == 'analysis'
    assert body['module_name'] == 'example_module_name'
    assert body['module_version'] == '1.0.0'
    assert body['container_name'] == 'example_container_name'
    assert body['module_author_name'] == 'Author name'
    assert body['module_author_email'] == 'Author email'
    assert body['module_author_institution'] == 'Author institution'
    assert body['analysis_url'] == 'analysis_url'
    assert body['home_title'] == 'Example title'
    assert body['home_description'] == 'Example description'
    assert body['section_left'] == section_left
    assert body['background'] == 'background.svg'
    assert body['help_sections'] == help_sections
    assert body['analysis_actions'] == analysis_actions
    assert body['analysis_profile'] == analysis_profile

    assert len(body['config_templates']) == 1