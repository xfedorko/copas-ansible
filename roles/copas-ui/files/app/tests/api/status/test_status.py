from flask.testing import FlaskClient
from app.tests.api.utils import expected_response, verify_error_response


def test_success(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(200, { 'services': [] }))
    response = client.get('/api/status')
    assert response.status_code == 200
    assert response.json == { 'services': [] }

def test_analysis_failed(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(status=404))
    response = client.get('/api/status')
    verify_error_response(response, 404, '404')

def test_analysis_failed_error_provided(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(404, { 'error': 'error details' }))
    response = client.get('/api/status')
    verify_error_response(response, 404, ['404', 'error details'])

def test_response_invalid_json(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(200, None))
    response = client.get('/api/status')
    verify_error_response(response, 503, 'invalid json')

def test_response_invalid_json_schema(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(200, { 'services': {} }))
    response = client.get('/api/status')
    verify_error_response(response, 503, ['invalid json', 'schema'])