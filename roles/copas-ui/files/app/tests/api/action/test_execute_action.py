import json
from flask.testing import FlaskClient
import requests

from app.tests.api.utils import expected_response, verify_error_response

def test_success(client: FlaskClient, mocker):

    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(200, { 'message': 'Action performed successfully' }))
    response = client.post('/api/analysis/action', 
                           data=json.dumps({ 'key': 'action1' }), 
                           content_type='application/json')
    assert response.status_code == 200
    assert 'message' in response.json
    assert response.json == { 'message': 'Action performed successfully' }

def test_success_no_message(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response(200, {}))
    response = client.post('/api/analysis/action', 
                           data=json.dumps({ 'key': 'action1' }), 
                           content_type='application/json')
    assert response.status_code == 200
    assert 'message' not in response.json
    assert response.json == { }

def test_failure_analysis_error_status(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(400, { 'error': 'invalid action key provided' }))
    response = client.post('/api/analysis/action', 
                           data=json.dumps({ 'key': 'action10' }), 
                           content_type='application/json')
    verify_error_response(response, 400, 'invalid action key provided' )

def test_failure_analysis_request_failure(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 side_effect=requests.exceptions.RequestException('error'))
    response = client.post('/api/analysis/action', 
                           data=json.dumps({ 'key': 'action1' }), 
                           content_type='application/json')
    verify_error_response(response, 500)


def test_response_invalid_json_schema(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response(200, { 'message': 22 }))
    response = client.post('/api/analysis/action', 
                           data=json.dumps({ 'key': 'action1' }), 
                           content_type='application/json')
    verify_error_response(response, 503, ['invalid json', 'schema'])