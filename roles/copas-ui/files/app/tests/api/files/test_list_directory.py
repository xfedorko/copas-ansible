import os
from flask.testing import FlaskClient

def test_success(client: FlaskClient, temp_dir_1):
    response = client.get(f'/api/files?path={temp_dir_1}')
    assert response.status_code == 200
    data = response.json
    assert isinstance(data, list)
    assert len(data) == 3
    assert 'dir' in data[0]['name']
    assert 'file1.txt' in data[1]['name']
    assert 'file2.txt' in data[2]['name']

def test_no_path(client: FlaskClient):
    response = client.get('/api/files')
    assert response.status_code == 400

def test_nonexistent_path(client: FlaskClient):
    response = client.get('/api/files?path=/nonexistent_path')
    assert response.status_code == 404

def test_regular_file(client: FlaskClient, temp_dir_1):
    response = client.get(f'/api/files?path={os.path.join(temp_dir_1, "file1.txt")}')
    assert response.status_code == 400