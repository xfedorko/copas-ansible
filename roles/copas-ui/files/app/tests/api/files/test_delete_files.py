import os
from flask.testing import FlaskClient

def test_delete_directory_with_contents(client: FlaskClient, temp_dir_1):
    response = client.delete(f'/api/files?path={os.path.dirname(temp_dir_1)}&name={temp_dir_1}')
    assert response.status_code == 200
    assert not os.path.exists(os.path.join(temp_dir_1))

def test_delete_regular_file(client: FlaskClient, temp_dir_1):
    response = client.delete(f'/api/files?path={temp_dir_1}&name={os.path.join(temp_dir_1, "file1.txt")}')
    assert response.status_code == 200
    assert not os.path.exists(os.path.join(temp_dir_1, "file1.txt"))
    assert os.path.exists(os.path.join(temp_dir_1, "file2.txt"))

def test_no_path(client: FlaskClient):
    response = client.delete('/api/files')
    assert response.status_code == 400

def test_nonexistent_directory_path(client: FlaskClient):
    response = client.delete('/api/files?path=/nonexistent_path&name=file1.txt')
    assert response.status_code == 404

def test_nonexistent_content_paths(client: FlaskClient, temp_dir_1):
    response = client.delete(f'/api/files?path={temp_dir_1}&name=nonexistent')
    assert response.status_code == 404

