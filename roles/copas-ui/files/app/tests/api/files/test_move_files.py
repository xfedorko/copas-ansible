import os
from flask.testing import FlaskClient

def test_success(client: FlaskClient, temp_dir_1):
    subdir = os.path.join(temp_dir_1, 'dir')
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    file2 = os.path.join(temp_dir_1, 'file2.txt')
    response = client.post(f'/api/files/move?destination={subdir}&path={file1}&path={file2}')
    assert response.status_code == 200
    assert not os.path.exists(file1)
    assert not os.path.exists(file2)
    assert os.path.exists(os.path.join(subdir, os.path.basename(file1)))
    assert os.path.exists(os.path.join(subdir, os.path.basename(file2)))

def test_no_destination(client: FlaskClient):
    response = client.post(f'/api/files/move?path=file1&path=file2')
    assert response.status_code == 400

def test_no_path(client: FlaskClient, temp_dir_1):
    response = client.post(f'/api/files/move?destination={temp_dir_1}')
    assert response.status_code == 400

def test_nonexistent_destination(client: FlaskClient, temp_dir_1):
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    response = client.post(f'/api/files/move?destination=/nonexistent&path={file1}')
    assert response.status_code == 404

def test_duplicate_creates_copy(client: FlaskClient, temp_dir_1):
    file1 = os.path.join(temp_dir_1, 'file1.txt')
    response = client.post(f'/api/files/move?destination={temp_dir_1}&path={file1}')
    assert response.status_code == 200
    assert len(os.listdir(temp_dir_1)) == 3


