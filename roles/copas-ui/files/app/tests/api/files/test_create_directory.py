import os
from flask.testing import FlaskClient

def test_success(client: FlaskClient, temp_dir_1):
    response = client.post(f'/api/files/directory?path={temp_dir_1}&name=new_dir')
    assert response.status_code == 201
    assert os.path.exists(os.path.join(temp_dir_1, 'new_dir'))


def test_no_path(client: FlaskClient):
    response = client.post('/api/files/directory?name=xa')
    assert response.status_code == 400

def test_no_name(client: FlaskClient):
    response = client.post('/api/files/directory?path=/')
    assert response.status_code == 400

def test_nonexistent_directory_path(client: FlaskClient):
    response = client.post('/api/files/directory?path=/nonexistent_path&name=new_dir')
    assert response.status_code == 404

def test_duplicate_name(client: FlaskClient, temp_dir_1):
    response = client.post(f'/api/files/directory?path={temp_dir_1}&name=file1.txt')
    assert response.status_code == 409

def test_invalid_name(client: FlaskClient, temp_dir_1):
    response = client.post(f'/api/files/directory?path={temp_dir_1}&name=***')
    assert response.status_code == 400

