from flask.testing import FlaskClient
import requests

from app.tests.api.utils import expected_response, verify_error_response

def test_success(client: FlaskClient, mocker):
    expected_response = requests.Response()
    expected_response.status_code = 200
    expected_response.json = lambda: { 'name': 'high' }
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response)
    response = client.get('/api/analysis/profile')
    assert response.status_code == 200
    assert 'name' in response.json
    assert response.json['name'] == 'high'

def test_failure_analysis_error_status(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', 
                 return_value=expected_response(400, { 'error': 'invalid profile name provided' }))
    response = client.get('/api/analysis/profile')
    verify_error_response(response, 400, 'invalid profile name provided')

def test_failure_analysis_error_status_no_message(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', 
                 return_value=expected_response(500, {}))
    response = client.get('/api/analysis/profile')
    verify_error_response(response, 500, str(response.status_code))

def test_response_invalid_json_schema(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', return_value=expected_response(200, {}))
    response = client.get('/api/analysis/profile')
    verify_error_response(response, 503, ['invalid json', 'schema'])

def test_failure_analysis_request_failure(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.get', side_effect=requests.exceptions.RequestException('error'))
    response = client.get('/api/analysis/profile')
    verify_error_response(response, 500)