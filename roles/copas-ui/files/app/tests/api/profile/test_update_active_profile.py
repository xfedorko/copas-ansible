import json
from flask.testing import FlaskClient
import requests

from app.tests.api.utils import expected_response, verify_error_response

def test_success(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(200, {}))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({ 'name': 'high' }), 
                           content_type='application/json')
    assert response.status_code == 200
    assert response.json == {}

def test_failure_missing_name(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(200, {}))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({}), 
                           content_type='application/json')
    verify_error_response(response, 400, 'name')

def test_failure_analysis_error_status(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(400, { 'error': 'invalid profile name' }))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({ 'name': 'invalid' }), 
                           content_type='application/json')
    verify_error_response(response, 400, 'invalid profile name')

def test_failure_analysis_error_status_no_message(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', 
                 return_value=expected_response(500, {}))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({ 'name': 'invalid' }), 
                           content_type='application/json')
    verify_error_response(response, 500)

def test_failure_request_failure(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', side_effect=requests.exceptions.RequestException('error'))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({ 'name': 'invalid' }), 
                           content_type='application/json')
    verify_error_response(response, 500)

def test_failure_invalid_json_response(client: FlaskClient, mocker):
    mocker.patch('app.session.AnalysisSession.post', return_value=expected_response(200, { 'whatever' }))
    response = client.post('/api/analysis/profile', 
                           data=json.dumps({ 'name': 'high' }), 
                           content_type='application/json')
    verify_error_response(response, 503)