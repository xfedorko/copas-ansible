
from typing import Dict, List
import requests


def expected_response(status: int = 200, json: Dict or None = None, headers: Dict = { 'Content-Type': 'application/json' }):
    response = requests.Response()
    response.status_code = status
    response.headers = headers
    response.json = lambda: json
    return response

def verify_error_response(response: requests.Response, status: int, errors: str or List[str] or None = None):
    assert response.status_code == status
    assert 'error' in response.json 
    if errors is not None:
        if isinstance(errors, str):
            assert errors in response.json['error']
        else:
            for e in errors:
                assert e in response.json['error']