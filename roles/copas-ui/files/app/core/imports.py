from contextlib import ExitStack
import os
import tempfile
from typing import Dict, List
from app.core.data.history import create_history_item, update_history_item, update_history_item_extracted_archives, update_history_item_failure, update_history_item_success
from flask import current_app as app
from flask_babel import gettext

from app.core.archives import process_archives
from app.session import get_analysis_session

def perform_import(regular_files: List[str], archives: List[Dict], config_contents, template_contents_id: int, origin: str):
    files_for_analysis = []

    import_id = create_history_item(origin, config_contents, template_contents_id, regular_files, archives)

    files_for_analysis.extend(regular_files)
    with ExitStack() as stack:
        temp_dirs = [stack.enter_context(tempfile.TemporaryDirectory()) for _ in range(len(archives))]
     
        if len(archives) > 0:
            update_history_item(import_id, 'extracting')
            files_for_analysis.extend(process_archives(archives, temp_dirs))
            update_history_item_extracted_archives(import_id)

        payload = {
            'files': files_for_analysis,
            'config': config_contents
        }

        app.logger.info('files prepared, sending request to analysis, payload: %s', payload)
        try:
            error_message = None
            update_history_item(import_id, 'importing')
            analysis_session= get_analysis_session()
            response = analysis_session.post(f'{analysis_session.base_url}/import', json = payload)
            analysis_data = response.json()

            if response.status_code >= 400:
                error_message = analysis_data['error'] if 'error' in analysis_data else 'error unspecified'
                app.logger.error('import to the analysis failed with status %d - %s', response.status_code, error_message)
                update_history_item_failure(import_id, message = error_message)
                return { 'error': error_message }, response.status_code
            
            app.logger.info('import to the analysis succeeded')

            failed_files = analysis_data['failed'] if 'failed' in analysis_data else []

            failed_regular_files = [f for f in failed_files if f in regular_files]

            failed_archives = []
            for archive, temp_dir in zip(archives, temp_dirs):
                mapped_archive = {
                    'path': archive['path'],
                    'contents': [c for c in archive['contents'] if next((f for f in failed_files if f == os.path.join(temp_dir, c)), None) is not None]
                }
                failed_archives.append(mapped_archive)
            
            update_history_item_success(import_id, failed_files = failed_regular_files, failed_archives = failed_archives)

            return { }, 200

        except:
            app.logger.error('import to the analysis failed - request failed', exc_info=True)
            update_history_item_failure(import_id, message = error_message)
            return { 'error': gettext('request to the analysis failed') }, 500
