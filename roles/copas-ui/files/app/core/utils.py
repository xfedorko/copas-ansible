import os
import tarfile
from typing import List
import zipfile

def get_regular_files(directory: str):
    regular_files = []
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if not os.path.basename(filename).startswith('.'):
                regular_files.append(os.path.abspath(os.path.join(root, filename)))
    return regular_files

def get_paths_for_files_in_directory(directory_path: str, files: List[str]):
    paths = []
    for f in files:
        dir_file = os.path.join(directory_path, f)
        if os.path.isfile(dir_file):
            paths.append(dir_file)
    return paths

def extract_tar(tar_path: str, destination_path: str, contents: List[str] or None = None):
    with tarfile.open(tar_path) as tar:
        tar.extractall(destination_path, members = None if contents is None \
                        else [m for m in tar.getmembers() if m.name in contents])

def extract_zip(zip_path: str, destination_path: str, contents: List[str] or None = None):
    with zipfile.ZipFile(zip_path, 'r') as zip:
        zip.extractall(destination_path, members = None if contents is None \
                       else [f for f in zip.filelist if f.filename in contents])