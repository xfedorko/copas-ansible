
import json
from typing import List
from app.core.watchdog.utils import register_watchdog_path, unregister_watchdog_path
from datetime import datetime
from flask import current_app as app
from flask_babel import gettext

from app.db.get import get_db

def insert_watch_item(directory_path: str, configuration: int or str, template_contents_id: int or None):
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        if isinstance(configuration, int):
            c = db.execute('SELECT contents, template_contents_id FROM configuration WHERE id = ?', (configuration,)).fetchone()
            if c is None:
                return { 'error': gettext('nonexistent configuration with id %(x)d', x=configuration) }, 400
            config, template_contents_id = c
        else:
            config = json.dumps(configuration)
        c = db.execute('INSERT INTO configuration(contents, template_contents_id, created, modified) VALUES (?,?,?,?)', 
            (config, template_contents_id, dt, dt))
        db.execute(
            'INSERT INTO watch(directory_path, configuration_id, created) VALUES (?,?,?)', 
            (directory_path, c.lastrowid, dt))
        register_watchdog_path(directory_path)
        db.commit()
        app.logger.info('successfully created watch item for directory %s', directory_path)
        return { }, 201
    except:
        app.logger.error('failed to insert watch item at path %s', directory_path, exc_info = True)
        return { 'error': gettext('failed to insert watch item at path %(x)s',x=directory_path) }, 500

def delete_watch_item(id: int):
    try:
        db = get_db()
        cursor = db.execute('SELECT configuration_id, directory_path FROM watch WHERE id = ?', (id,))
        r = cursor.fetchone()
        if r is None:
            return { 'error': gettext('no watch item with id %(x)d', x=id) }, 400
        db.execute('DELETE FROM configuration WHERE id = ?', (r[0],))
        unregister_watchdog_path(r[1])
        db.commit()
        app.logger.info('successfully deleted watch item with id %s', id)
        return { }, 200
    except:
        app.logger.error('failed to delete watch item with id %d', id, exc_info = True)
        return { 'error': gettext('failed to delete watch item with id %(x)d',x=id) }, 500

def get_all_watch_items() -> List:
    db = get_db()
    rows = db.execute('''
        SELECT id, configuration_id, directory_path, created
        FROM watch 
        ORDER BY watch.created DESC'''
    ).fetchall()
    return [{
        'id': id,
        'configuration_id': configuration_id,
        'directory_path': directory_path,
        'created': created
    } for id, configuration_id, directory_path, created in rows]
        