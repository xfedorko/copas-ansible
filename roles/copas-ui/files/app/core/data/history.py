
import json
from typing import Dict, List
from datetime import datetime
from flask import current_app as app

from app.db.get import get_db

def create_history_item(
    origin: str,
    config_contents: any,
    template_contents_id: int,
    files: List[str],
    archives: List[Dict]):
    try:
        db = get_db()
        dt = str(datetime.utcnow())  
        c = db.execute('INSERT INTO configuration(contents, template_contents_id, created, modified) VALUES (?,?,?,?)', 
            (json.dumps(config_contents), template_contents_id, dt, dt))
        c = db.execute('INSERT INTO import_item(configuration_id, origin, status, start) VALUES (?,?,?,?)', 
            (c.lastrowid, origin, 'created', dt))
        import_item_id = c.lastrowid
        db.executemany('INSERT INTO imported_file(import_item_id, path, status) VALUES (?,?,?)', 
            [(import_item_id, path, 'initial') for path in files])
        db.executemany('INSERT INTO imported_file(import_item_id, archive_path, path, status) VALUES (?,?,?,?)', 
            [(import_item_id, archive['path'], path, 'initial') for archive in archives for path in archive['contents']])
        db.commit()
        return import_item_id
    except:
        app.logger.error('failed to create history item', exc_info=True)
        raise

def update_history_item_extracted_archives(id: int):
    try:
        db = get_db()
        db.execute('UPDATE imported_file SET status = ? WHERE import_item_id = ? AND archive_path IS NOT NULL', ('extracted', id,))
        db.commit()
    except:
        app.logger.error('failed to update history item %d with extracted archives', id, exc_info=True)
        raise

def update_history_item(id: int, status: str, message: str or None = None):
    try:
        db = get_db()
        db.execute('UPDATE import_item SET status = ?, message = ? WHERE id = ?', (status, message, id))
        db.commit()
    except:
        app.logger.error('failed to update history item %d with status %s', id, status, exc_info=True)
        raise

def update_history_item_failure(id: int, message: str):
    ''' update history item with given id to a failed state '''
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        db.execute('UPDATE import_item SET status = ?, message = ?, end = ? WHERE id = ?', ('failure', message, dt, id))
        db.execute('UPDATE imported_file SET status = ? WHERE import_item_id = ?', ('failure',  id))
        db.commit()
    except:
        app.logger.error('failed to update history item with id %d  with status %s', id, 'failure', exc_info=True)
        raise

def update_history_item_success(id: int, failed_files: List[str], failed_archives: List[Dict]):
    '''
    update history with given id to a success state
    failed_files is a list of files which failed the import
    failed_archives is a list of archive contents, which failed the import
        - path - path to the archive
        - contents - files inside the archive, which failed the import
    '''
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        db.execute('UPDATE import_item SET status = ?, end = ? WHERE id = ?', ('success', dt, id))
        db.execute('UPDATE imported_file SET status = ? WHERE import_item_id = ?', ('success', id))
        db.executemany('UPDATE imported_file SET status = ? WHERE path = ? AND import_item_id = ?',
            [('failure', file, id) for file in failed_files])
        db.executemany('UPDATE imported_file SET status = ? WHERE archive_path = ? AND path = ? AND import_item_id = ?', 
            [('failure', archive['path'], file, id) for archive in failed_archives for file in archive['contents']])
        db.commit()
    except:
        app.logger.error('failed to update history item with id %d  with status %s', id, 'failure', exc_info=True)
        raise

def finalize_unfinished_history_items():
    try:
        db = get_db()
        cursor = db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='import_item'")
        database_initialized = cursor.fetchone() is not None
        if database_initialized:
            dt = str(datetime.utcnow())
            rows = db.execute('SELECT id FROM import_item WHERE status NOT IN (?,?,?)', ('success', 'failure', 'unknown')).fetchall()
            for id, in rows:
                db.execute('UPDATE import_item SET status = ?, end = ? WHERE id = ?', ('unknown', dt, id))
                db.execute('UPDATE imported_file SET status = ? WHERE import_item_id = ?', ('unknown', id))
            db.commit()
            if len(rows) > 0:
                app.logger.info('successfully finalized %d unfinished history items', len(rows))
    except:
        app.logger.error('failed to finalize unfinished history items', exc_info=True)
        raise

def get_all_history_items():
    db = get_db()
    rows = db.execute('''
        SELECT i.id, i.configuration_id, i.origin, i.status, i.message, i.start, i.end, f.archive_path, f.path, f.status
        FROM import_item as i
        INNER JOIN imported_file as f 
        ON i.id = f.import_item_id 
        ORDER BY IFNULL(i.end, '6666-01-01') DESC
        ''').fetchall()
    history_items = []
    for id, configuration_id, origin, status, message, start, end, archive_path, path, file_status in rows:
        if len(history_items) == 0 or id != history_items[-1]['id']:
            history_items.append({
                'id': id,
                'configuration_id': configuration_id, 
                'origin': origin,
                'status': status,
                'message': message,
                'start': start,
                'end': end,
                'files': [{
                    'path': path, 
                    'status': file_status
                }] if archive_path is None else [],
                'archives': [] if archive_path is None else \
                    [{ 
                        'archive_path': archive_path,
                        'contents': [{ 'path': path, 'status': file_status}]
                    }],
            })
        else:
            if archive_path is None:
                history_items[-1]['files'].append({
                        'path': path,
                        'status': file_status
                    })
            else:
                if len(history_items[-1]['archives']) == 0 or history_items[-1]['archives'][-1]['archive_path'] != archive_path:
                    history_items[-1]['archives'].append({
                        'archive_path': archive_path,
                        'contents': [{ 'path': path, 'status': file_status }]
                    })
                else:
                    history_items[-1]['archives'][-1]['contents'].append({ 'path': path, 'status': file_status })
    return history_items

