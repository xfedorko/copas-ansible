import json
from datetime import datetime
from flask import current_app as app
from flask_babel import gettext

from app.db.get import get_db

def update_configuration(id: int, contents: any):
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        db.execute('UPDATE configuration SET contents = ?, modified = ? WHERE id = ?', (json.dumps(contents), dt, id))
        db.commit()
        app.logger.info('succesfully updated configuration with id %d', id)
        return {}, 200
    except:
        app.logger.error('failed to update configuration with id %d', id, exc_info=True)
        return { 'error': gettext('failed to update configuration with id %(x)d', x=id) }, 500


def get_configuration(id: int):
    db = get_db()
    config_row = db.execute('''
        SELECT id, template_contents_id, contents, created, modified
        FROM configuration
        WHERE id = ?
        ''', (id,)).fetchone()
    if config_row is None:
        return None
    config_id, template_contents_id, config_contents, created, modified = config_row
    r = db.execute('SELECT name FROM saved_configuration_info WHERE configuration_id = ?', (config_id,)).fetchone()
    saved_config_name = r[0] if r is not None else None
    r = db.execute('SELECT directory_path FROM watch WHERE configuration_id = ?', (config_row[0],)).fetchone()
    directory_path = r[0] if r is not None else None

    return {
        'id': config_id,
        'config_contents': json.loads(config_contents),
        'template_contents_id': template_contents_id,
        'created': created,
        'modified': modified,
        'watching_directory': directory_path,
        'saved_config': saved_config_name
    }
