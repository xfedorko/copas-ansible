
import json
import sqlite3
from typing import List
from datetime import datetime
from flask import current_app as app
from flask_babel import gettext

from app.db.get import get_db

def insert_saved_configuration_info(
    name: str,
    contents: any,
    template_contents_id: int  = None,
    note: str = None,
    template_key: str = None ):
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        if template_contents_id is None and template_key is not None:
            c = db.execute('SELECT id FROM configuration_template_contents WHERE configuration_template_key = ?', (template_key,)).fetchone()
            if c is None:
                return { 'error': gettext('invalid template key "%(x)s" provided', x=template_key) }, 400
            template_contents_id, = c
        c = db.execute(
            'INSERT INTO configuration(contents, template_contents_id, created, modified) VALUES (?,?,?,?)', 
            (json.dumps(contents), template_contents_id, dt, dt))
        db.execute(
            'INSERT INTO saved_configuration_info(name, configuration_id, note, created, modified) VALUES (?,?,?,?,?)', 
            (name, c.lastrowid, note if note is not None else '', dt, dt))
        db.commit()
        app.logger.info('successfully saved config with name %s', name)
        return {}, 201
    except sqlite3.IntegrityError as e:
        if 'UNIQUE' in str(e):
            app.logger.error('failed to create config with already existing name "%s"', name)
            return { 'error': gettext('failed to create config with already existing name "%(x)s"', x=name) }, 400
    except:
        app.logger.error('failed to save configuration with name %s', name, exc_info = True)
        return { 'error': gettext('failed to save configuration with name %(x)s', x=name) }, 500

def delete_saved_configuration_info(name: str):
    try:
        db = get_db()
        r = db.execute('SELECT configuration_id FROM saved_configuration_info WHERE name = ?', (name,)).fetchone()
        if r is None:
            return { 'error': gettext('no configuration with name with name %(x)s exists', x=name) }, 400
        db.execute('DELETE FROM configuration WHERE id = ?', (r[0],))
        db.commit()
        app.logger.info('successfully deleted configuration with name %s', name)
        return {}, 200
    except:
        app.logger.error('failed to delete configuration with name %s', name, exc_info = True)
        return { 'error': gettext('failed to delete configuration with name %(x)s', x=name) }, 500

def get_saved_configuration_info(name: str):
    db = get_db()
    r = db.execute('''
        SELECT id, configuration_id, name, note, created, modified
        FROM saved_configuration_info
        WHERE name = ?''', (name,)).fetchone()
    if r is None:
        return None
    id, configuration_id, name, note, created, modified = r
    return {
        'id': id,
        'configuration_id': configuration_id,
        'name': name,
        'note': note,
        'created': created,
        'modified': modified
    }

def get_all_saved_configuration_infos() -> List:
    db = get_db()
    rows = db.execute('''
        SELECT id, configuration_id, name, note, created, modified
        FROM saved_configuration_info''').fetchall()
    return [{
        'id': id,
        'configuration_id': configuration_id,
        'name': name,
        'note': note,
        'created': created,
        'modified': modified            
    } for id, configuration_id, name, note, created, modified in rows]
