import json
import sqlite3
from typing import Dict, List
from datetime import datetime
from flask import current_app as app
from flask_babel import gettext

from app.db.get import get_db
from app.config.locale import load_locale_template_translations

def insert_static_config_template(key: str, contents: any):
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        db.execute(
            'INSERT INTO configuration_template(key, type) VALUES (?,?)', (key, 'static'))
        db.execute(
            'INSERT INTO configuration_template_contents(contents, created, configuration_template_key) VALUES (?,?,?)',
            (json.dumps(contents), dt, key)
            )
        db.commit()
        app.logger.info('successfully created static configuration template with key %s', key)
        return {}, 201
    except sqlite3.IntegrityError as e:
        if 'UNIQUE' in str(e):
            app.logger.error('failed to create configuration template because of key conflict "%s"', key)
            return { 'error': gettext('failed to create configuration template because of key conflict "%(x)s"', x=key) }, 400
    except:
        app.logger.error('failed to create configuration template with key %s', key, exc_info=True)
        return { 'error': gettext('failed to create configuration template with key "%(x)s', x=key) }, 500

def insert_dynamic_config_template(key: str):
    try:
        db = get_db()
        db.execute(
            'INSERT INTO configuration_template(key, type) VALUES (?,?)', 
            (key, 'dynamic'))
        db.commit()
        app.logger.info('successfully created dynamic configuration template with key %s', key)
        return {}, 201
    except:
        app.logger.error('failed to create dynamic configuration template with key %s', key, exc_info=True)
        return { 'error': f'failed to create dynamic configuration template with key {key}' }, 500

def insert_dynamic_config_template_contents(key: str, contents: any):
    try:
        db = get_db()
        dt = str(datetime.utcnow())
        c = db.execute(
            'INSERT INTO configuration_template_contents(contents, created, configuration_template_key) VALUES (?,?,?)',
            (json.dumps(contents), dt, key)
        )
        db.commit()
        app.logger.info('successfully created template contents for dynamic configuration with key %s', key)
        return c.lastrowid
    except:
        app.logger.error('failed to create template contents for dynamic configuration with key %s', key, exc_info=True)
        raise

def update_config_contents_with_translations(element: Dict or List, translations: Dict or None):
    if translations is None:
        return element
    
    if isinstance(element, List):
        return [update_config_contents_with_translations(e, translations) for e in element]

    if element['content_type'] == 'section':
        sections = translations['sections']
        new_title = sections.get(element['title'], None)
        if new_title is not None:
            element['title'] = new_title
        for c in element['contents']:
            update_config_contents_with_translations(c, translations)
    elif element['content_type'] == 'control':
        controls = translations['controls']
        new_control_values = controls.get(element['config_key'], None)
        if new_control_values is not None:
            for k, v in new_control_values.items():
                element[k] = v
    return element

def get_all_static_config_templates(locale: str or None = None):
    db = get_db()
    rows = db.execute('''
        SELECT t.key, t.type, c.id, c.contents, c.created 
        FROM configuration_template as t
        INNER JOIN configuration_template_contents as c 
        ON t.key = c.configuration_template_key 
        WHERE t.type = "static"
        ''').fetchall()
    
    templates_translations = load_locale_template_translations(locale) if locale is not None else None

    return [
        {
            'key': key,
            'type': template_type,
            'contents_id': contents_id,
            'contents': update_config_contents_with_translations(json.loads(contents)['contents'], 
                                                                 templates_translations.get(key, None) if templates_translations is not None else None),
            'created': created
        } for key, template_type, contents_id, contents, created in rows 
    ]

def get_all_dynamic_config_templates():
    db = get_db()
    rows = db.execute('''
        SELECT key, type 
        FROM configuration_template
        WHERE type = "dynamic"
        ''').fetchall()
    return [
        {
            'key': key,
            'type': template_type
        } for key, template_type in rows
    ]