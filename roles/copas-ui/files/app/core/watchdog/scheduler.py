import fcntl
import os
import tarfile
import zipfile

from flask import Flask
from app.core.imports import perform_import
from app.core.data.configuration import get_configuration
from .utils import add_processed_files, get_nonprocessed_files
from app.core.data.watch import get_all_watch_items

def watchdog_check(app: Flask):    
    with app.app_context():
        watch_items = get_all_watch_items()

        for path, config_id in [(item['directory_path'], item['configuration_id']) for item in watch_items]:
            if not os.path.exists(path) or not os.path.isdir(path):
                app.logger.warn('Nonexistent directory path %s. Skipping', path)
                continue
            lockfile = os.path.join(path, '.lock')
            with open(lockfile, 'w') as f:
                fcntl.flock(f, fcntl.LOCK_EX)
                files_for_import = None
                try:
                    files_for_import = get_nonprocessed_files(path)
                    if len(files_for_import) == 0:
                        continue

                    app.logger.info('Detecting %d new files', len(files_for_import))

                    config = get_configuration(config_id)

                    archives = [{ 'path': f } for f in files_for_import \
                                if tarfile.is_tarfile(f) or zipfile.is_zipfile(f)]
                    regular_files = list(set(files_for_import).difference([archive['path'] for archive in archives]))

                    res, status = perform_import(regular_files, archives, config['config_contents'], config['template_contents_id'], origin = 'watchdog')
                    add_processed_files(path, files_for_import, 'failure' if status > 299 else 'success')                 
                except:
                    app.logger.error('Failed the watchdog check routine for path %s', path, exc_info=True)
                    if files_for_import is not None and len(files_for_import) > 0:
                        add_processed_files(path, files_for_import, 'failure')
                finally:
                    fcntl.flock(f, fcntl.LOCK_UN)

