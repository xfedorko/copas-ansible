
import json
import os
from typing import List

from app.core.utils import get_regular_files

def register_watchdog_path(directory_path: str):
    watchdog_path = get_watchdog_path(directory_path)
    if os.path.exists(watchdog_path):
        os.remove(watchdog_path)
    with open(watchdog_path, 'w') as file:
        watchdog_files = {
            'initial': get_regular_files(directory_path),
            'success': [],
            'failure': []
        }
        json.dump(watchdog_files, file, indent = 4)

def unregister_watchdog_path(directory_path: str):
    watchdog_path = get_watchdog_path(directory_path)
    if os.path.exists(watchdog_path):
        os.remove(watchdog_path)

def get_watchdog_path(directory_path: str) -> str:
    return os.path.join(directory_path, '.watchdog')

def get_all_processed_files(directory_path: str) -> List[str]:
    watchdog_path = get_watchdog_path(directory_path)
    if not os.path.exists(watchdog_path):
        return []
    with open(watchdog_path, 'r') as file:
        watchdog_files = json.load(file)
        return watchdog_files['initial'] + watchdog_files['success'] + watchdog_files['failure']

def get_nonprocessed_files(directory_path: str) -> List[str]:
    return list(set(get_regular_files(directory_path)).difference(get_all_processed_files(directory_path)))

def add_processed_files(directory_path: str, processed_files: List[str], status: str):
    watchdog_path = get_watchdog_path(directory_path)
    with open(watchdog_path, 'r') as file:
        watchdog_files = json.load(file)
    with open(watchdog_path, 'w') as file:
        watchdog_files[status].extend(processed_files)
        json.dump(watchdog_files, file, indent = 4)
            
        



