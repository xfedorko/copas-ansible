import tarfile
from typing import Dict, List
import zipfile
from flask import current_app as app

from app.core.utils import extract_tar, extract_zip, get_regular_files

def process_archive(archive_path: str, directory_output: str, archive_contents: List[str] or None = None ):
    '''
    extracts archive given by 'archive_path' to 'directory_output'
    if 'archive_contents' is provided, extract only given files, if not - extract all files
    returns list of paths to extracted files
    '''
    if tarfile.is_tarfile(archive_path):
        app.logger.info('extracting tar %s contents to a directory %s', archive_path, directory_output)
        extract_tar(archive_path, directory_output, archive_contents)
    elif zipfile.is_zipfile(archive_path):
        app.logger.info('extracting zip %s contents to a directory %s', archive_path, directory_output)
        extract_zip(archive_path, directory_output, archive_contents)
    else:
        app.logger.error('invalid archive path provided: %s', archive_path)
        raise Exception(f'invalid archive path provided: {archive_path}')
    return get_regular_files(directory_output)

def process_archives(archives: List[Dict], directories: List[str]):
    '''
    extracts archives into directories
    Input: 
        archives represented by list of dicts 
        -> path - the path to archive
        -> contents - if provided - list of files for extraction inside the archive, else all files are extracted
        directories to extract archives into, same length as archives
    Output - paths to extracted files, which should be analyzed
    '''
    app.logger.info(f'{len(archives)} archives detected, extracting')

    paths_to_extracted_files = []
    for archive, dir in zip(archives, directories):
        paths = process_archive(archive['path'], dir, archive.get('contents'))
        paths_to_extracted_files.extend(paths)
        if archive.get('contents') is None:
            archive['contents']  = [p.removeprefix(f'{dir}/') for p in paths]
    
    return paths_to_extracted_files