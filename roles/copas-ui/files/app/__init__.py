import os

from app.api.config.detail import ConfigResource
from app.api.config.saved import SavedConfigInfoResource
from app.api.config import config_blueprint
from app.api.imports import import_blueprint
from app.api.status import status_blueprint
from app.api.action import action_blueprint
from app.api.profile import profile_blueprint
from app.api.info import info_blueprint
from flask import Blueprint, Flask, send_from_directory, make_response
from flask_restful import Api
from app.api.files import files_blueprint
from app.api.watch import WatchResource
from app.api.history import history_blueprint
from app.config import load_config
from app.core.data.history import finalize_unfinished_history_items
from app.config.locale import get_locale

from flask_apscheduler import APScheduler
from flask_babel import Babel

from app.core.watchdog.scheduler import watchdog_check

def create_app(config_dir = '/etc/copas', database_path = 'copas.db'):

    app = Flask(__name__, static_folder='../ui/build')

    babel = Babel(app, configure_jinja=False, locale_selector=get_locale)

    load_config(app, config_dir, database_path)

    module_type = app.config['COPAS']['module']['type']

    if module_type == 'analysis':
        api = Api(app)           
        
        api.add_resource(ConfigResource, '/api/config', '/api/config/<id>')
        api.add_resource(SavedConfigInfoResource, '/api/config/saved', '/api/config/saved/<name>')
        api.add_resource(WatchResource, '/api/watch')

        app.register_blueprint(import_blueprint)
        app.register_blueprint(history_blueprint)
        app.register_blueprint(config_blueprint)

        from app.db import db
        db.init_app(app)

        with app.app_context():
            finalize_unfinished_history_items()
        
        scheduler = APScheduler()
        scheduler.init_app(app)
        scheduler.start()
        scheduler.add_job(id='watchdog_check' ,func=watchdog_check, trigger='interval', seconds=120, args=(app,))

    @app.after_request
    def cors(response):
        if app.debug:
            response.headers.add('Access-Control-Allow-Origin', '*')
            response.headers.add('Access-Control-Allow-Methods', '*')
            response.headers.add('Access-Control-Allow-Headers', '*')
        return response

    # Serve ui react app
    @app.route('/')
    def index():
        return send_from_directory(app.static_folder, 'index.html')
    
    # Serve files in the whole filesystem from filemanager
    @app.route('/show/<path:path>')
    def show_or_download_file(path):
        if os.path.exists(os.path.join('/', path)):
            return send_from_directory('/', path)
        return make_response('Not found', 404)
    
    # Serve resources stored in configuration
    static_blueprint = Blueprint('analysis', __name__, static_url_path='/analysis/static', static_folder=os.path.join(config_dir, 'public'))
    app.register_blueprint(static_blueprint)

    # Serve resources for the ui
    @app.route('/<path:path>')
    def serve(path):
        if os.path.exists(os.path.join(app.static_folder, path)):
            return send_from_directory(app.static_folder, path)
        return make_response('Not found', 404)
    
    app.register_blueprint(info_blueprint)
    app.register_blueprint(files_blueprint)
    app.register_blueprint(status_blueprint)
    app.register_blueprint(action_blueprint)
    app.register_blueprint(profile_blueprint)

    return app
