import os
from flask import request, current_app as app
import yaml

def get_supported_languages():
    supported_languages =  [ 'en' ]
    with app.app_context():
        localization_directory = os.path.join(app.config['CONFIG_DIR'], 'localization')
        if not os.path.isdir(localization_directory):
            return supported_languages            
        for entry in os.listdir(localization_directory):
            full_path = os.path.join(localization_directory, entry)
            if os.path.isdir(full_path):
                supported_languages.append(entry)
    return supported_languages


def get_locale():
    return request.accept_languages.best_match(get_supported_languages())

def load_locale_config_if_exists(locale: str):
    if not isinstance(locale, str):
        return
    with app.app_context():
        configuration_directory = app.config['CONFIG_DIR']
        locale_directory = os.path.join(configuration_directory, 'localization', locale)
        if os.path.exists(locale_directory):
            configuration_file = os.path.join(locale_directory, 'copas.yaml')
            if os.path.exists(configuration_file):
                with open(configuration_file) as f:
                    app.logger.info(f'Config file {configuration_file} for locale {locale} detected. Loading configuration...')
                    try:
                        return yaml.safe_load(f)
                    except yaml.YAMLError:
                        app.logger.error(f'Failed to load configuration {configuration_file}.', exc_info = True)
                        return None
                    
def load_locale_template_translations(locale: str):
    with app.app_context():
        configuration_directory = app.config['CONFIG_DIR']
        locale_directory = os.path.join(configuration_directory, 'localization', locale)
        if os.path.exists(locale_directory):
            translations_file = os.path.join(locale_directory, 'templates.yaml')
            if os.path.exists(translations_file):
                with open(translations_file) as f:
                    app.logger.info(f'Translation file {translations_file} for templates for locale {locale} detected. Loading...')
                    try:
                        yaml_translations = yaml.safe_load(f)
                        for template_key in yaml_translations: 
                            if 'controls' not in yaml_translations[template_key]:
                                yaml_translations[template_key]['controls'] = {}
                            if 'sections' not in yaml_translations[template_key]:
                                yaml_translations[template_key]['sections'] = {}
                        return yaml_translations
                    except yaml.YAMLError:
                        app.logger.error(f'Failed to load translations file {translations_file}.', exc_info = True)
                        return None
