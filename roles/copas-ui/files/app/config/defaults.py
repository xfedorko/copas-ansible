CONFIG_DEFAULTS = {
    'home': {
        'title': 'Unspecified title',
        'description': 'Unspecified description'
    },
    'config': {
        'templates': [],
        'initial': []
    },
    'help': [],
    'module': {
        'type': 'analysis',
        'name': 'module',
        'version': 'version',
        'author': {
            'name': 'Unknown author',
            'email': '',
            'institution': '',
        }
    },
    'container': {
        'name': 'container'
    },
    'analysis': {
        'status': True,
        'ui': {
            'url': 'http://localhost:8081'
        },
        'api': {
            'url': 'http://localhost:8082/api'
        },
        'actions': []
    }
}
