import logging
import os
import yaml
from flask import Flask
from mergedeep import merge

from app.config.defaults import CONFIG_DEFAULTS
from app.config.utils import setInDict

def load_config(app: Flask, config_dir: str, database_path: str):

    if not app.debug:
        gunicorn_logger = logging.getLogger('gunicorn.error')
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)

    app.config['CONFIG_DIR'] = config_dir
    app.config['DATABASE_PATH'] = database_path

    config_file = os.path.join(config_dir, 'copas.yaml')

    if os.path.exists(config_file):
        with open(config_file) as f:
            app.logger.info(f'Config file {config_file} detected. Loading configuration...')
            try:
                config = yaml.safe_load(f)
                app.config.update({
                    'COPAS': merge(CONFIG_DEFAULTS, config)
                })
            except yaml.YAMLError:
                app.logger.error('Failed to load configuration.', exc_info = True)
                raise
    else:
        app.logger.warning(f'No config file {config_file} detected. Using just defaults...')
        app.config.update({
            'COPAS': CONFIG_DEFAULTS
        })
    
    for key, value in os.environ.items():
        if key.startswith('COPAS_'):
            subparts = [s.lower() if s != 'COPAS' else s for s in key.split('_')]
            setInDict(app.config, subparts, value)
    
