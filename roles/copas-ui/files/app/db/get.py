import sqlite3
from flask import current_app as app, g

def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(
            app.config['DATABASE_PATH'],
            detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row
        g.db.execute("PRAGMA foreign_keys = ON")

    return g.db