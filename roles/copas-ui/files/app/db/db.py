import os
import click
import yaml
from flask.cli import with_appcontext
from flask import current_app as app, Flask, g
from app.core.data.configuration_template import insert_static_config_template
from app.db.get import get_db
from app.core.data.saved_configuration_info import insert_saved_configuration_info


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()

def initialize_static_templates():
    config_static_templates = app.config['COPAS']['config']['templates']
    
    names = [t['name'] for t in config_static_templates]
    if len(names) != len(set(names)):
        app.logger.error('There are duplicate names among templates!')
        return
    
    keys = [t['key'] for t in config_static_templates]
    if len(keys) != len(set(keys)):
        app.logger.error('There are duplicate keys among templates!')
        return
    
    paths = [os.path.join(app.config['CONFIG_DIR'], t['path']) for t in config_static_templates]
    missing_paths = [p for p in paths if not os.path.exists(p)]
    if len(missing_paths) > 0:
        app.logger.error('Nonexistent file paths were provided %s', str(missing_paths))
        return

    for t in config_static_templates:
        with open(os.path.join(app.config['CONFIG_DIR'], t['path']), 'r') as template_file:
            template_contents = yaml.safe_load(template_file)
            template_key = t['key']
            insert_static_config_template(template_key, template_contents)
            
def initialize_analytical_configs():
    configurations = app.config['COPAS']['config']['initial']

    names = [c['name'] for c in configurations]
    if len(names) != len(set(names)):
        app.logger.error('There are duplicate names among configurations!')
        return
    paths = [os.path.join(app.config['CONFIG_DIR'], c['path']) for c in configurations]
    missing_paths = [p for p in paths if not os.path.exists(p)]
    if len(missing_paths) > 0:
        app.logger.error('Nonexistent file paths were provided %s', str(missing_paths))
        return

    for c in configurations:
        with open(os.path.join(app.config['CONFIG_DIR'], c['path']), 'r') as config_contents_file:
            config_contents = yaml.safe_load(config_contents_file)
            template_key = c['template_key']
            insert_saved_configuration_info(c['name'], config_contents, template_key=template_key, note=c.get('note'))
            
def initialize_db(app: Flask):
    db = get_db()

    with app.open_resource(os.path.join('db', 'init.sql')) as f:
        script_contents = f.read().decode('utf8')
        db.executescript(script_contents)

    initialize_static_templates()
    initialize_analytical_configs()

@click.command('init-db')
@with_appcontext
def init_db_command():
    initialize_db(app)
    click.echo('Initialized the database.')

@click.command('clear-db')
@with_appcontext
def clear_db_command():
    if os.path.exists(app.config['DATABASE_PATH']):
        os.remove(app.config['DATABASE_PATH'])
        click.echo('Removed the database.')
    else:
        click.echo('Database file does not exist.')

@click.command('list-db')
@with_appcontext
def list_db_command():
    if not os.path.exists(app.config['DATABASE_PATH']):
        click.echo('Database file does not exist.')
        return
    
    db = get_db()

    tables = [
        ('Templates', 'configuration_template'),
        ('Configurations', 'configuration'),
        ('Saved configuration', 'saved_configuration_info'),
        ('Watched directories', 'watch'),
        ('Import items', 'import_item'),
        ('Imported files', 'imported_file')
    ]
    for label, table_name in tables:
        click.echo(f'{label}:')
        cursor = db.cursor()
        cursor.execute(f'SELECT * FROM {table_name}')
        rows = cursor.fetchall()
        for r in rows:
            click.echo(dict(r))

def init_app(app):
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_command)
    app.cli.add_command(clear_db_command)
    app.cli.add_command(list_db_command)

