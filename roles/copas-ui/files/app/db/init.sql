CREATE TABLE IF NOT EXISTS configuration (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    template_contents_id INTEGER NOT NULL,
    contents TEXT NOT NULL,
    created TEXT NOT NULL,
    modified TEXT NOT NULL,
    FOREIGN KEY(template_contents_id) 
        REFERENCES configuration_template_contents(id)
            ON DELETE RESTRICT
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS configuration_template (
    key TEXT PRIMARY KEY,
    type TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS configuration_template_contents (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    contents TEXT NOT NULL,
    created TEXT NOT NULL,
    configuration_template_key TEXT NOT NULL,
    FOREIGN KEY(configuration_template_key) 
        REFERENCES configuration_template(key)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS saved_configuration_info (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    configuration_id INTEGER NOT NULL,
    name TEXT NOT NULL UNIQUE,
    note TEXT,
    created TEXT NOT NULL,
    modified TEXT NOT NULL,
    FOREIGN KEY(configuration_id) 
        REFERENCES configuration(id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS watch (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    configuration_id INTEGER NOT NULL,
    directory_path TEXT NOT NULL,
    created TEXT NOT NULL,
    FOREIGN KEY(configuration_id) 
        REFERENCES configuration(id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS import_item (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    configuration_id INTEGER NOT NULL,
    origin TEXT NOT NULL,
    status TEXT NOT NULL,
    message TEXT,
    start TEXT NOT NULL,
    end TEXT,
    FOREIGN KEY(configuration_id) 
        REFERENCES configuration(id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);

CREATE TABLE IF NOT EXISTS imported_file (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    import_item_id INTEGER NOT NULL,
    archive_path TEXT,
    path TEXT NOT NULL,
    status TEXT NOT NULL,
    FOREIGN KEY(import_item_id) 
        REFERENCES import_item(id)
            ON DELETE CASCADE
            ON UPDATE NO ACTION
);