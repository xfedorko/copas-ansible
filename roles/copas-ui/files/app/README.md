# CopAS UI - the server part

This project is a [Flask](https://flask.palletsprojects.com/en/2.3.x/) application. 

The instructions for running and installing dependencies are in the main README file.

## Project structure

- **\_\_init\_\_.py**: File where the Flask application is defined
- **api**: Place for all api endpoints of the application
- **config**: Where loading of configuration from the configuration directory is handled
- **core**: The core functionality - data management and storage, importing of files, watchdog scheduler
- **db**: Database related stuff - flask init-db and clear-db definition, global session for database
- **tests**: Tests implemented using pytest framework
- **translations**: This is where translations are placed. Localization is implemented using [Flask-Babel](https://python-babel.github.io/flask-babel/).