from flask import Blueprint, current_app as app
import jsonschema

from app.session import get_analysis_session
from app.api.utils import handle_analysis_request_exceptions

status_blueprint = Blueprint('status', __name__, url_prefix='/api/status')

schema = {
    'type': 'object',
    'properties': {
        'services': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'name': {'type': 'string'},
                    'status': {'type': 'string', 'enum': ['up', 'down']},
                    'info': {
                        'type': 'array',
                        'items': {
                            'type': 'object',
                            'properties': {
                                'label': {'type': 'string'},
                                'value': {'type': ['string', 'number']}
                            },
                            'required': ['label', 'value']
                        }
                    }
                },
                'required': ['name', 'status', 'info']
            }
        }
    },
    'required': ['services']
}


@status_blueprint.route('', methods=('GET',))
@handle_analysis_request_exceptions()
def get_status():
    if not app.config['COPAS']['analysis']['status']: 
        return { 'services': [] }, 200 # Status check for analysis is turned off
    analysis_session= get_analysis_session()
    response = analysis_session.get(f'{analysis_session.base_url}/status')
    response.raise_for_status()
    json_data = response.json()
    jsonschema.validate(json_data, schema)
    return json_data

