from flask import Blueprint, request, current_app as app


from flask_babel import gettext
import jsonschema

from app.session import get_analysis_session
from app.api.utils import handle_analysis_request_exceptions

action_blueprint = Blueprint('action', __name__, url_prefix='/api/analysis/action')

schema = {
    'type': 'object',
    'properties': {
        'message': {
            'type': 'string'
        }
    }
}


@action_blueprint.route('', methods=('POST',))
@handle_analysis_request_exceptions()
def execute_action():
    action = request.json

    if action is None:
        return { 'error': gettext('no json body provided') }, 400

    if 'key' not in action:
        return { 'error': gettext('missing attribute \'key\'') }, 400
    
    analysis_session = get_analysis_session()
    response = analysis_session.post(f'{analysis_session.base_url}/action', json={
        'key': action['key']
    })
    response.raise_for_status()
    json_data = response.json()
    jsonschema.validate(json_data, schema)
    return json_data
