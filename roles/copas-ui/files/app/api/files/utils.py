import os
from werkzeug.utils import secure_filename

def generate_unique_filename(path: str, candidate_filename:str):
    '''
    generates a new name for file at path from candidate_filename
    the filename is generated such that there are is no file of the same name at path
    '''
    filename = secure_filename(candidate_filename)
    filename_path = os.path.join(path, filename)
    i = 1
    while os.path.exists(filename_path):
        filename = f'{secure_filename(candidate_filename)}.{i}'
        filename_path = os.path.join(path, filename)
        i += 1
    return filename