import os
import shutil
from flask import request, current_app as app
from app.api.files.utils import generate_unique_filename
from .blueprint import files_blueprint

from flask_babel import gettext

@files_blueprint.route('/move', methods=('POST',))
def move_files():
    '''
    move files fiven by 'path' params to a destination path
    filename conflicts are resolved by generating a new name
    '''
    destination_path = request.args.get('destination', type=str)
    source_paths = request.args.getlist('path')

    if destination_path is None:
        return { 'error': gettext('destination path not provided') }, 400
    
    if source_paths is None or len(source_paths) == 0:
        return { 'error': gettext('no paths provided') }, 400
    
    if not os.path.exists(destination_path):
        return { 'error': gettext('destination does not exist') }, 404
    
    if not os.path.isdir(destination_path):
        return { 'error': gettext('destination is not a directory') }, 400
    
    if not os.access(destination_path, os.W_OK | os.X_OK):
        return { 'error': gettext('don\'t have permission to write to destination') }, 403
    
    for p in source_paths:
        if not os.path.exists(p):
            return { 'error': gettext('source path %(x)s does not exist', x=p) }, 400
        
        dir = os.path.dirname(p)

        if not os.access(dir, os.R_OK | os.W_OK | os.X_OK):
            return { 'error': gettext('don\'t have sufficient permissions to source directory %(x)s', x=dir) }, 403
        
        if not os.access(p, os.R_OK):
            return { 'error': gettext('don\'t have sufficient permissions to read the path %(x)s', x=p) }, 403

    move_failed = False
    for p in source_paths:
        candidate_filename = os.path.basename(p)
        filename = generate_unique_filename(destination_path, candidate_filename)
        destination_file = os.path.join(destination_path, filename)

        try:
            shutil.move(p, destination_file)
            if candidate_filename == filename:
                app.logger.info('moved %s to directory %s', p, destination_path)
            else:
                app.logger.info('moved %s to directory %s under generated name %s', p, destination_path, filename)
        except:
            move_failed = True
            app.logger.error('failed to move %s to directory %s', p, destination_path, exc_info=True)
    
    if move_failed:
            return { 'error': gettext('some files weren\'t moved correctly') }, 500

    return {}, 200