import os
from flask import request, current_app as app
from .blueprint import files_blueprint

from flask_babel import gettext


@files_blueprint.route('/directory', methods=('POST',))
def create_directory():
    '''
    creates a directory named 'name' at path 'path'
    '''
    path = request.args.get('path', type=str)
    name = request.args.get('name', type=str)

    if path is None:
        return { 'error': gettext('path not provided') }, 400
    
    if name is None or len(name) == 0:
        return { 'error': gettext('name not provided') }, 400
    
    if not all([c.isalnum() or c in "_-." for c in name]):
        return { 'error': gettext('name has invalid characters') }, 400
    
    if not os.path.exists(path):
        return { 'error': gettext('path does not exist') }, 404
    
    if not os.access(path, os.W_OK | os.X_OK):
        return { 'error': gettext('directory can\'t be written to - insufficient access rights') }, 403
        
    directory_path = os.path.join(path, name)

    if os.path.exists(directory_path):
        return { 'error': gettext('file with given name already exists') }, 409

    try:
        os.mkdir(directory_path)   
        app.logger.info('created directory %s at path %s', name, path)
        return {}, 201
    except:
        app.logger.error('failed to created directory %s at path %s', name, path)
        return { 'error': gettext('failed to created directory %(x)s at path %(y)s', x=name, y=path) }, 500
