import os
import tarfile
import zipfile
from flask import request, current_app as app
from .blueprint import files_blueprint

from flask_babel import gettext

@files_blueprint.route('', methods=('GET',))
def list_directory():
    '''
    endpoint listing the directory contents
    for each file in a directory given by path lists the following information
        - name of the file
        - type of the file (regular file, directory, archive (zip or tar) or symbolic link)
        - size of the file
        - time the field was modified
    files are sorted first by file type, then by name
    '''
    path = request.args.get('path', type=str)

    if path is None:
        return { 'error': gettext('path not provided') }, 400
    
    if not os.path.exists(path):
        return { 'error': gettext('path does not exist') }, 404

    if not os.path.isdir(path):
        return { 'error': gettext('path is not a directory') }, 400

    if not os.access(path, os.R_OK):
        return { 'error': gettext('directory can\'t be read - missing read access rights') }, 403

    files = []
    try:
        for file in os.scandir(path):
                if os.path.exists(file.path):
                    if file.is_dir():
                        type = 'dir'
                    elif file.is_symlink():
                        type = 'slink'                        
                    elif file.is_file():
                        if os.access(file.path, os.R_OK) and (tarfile.is_tarfile(file.path) or zipfile.is_zipfile(file.path)):
                            type = 'archive'
                        else:
                            type = 'file'
                    else:
                        continue
                    stat = file.stat()
                    files.append({
                        'name': file.name,
                        'type': type,
                        'size': stat.st_size,
                        'modified': stat.st_mtime,
                    })
        sorted_files = sorted(files, key = lambda f: (f['type'], f['name']))

        return sorted_files, 200
    except:
        app.logger.error('failed to read directory %s - file %s', path, file.name, exc_info=True)
        return { 'error': gettext('failed to read directory %(x)s - file %(y)s', x=path, y=file.name) }, 500

