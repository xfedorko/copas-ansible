import os
from flask import request, current_app as app

from .utils import generate_unique_filename
from .blueprint import files_blueprint

from flask_babel import gettext

@files_blueprint.route('', methods=('POST',))
def upload_files():
    '''
    uploads files at a given path
    filename conflicts are resolved by generating a new name
    '''
    path = request.args.get('path', '/', type=str)

    if path is None:
        return { 'error': gettext('path not provided') }, 400
    
    if not os.path.exists(path):
        return { 'error': gettext('path does not exist') }, 400
    
    if not os.path.isdir(path):
        return { 'error': gettext('path is not a directory') }, 400
    
    if not os.access(path, os.W_OK | os.X_OK):
        return { 'error': gettext('insufficient access rights to upload files to path') }, 403
    
    files = list(request.files.items())
    
    if len(files) == 0:
        return { 'error': gettext('no files to upload provided') }, 400

    file_failed = False
    for n, fs in files:
        try:
            filename = generate_unique_filename(path, fs.filename)
            fs.save(os.path.join(path, filename))
            if fs.filename != filename:
                app.logger.info('uploaded file %s at path %s under generated name %s', fs.filename, path, filename)
            else:
                app.logger.info('uploaded file %s at path %s', fs.filename, path)
        except:
            file_failed = True
            app.logger.error('failed to upload file %s at path %s', fs.filename, path, exc_info=True)

    if file_failed:
        return { 'error': gettext('upload of at least one file failed') }, 500
    
    return {}, 200