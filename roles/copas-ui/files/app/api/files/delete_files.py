import os
import shutil
from flask import request, current_app as app
from .blueprint import files_blueprint

from flask_babel import gettext

@files_blueprint.route('', methods=('DELETE',))
def delete_files():
    '''
    deletes files given by paramaters 'name' at a given path 'path'
    only deletes regular files and empty directories
    requests to delete directories with contents are ignored
    '''
    path = request.args.get('path', type=str)
    files = request.args.getlist('name')

    if path is None:
        return { 'error': gettext('path not provided') }, 400
    
    if files is None or len(files) == 0:
        return { 'error': gettext('no file provided') }, 400
    
    if not os.path.exists(path):
        return { 'error': gettext('nonexistent path') }, 404
    
    if not os.path.isdir(path):
        return { 'error': gettext('path is not a directory') }, 400
    
    if not os.access(path, os.R_OK | os.W_OK | os.X_OK):
        return { 'error': gettext('insufficient rights to delete files in the directory') }, 403

    paths_to_delete = [os.path.join(path, f) for f in files if os.path.exists(os.path.join(path, f))]
    if len(paths_to_delete) != len(files):
        return { 'error': gettext('some nonexistent paths to delete were provided') }, 404

    failed_to_delete = False
    for p in paths_to_delete:
        try:
            if os.path.isfile(p):
                os.remove(p)
                app.logger.info('deleted file %s', p)
            elif os.path.isdir(p):
                if len(os.listdir(p)) == 0:
                    os.rmdir(p)
                    app.logger.info('deleted empty directory %s', p)
                else:
                    shutil.rmtree(p)                      
                    app.logger.info('deleted nonempty directory %s', p)
        except:
            failed_to_delete = True
            app.logger.error('attempt to delete file %s failed', p, exc_info=True)

    if failed_to_delete:
        return { 'error': gettext('error occured when attempting to delete some files') }, 400

    return {}, 200