import os
import shutil
from flask import request, current_app as app
from .utils import generate_unique_filename
from .blueprint import files_blueprint

from flask_babel import gettext

@files_blueprint.route('/copy', methods=('POST',))
def copy_files():
    '''
    copy files fiven by 'path' params to a destination path
    filename conflicts are resolved by generating a new name
    '''
    destination_path = request.args.get('destination', type=str)
    source_paths = request.args.getlist('path')

    if destination_path is None:
        return { 'error': gettext('destination path not provided') }, 400
    
    if source_paths is None or len(source_paths) == 0:
        return { 'error': gettext('no source_paths provided') }, 400
    
    if not os.path.exists(destination_path):
        return { 'error': gettext('destination does not exist') }, 404
    
    if not os.path.isdir(destination_path):
        return { 'error': gettext('destination is not a directory') }, 400
    
    if not os.access(destination_path, os.W_OK | os.X_OK):
        return { 'error': gettext('don\'t have permission to write to destination') }, 403
    
    for p in source_paths:
        if not os.path.exists(p):
            return { 'error': gettext('source path %(x)s does not exist', x=p) }, 400
        
        dir = os.path.dirname(p)

        if not os.access(dir, os.R_OK | os.X_OK):
            return { 'error': gettext('don\'t have permission to read from directory %(x)s', x=dir) }, 403
        
        if not os.access(p, os.R_OK):
            return { 'error': gettext('don\'t have permission to read the file %(x)s', x=p) }, 403

    copy_failed = False

    for p in source_paths:
        candidate_filename = os.path.basename(p)
        filename = generate_unique_filename(destination_path, candidate_filename)
        destination_file = os.path.join(destination_path, filename)
        
        try:
            if os.path.isfile(p):
                shutil.copy(p, destination_file)
            elif os.path.isdir(p):
                shutil.copytree(p, destination_file)
            
            if candidate_filename == filename:
                app.logger.info('copied %s to directory %s', p, destination_path)
            else:
                app.logger.info('copied %s to directory %s under generated name %s', p, destination_path, filename)

        except:
            copy_failed = True
            app.logger.error('failed to copy %s to directory %s', p, destination_path, exc_info=True)
    
    if copy_failed:
            return { 'error': gettext('some files weren\'t copied correctly') }, 500
    
    return {}, 200