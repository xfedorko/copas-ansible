from flask import Blueprint

config_blueprint = Blueprint('config', __name__, url_prefix='/api/config')
