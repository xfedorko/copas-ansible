from flask import request
from flask_restful import Resource
from flask_babel import gettext

from app.core.data.configuration import get_configuration, update_configuration


class ConfigResource(Resource):
    
    def get(self, id: int):
        c = get_configuration(id) 
        return (c, 200) if c is not None else ({ 'error': gettext('configuration with id "%(x)d" does not exist', x=id) }, 404)

    def patch(self, id: int):
        contents = request.json
        if id is None:
            return { 'error': gettext('config id not provided') }, 400
        if contents is None:
            return { 'error': gettext('config contents not provided') }, 400

        return update_configuration(int(id), contents)