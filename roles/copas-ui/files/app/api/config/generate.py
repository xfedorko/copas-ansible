from flask import request
from flask_babel import gettext
import jsonschema

from app.session import get_analysis_session
from app.api.utils import handle_analysis_request_exceptions
from .blueprint import config_blueprint

schema = {
    'type': 'object',
    'properties': { }
}

@config_blueprint.route('generate', methods=('GET',))
@handle_analysis_request_exceptions()
def generate_config_contents():
    template_key = request.args.get('key', None)
    path = request.args.get('path', None)

    if template_key is None or len(template_key) == 0:
        return { 'error': gettext('no template key body provided') }, 400

    if path is None:
        return { 'error': gettext('missing file to generate from') }, 400
    
    analysis_session = get_analysis_session()
    response = analysis_session.get(f'{analysis_session.base_url}/config/generate?key={template_key}&path={path}')
    response.raise_for_status()
    json_data = response.json()
    jsonschema.validate(json_data, schema)
    return json_data