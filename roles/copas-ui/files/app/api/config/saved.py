from flask import request, current_app as app
from flask_restful import Resource
from flask_babel import gettext
from jsonschema import ValidationError, validate
from app.core.data.saved_configuration_info import delete_saved_configuration_info, get_all_saved_configuration_infos, insert_saved_configuration_info

saved_config_schema = {
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'contents': {'type': 'object'},
        'template_contents_id': {'type': 'number'},
        'note': {'type': 'string'}
    },
    'required': ['name', 'contents', 'template_contents_id']
}

class SavedConfigInfoResource(Resource):

    def get(self):
        return get_all_saved_configuration_infos()
    
    def post(self):
        config = request.json
        try:
            validate(instance=config, schema=saved_config_schema)
        except ValidationError:
            app.logger.warning('invalid saved config info json provided', exc_info=True)
            return { 'error': gettext('invalid json format') }, 400
        
        return insert_saved_configuration_info(config['name'], config['contents'], 
                                               template_contents_id=int(config['template_contents_id']), note = config.get('note'))
    

    def delete(self):
        name = request.args.get('name', type=str)
        if name is None:
            return {'error': gettext('no config name provided') }, 400
        return delete_saved_configuration_info(name)
