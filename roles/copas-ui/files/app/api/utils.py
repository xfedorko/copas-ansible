import functools
import json
import jsonschema

import requests

from flask import current_app as app
from flask_babel import gettext

def handle_analysis_request_exceptions():
    def decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)

            except requests.exceptions.HTTPError as e:
                app.logger.error(f'request to the analysis failed with HTTP error: {str(e)}')
                error_message =  gettext('request to the analysis failed with HTTP error of status %(x)d', x=e.response.status_code)
                if e.response.headers.get('Content-Type') == 'application/json':
                    error_json = e.response.json()
                    if isinstance(error_json, dict) and 'error' in error_json:
                        error_message += gettext(' and error message - %(x)s', x=error_json['error'])
                return {'error': error_message}, e.response.status_code
            except json.decoder.JSONDecodeError:
                app.logger.error('analysis sent an invalid json response')
                return {'error': gettext('analysis sent an invalid json response')}, 503
            except jsonschema.ValidationError:
                app.logger.error('analysis sent an invalid json response that did not match the expected schema')
                return {'error': gettext('analysis sent an invalid json response that did not match the expected schema')}, 503
            except requests.exceptions.ConnectionError:
                app.logger.error('analysis is unreachable')
                return { 'error': gettext('analysis is unreachable') }, 503
            except requests.exceptions.RequestException as e:
                app.logger.error(f'request to the analysis failed: {str(e)}')
                return {'error': gettext('request to the analysis failed')}, 500
            except Exception as e:
                app.logger.error(f'An unexpected error occurred: {str(e)}')
                return {'error': gettext('an unexpected error occurred')}, 500
        return wrapper
    return decorator
