import os
import tarfile
import zipfile

from flask import request
from .blueprint import import_blueprint

from flask_babel import gettext

def get_contents(file: str):
    if os.path.isdir(file):
        return [f.name for f in os.scandir(file) if os.path.isfile(f) \
                        and not f.name.startswith('.')]
    elif tarfile.is_tarfile(file):
        with tarfile.open(file) as tar:
            return [m.name for m in tar if m.isfile()]
    elif zipfile.is_zipfile(file):
        return zipfile.ZipFile(file).namelist()
    return None

def get_file_type(file: str):
    if os.path.isdir(file):
        return 'dir'
    elif tarfile.is_tarfile(file) or zipfile.is_zipfile(file):
        return 'archive'
    return 'file'

def file_has_contents(file_type: str):
    return file_type != 'file'

@import_blueprint.route('', methods=('GET',))
def file_info():
    ''' 
    gets basic info about a given file
     - file type (regular file, directory, archive)
     - path
     - contents (if directory or archive)
    '''
    file = request.args.get('file', type=str)

    if file is None:
        return { 'error': gettext('file not provided') }, 400
    
    if not os.path.exists(file):
        return { 'error': gettext('file does not exist') }, 400
    
    file_type = get_file_type(file)
    res = {
        'type': file_type,
        'path': file,
    }
    
    if file_has_contents(file_type):
        if not os.access(file, os.R_OK):
            return { 'error': gettext('can\' read the contents because of missing read access') }, 403
        res['contents'] = get_contents(file)

    return res, 200
