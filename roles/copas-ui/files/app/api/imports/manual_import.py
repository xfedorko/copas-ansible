import os
import tarfile
from typing import List
import zipfile
from flask import request, current_app as app
from jsonschema import ValidationError, validate
from app.core.data.watch import insert_watch_item
from app.core.imports import perform_import
from app.core.data.saved_configuration_info import insert_saved_configuration_info
from app.core.data.configuration_template import insert_dynamic_config_template_contents
from .blueprint import import_blueprint

from flask_babel import gettext

manual_import_schema = {
    'type': 'object',
    'properties': {
        'files': {'type': 'array'},
        'archives': {'type': 'array'},
        'config': {'type': 'object'},
        'template': {
            'type': 'object',
            'properties': {
                'key': {'type': 'string'},
                'type': {'type': 'string'},
                'contents_id': {'type': 'number'},
                'contents' : {'type': 'object'}
            },
            'required': ['key', 'type']
        },
        'watch': {'type': 'array'},
        'save': {
            'type': 'object',
            'properties': {
                'name': {'type': 'string'},
                'note': {'type': 'string'}
            },
            'required': ['name']
        }
    },
    'required': ['files', 'archives', 'config', 'template']
}

@import_blueprint.route('', methods=('POST',))
def manual_import():
    '''
    this endpoint performs manual import to the analysis
    it exptects json object with the following properties:
        - files - array of paths of files for analysis
        - archives - array of archives represented by an object with properties
            - path - path to archive
            - contents - files inside the archive to analyse
        - config - config contents
        - template 
            - type one of 'static' or 'dynamic'
            - key - template key
            - contents_id - in case the contents are already stored (static type always, dynamic may be)
            - contents - only in case of dynamic type (template contents were generated and should be stored in db)
        - watch - optional array of paths containing directories to start watching with provided configuration
        - save - if provided, saves the configuration if the import doesn't fail
            - name - name of the configuration
            - note 
    '''
    import_data = request.json
    try:
        validate(instance=import_data, schema=manual_import_schema)
    except ValidationError:
        app.logger.warning('invalid data passed to import', exc_info=True)
        return { 'error': gettext('invalid json format') }, 400

    config_contents = import_data['config']
    template = import_data['template']
    directories_for_watch = import_data.get('watch')
    save_name, save_note = None, None
    if 'save' in import_data:
        if 'name' in import_data['save']:
            save_name = import_data['save']['name']
        if 'note' in import_data['save']:
            save_note = import_data['save']['note']

    archives: List = import_data['archives']
    for archive in archives:
        archive_path = archive['path']
        if not tarfile.is_tarfile(archive_path) and not zipfile.is_zipfile(archive_path):
            return { 'error': gettext('invalid archive %(x)s', x=archive_path) }, 400

    files: List = import_data['files']
    regular_files = []
    for file in files:
        if tarfile.is_tarfile(file) or zipfile.is_zipfile(file):
            archives.append({ 'path': file })
        elif os.path.isfile(file):
            regular_files.append(file)
        else:
            return { 'error': gettext('invalid regular file %(x)s', x=file) }, 400
    
    if template['type'] == 'static':
        template_contents_id = int(template['contents_id'])
    else:
        raise TypeError('Unsupported template type')

    response, status = perform_import(regular_files, archives, config_contents, template_contents_id, origin = 'manual')

    if status >= 200 and status < 300:
        if save_name:
            r, s = insert_saved_configuration_info(save_name, config_contents, 
                                                   template_contents_id=template_contents_id, note = save_note)
            if s >= 400:
                return r, s
        
        if directories_for_watch and len(directories_for_watch) > 0:
            for d in directories_for_watch:
                insert_watch_item(d, config_contents, template_contents_id)

    return response, status