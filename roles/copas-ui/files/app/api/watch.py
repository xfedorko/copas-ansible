
import os
from flask import request, current_app as app
from flask_restful import Resource
from jsonschema import ValidationError, validate

from app.core.data.watch import delete_watch_item, get_all_watch_items, insert_watch_item

from flask_babel import gettext

watch_schema = {
    'type': 'object',
    'properties': {
        'directory_path': {'type': 'string'},
        'configuration_id': {'type': 'number'}
    },
    'required': ['directory_path', 'configuration_id']
}

class WatchResource(Resource):
    
    def get(self):
        return get_all_watch_items()

    def post(self):
        config = request.json

        try:
            validate(instance=config, schema=watch_schema)
        except ValidationError:
            app.logger.warning('invalid watch item json provided', exc_info=True)
            return { 'error': gettext('invalid json format') }, 400
        directory_path = config['directory_path']
        configuration_id = config['configuration_id']

        if not os.path.exists(directory_path):
            return { 'error': gettext('directory %(x)s does not exist', x=directory_path) }, 400
        
        if not os.path.isdir(directory_path):
            return { 'error': gettext('path %(x)s is not a directory', x=directory_path) }, 400

        if not os.access(directory_path, os.R_OK | os.W_OK | os.X_OK):
            return { 'error': gettext('insufficient access rights for the directory') }, 403

        return insert_watch_item(directory_path, configuration_id, template_contents_id=None)
    
    def delete(self):
        id = request.args.get('id', type=str)

        if id is None:
            return { 'error': gettext('no watch id provided') }, 400
    
        return delete_watch_item(int(id))

