from flask import request
import jsonschema

from app.session import get_analysis_session
from app.api.utils import handle_analysis_request_exceptions
from .blueprint import profile_blueprint

from flask_babel import gettext

schema = {
    'type': 'object',
    'properties': { }
}

@profile_blueprint.route('', methods=('POST',))
@handle_analysis_request_exceptions()
def update_profile():
    profile = request.json

    if profile is None:
        return { 'error': gettext('no json body provided') }, 400

    if 'name' not in profile:
        return { 'error': gettext('missing attribute \'name\'') }, 400
    
    analysis_session = get_analysis_session()
    response = analysis_session.post(f'{analysis_session.base_url}/profile', json = {
        'name': profile['name']
    })
    response.raise_for_status()
    json_data = response.json()
    jsonschema.validate(json_data, schema)
    return json_data
