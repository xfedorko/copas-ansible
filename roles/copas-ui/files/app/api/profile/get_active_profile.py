import jsonschema

from app.session import get_analysis_session
from app.api.utils import handle_analysis_request_exceptions
from .blueprint import profile_blueprint

schema = {
    'type': 'object',
    'properties': {
        'name': {
            'type': 'string'
        }
    },
    'required': [ 'name' ]
}

@profile_blueprint.route('', methods=('GET',))
@handle_analysis_request_exceptions()
def get_active_profile():
    analysis_session = get_analysis_session()
    response = analysis_session.get(f'{analysis_session.base_url}/profile')
    response.raise_for_status()
    json_data = response.json()
    jsonschema.validate(json_data, schema)
    return json_data
