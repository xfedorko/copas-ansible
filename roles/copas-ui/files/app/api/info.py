import os
from typing import Dict, List
from flask import Blueprint, current_app as app
from mergedeep import merge, Strategy
from app.core.data.configuration_template import get_all_static_config_templates
from app.config.locale import get_locale, get_supported_languages, load_locale_config_if_exists

info_blueprint = Blueprint('info', __name__, url_prefix='/api/info')

def get_required_environment_variable(name: str, default: str):
    ev = os.getenv(name)
    if ev is None:
        app.logger.warning(f'Environment variable {name} not set, setting default')
        return default
    return ev

def get_dictionary_item(dict: Dict, keys: List[str]):
    try:
        d = dict
        for k in keys:
            d = d[k]
        return d
    except KeyError:
        return None

@info_blueprint.route('', methods=('GET',))
def get_info():
    copas_config = app.config['COPAS']
    locale = get_locale()
    locale_copas_config = load_locale_config_if_exists(locale)
    if locale_copas_config is not None:
        copas_config = merge({}, copas_config, locale_copas_config, strategy=Strategy.REPLACE)
        
    module_type = copas_config['module']['type']
    info = {
        'module_type': module_type,
        'module_name': copas_config['module']['name'],
        'module_version': copas_config['module']['version'],
        'container_name': copas_config['container']['name'],
        'home_title': copas_config['home']['title'],
        'home_description': copas_config['home']['description'],
        'module_author_name': copas_config['module']['author']['name'],
        'module_author_email': copas_config['module']['author']['email'],
        'module_author_institution': copas_config['module']['author']['institution'],
        'analysis_url': copas_config['analysis']['ui']['url'],
    }

    info['section_left'] = get_dictionary_item(copas_config, ['home', 'sections', 'left'])
    info['section_right'] = get_dictionary_item(copas_config, ['home', 'sections', 'right'])

    info['background'] = get_dictionary_item(copas_config, ['theme', 'background', 'image'])
    info['background_style'] = get_dictionary_item(copas_config, ['theme', 'background', 'style'])

    info['help_sections'] = []
    for m in copas_config['help']:
        section = {}
        section['name'] = m['name']
        markdown_path = os.path.join(app.config['CONFIG_DIR'], m['path'])
        if os.path.exists(markdown_path):
            with open(markdown_path, 'r') as markdown_file:
                section['contents'] = markdown_file.read()
            info['help_sections'].append(section)
        else:
            app.logger.error('The help file %s does not exist', markdown_path)

    info['analysis_actions'] = get_dictionary_item(copas_config, ['analysis', 'actions'])
    info['analysis_profile'] = get_dictionary_item(copas_config, ['analysis', 'profile'])
    
    if module_type == 'analysis':
        info['config_templates'] = [
            merge({ 'generation': False }, info_from_config, info_from_db) \
            for info_from_config, info_from_db \
            in zip(copas_config['config']['templates'], get_all_static_config_templates(locale))
        ]

    info['supported_languages'] = get_supported_languages()

    return info
    