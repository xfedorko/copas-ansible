from flask import Blueprint

history_blueprint = Blueprint('history', __name__, url_prefix='/api/history')