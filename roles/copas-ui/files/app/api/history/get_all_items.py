
from .blueprint import history_blueprint
from app.core.data.history import get_all_history_items

@history_blueprint.route('', methods=('GET',))
def get_all_items():
    return get_all_history_items()