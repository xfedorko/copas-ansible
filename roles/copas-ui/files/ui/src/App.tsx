import React, { lazy, useEffect, useState } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './defs.css';
import styles from './App.module.css';
import Statusbar from './components/Statusbar/Statusbar';
import Home from './pages/Home/Home';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Main from './pages/Main/Main';
import { ReactQueryDevtools } from 'react-query/devtools';
import { Alert, Spin, Typography } from 'antd';
import CopasLogo from './images/copas-logo.svg';
import { SERVER_URL } from './clients/url';
import { useModuleInfo } from './core/info/hooks';
import { ModuleInfoContext } from './core/info/context';
import { PathContext } from './core/files/context';
import { useTranslation } from 'react-i18next';
import i18n from './i18n';

const Files = lazy(() => import('./pages/Files/Files'));
const Help = lazy(() => import('./pages/Help/Help'));
const History = lazy(() => import('./pages/History/History'));
const Config = React.lazy(() => import('./pages/Config/Config'));
const Watch = React.lazy(() => import('./pages/Watch/Watch'));
const Import = React.lazy(() => import('./pages/Import/Import'));
const SelectFilesImport = React.lazy(() => import('./pages/Import/SelectFiles/SelectFilesImport'));
const ChooseConfigImport = React.lazy(() => import('./pages/Import/ChooseConfig/ChooseConfigImport'));
const ModifyConfigImport = React.lazy(() => import('./pages/Import/ModifyConfig/ModifyConfigImport'));
const SummaryImport = React.lazy(() => import('./pages/Import/Summary/SummaryImport'));
const CreateConfig = React.lazy(() => import('./pages/Config/Create/CreateConfig'));
const DetailConfig = React.lazy(() => import('./pages/Config/Detail/DetailConfig'));
const EditConfig = React.lazy(() => import('./pages/Config/Edit/EditConfig'));
const CreateWatch = React.lazy(() => import('./pages/Watch/Create/CreateWatch'));

function App() {
    const { moduleInfo, isLoading, isError, refetch } = useModuleInfo();
    const [activePath, setActivePath] = useState('/');

    useEffect(() => {
        const languageListener = () => {
            i18n.changeLanguage(window.navigator.language).then(() => refetch());
        };
        window.addEventListener('languagechange', languageListener);
        return () => window.removeEventListener('languagechange', languageListener);
    }, []);

    useEffect(() => {
        if (moduleInfo) {
            document.title = moduleInfo.containerName;
        }
    },[moduleInfo]);
    const { t } = useTranslation();

    if (isLoading || isError || !moduleInfo) {
        return (
            <div className={styles['loading-screen']}>
                <img src={CopasLogo} alt='CoPAS logo'/>
                <div className={styles['copas-text']}>
                    <Typography>{t('home.logo.slogan')}</Typography>
                </div>
                {
                    isLoading ? (<Spin tip='Loading' size='large'/>) : (
                        <Alert message='Error' type='error' showIcon
                            description={t('other.info_load_error')}/>
                    )
                }
            </div>
        );
    }

    
    const moduleType = moduleInfo?.moduleType || 'analysis';

    return (
        <>
            <ModuleInfoContext.Provider value={{ ...moduleInfo }}>
                <PathContext.Provider value={{ path: activePath, setPath: setActivePath }}>
                    <div className={styles.app} style={{
                        backgroundImage: moduleInfo.background ? `url("${SERVER_URL}/analysis/static/${moduleInfo.background}")` : 'url(\'background.svg\')',
                        ...moduleInfo.backgroundStyle
                    }}>
                        <BrowserRouter>
                            <Routes>
                                <Route path='/' element={<Home/>}/>
                                <Route path='/*' element={<Main/>}>
                                    <Route path='files' element={<Files/>}/>
                                    {
                                        moduleType === 'analysis' && (
                                            <>
                                                <Route path='import' element={<Import/>}>
                                                    <Route index element={<SelectFilesImport/>} />
                                                    <Route path='config' element={<ChooseConfigImport/>} />
                                                    <Route path='create' element={<ModifyConfigImport/>} />
                                                    <Route path='summary' element={<SummaryImport/>} />
                                                </Route>
                                                <Route path='config' element={<Config/>}/>
                                                <Route path='config/create/:templateKey' element={<CreateConfig/>}/>
                                                <Route path='config/detail/:id' element={<DetailConfig/>} />
                                                <Route path='config/edit/:id' element={<EditConfig/>}/>
                                                <Route path='watch' element={<Watch/>}/>
                                                <Route path='watch/create' element={<CreateWatch/>}/>
                                                <Route path='history' element={<History/>}/>
                                            </>

                                        )
                                    }    
                                    <Route path='help' element={<Help/>}/>
                                </Route>
                            </Routes>
                        </BrowserRouter>
                        <Statusbar/>
                    </div>
                    <ToastContainer position='bottom-right' autoClose={2000} hideProgressBar={true}/>
                    <ReactQueryDevtools initialIsOpen={false} position='top-left' />
                </PathContext.Provider>
            </ModuleInfoContext.Provider>
        </>
    );
}

export default App;
