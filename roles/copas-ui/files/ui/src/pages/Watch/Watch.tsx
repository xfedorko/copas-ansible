

import React from 'react';
import styles from './Watch.module.css';
import Surface from '../../components/Surface/Surface';
import SurfaceHeader from '../../components/Surface/SurfaceHeader';
import { Tooltip, Button, List, Popconfirm } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import SurfaceBody from '../../components/Surface/SurfaceBody';
import { useNavigate } from 'react-router-dom';
import BackButton from '../../components/Buttons/BackButton';
import { useDeleteWatchItem, useWatchItems } from '../../core/watch/hooks';
import { useTranslation } from 'react-i18next';

const Watch : React.FC = () => {
    const { watchItems, isLoading, isError } =  useWatchItems();
    const { deleteWatchItem } = useDeleteWatchItem();
    const navigate = useNavigate();
    const { t } = useTranslation();

    return (
        <div className={styles.container}>
            <Surface className={styles.watchList}>
                <SurfaceHeader
                    leftSection={<BackButton route='/'/>}
                    rightSection={
                        <Tooltip title={t('watch.add_button.tooltip')}>
                            <Button type='primary' shape='round' icon={<PlusOutlined/>} 
                                style={{marginLeft: '2em'}} onClick={() => navigate('/watch/create')}
                            >{t('watch.add_button.label')}</Button>
                        </Tooltip>
                    }
                >
                    <h2>{t('watch.title')}</h2>
                </SurfaceHeader>
                <SurfaceBody isLoading={isLoading} isError={isError}>
                    <List dataSource={watchItems} itemLayout='horizontal' style={{minHeight: '100%'}}
                        renderItem={item => (
                            <List.Item actions={[
                                <Tooltip key={item.id} title={t('watch.view_config_button.tooltip')}>
                                    <Button type='ghost' shape='round' onClick={() => navigate(`/config/detail/${item.configurationId}`)}>{t('watch.view_config_button.label')}</Button>
                                </Tooltip>,
                                <Tooltip key={item.id} title={t('watch.remove_button.tooltip')}>
                                    <Popconfirm title={t('watch.remove_button.modal.text')} 
                                        okText={t('watch.remove_button.modal.confirm')}
                                        cancelText={t('watch.remove_button.modal.cancel')}
                                        onConfirm={() => deleteWatchItem(item.id)}
                                    >
                                        <Button type='ghost' shape='round'>{t('watch.remove_button.label')}</Button>
                                    </Popconfirm>
                                </Tooltip>
                            ]}>
                                {item.directoryPath}
                            </List.Item>
                        )}/>
                </SurfaceBody>
            </Surface>
        </div>
    );
};

export default Watch;