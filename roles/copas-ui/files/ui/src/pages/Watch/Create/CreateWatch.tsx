import React, { useContext, useState } from 'react';
import styles from './CreateWatch.module.css';
import Surface from '../../../components/Surface/Surface';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import { Button, Form, Input, Select } from 'antd';
import FileManager from '../../../components/FileManager/FileManager';
import { useSavedConfigs } from '../../../core/config/hooks';
import { SavedConfigItem } from '../../../core/config/model';
import { PathContext } from '../../../core/files/context';
import { useCreateWatchItem } from '../../../core/watch/hooks';
import { useTranslation } from 'react-i18next';

const CreateWatch = () => {
    const { path, setPath, } = useContext(PathContext);
    const [selectedDirectory, setSelectedDirectory] = useState<string>();
    const [selectedConfigId, setSelectedConfigId] = useState<number>();
    const { savedConfigItems, isLoading } = useSavedConfigs();
    const { createWatchItem } = useCreateWatchItem();
    const { t } = useTranslation();
    
    const configOptions = savedConfigItems?.map((c: SavedConfigItem) => ({ label: c.name, value: c.configurationId }));

    return (
        <div className={styles.container}>
            <FileManager className={styles.filePicker} path={path} onPathChange={setPath} readonly={true}
                onSelectionChange={(files) => {
                    if (files.length === 0 || files[0].type !== 'dir') {
                        setSelectedDirectory(undefined);
                    } else {
                        setSelectedDirectory(path === '/' ? `/${files[0].name}` : `${path}/${files[0].name}`);
                    }
                }} />
            <Surface className={styles.controls}>
                <SurfaceBody style={{marginTop: '1em' }}>
                    <Form
                        name="basic"
                        labelCol={{ md: { span: 4, offset:2 }}}
                        wrapperCol={{ md: { span: 14 }}}
                        autoComplete="off"
                    >
                        <Form.Item
                            label={t('watch.create.form.directory.label')}
                            rules={[
                                {
                                    required: true,
                                    message: t('watch.create.form.directory.rules.required') || undefined
                                }
                            ]}
                        >
                            <Input value={selectedDirectory} contentEditable={false} />
                        </Form.Item>
                        <Form.Item
                            label={t('watch.create.form.configuration.label')}>
                            <Select
                                showSearch
                                loading={isLoading}
                                optionFilterProp="children"
                                filterOption={(input, option) => (option?.label ?? '').includes(input)}
                                filterSort={(optionA, optionB) =>
                                    (optionA?.label ?? '').toLowerCase().localeCompare((optionB?.label ?? '').toLowerCase())
                                }
                                onChange={(v) => {
                                    setSelectedConfigId(v);
                                }}
                                options={configOptions}
                            />
                        </Form.Item>
                        <Form.Item wrapperCol={{ md: { offset: 9, span: 15 }}}>
                            <Button type="primary" shape='round' disabled={!selectedDirectory || !selectedConfigId}
                                onClick={() => {
                                    if (selectedDirectory && selectedConfigId) {
                                        createWatchItem({ directoryPath: selectedDirectory, configurationId: selectedConfigId });
                                    }
                                }}>
                                {t('watch.create.form.button')}
                            </Button>
                        </Form.Item>
                    </Form>
                </SurfaceBody>
            </Surface>
        </div>
    );
};

export default CreateWatch;