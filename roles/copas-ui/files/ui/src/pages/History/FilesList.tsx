import { Tag, Typography } from 'antd';
import React from 'react';
import { statusColor } from '../../core/history/utils';
import { ArchiveImportStatus, FileImportStatus } from '../../core/history/model';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileZipper } from '@fortawesome/free-solid-svg-icons';

const FilesList : React.FC<{files: FileImportStatus[], archives: ArchiveImportStatus[], expanded?: boolean}> = ({files, archives, expanded}) => (
    <div style={{ marginLeft: expanded ? '32em' : '0' }}>
        {files.map(({path, status}) => (
            <Typography key={path} style={{marginBottom: '0.5em'}}>
                <Tag color={statusColor(status)}>
                    {path}
                </Tag>
            </Typography>
        ))}
        {
            archives.map(({ archivePath, contents }) => (
                <div key={archivePath}>
                    <Typography style={{marginBottom:  '0.5em'}}>
                        <Tag icon={<FontAwesomeIcon icon={faFileZipper} />}>
                            {` ${archivePath}`}
                        </Tag>
                    </Typography>
                    <>
                        {
                            contents.map(({path, status}) => (
                                <Typography key={`${archivePath}${path}`} style={{marginLeft: '1em', marginBottom:  '0.5em' }}>
                                    <Tag color={statusColor(status)}>
                                        {path}
                                    </Tag>
                                </Typography>
                            ))
                        }
                    </>
                </div>
            ))
        }
    </div>
);

export default FilesList;