import React from 'react';
import styles from './History.module.css';
import Table from 'antd/lib/table';
import Surface from '../../components/Surface/Surface';
import SurfaceHeader from '../../components/Surface/SurfaceHeader';
import SurfaceBody from '../../components/Surface/SurfaceBody';
import { Input } from 'antd';
import BackButton from '../../components/Buttons/BackButton';
import { shouldBeExpanded } from '../../core/history/utils';
import { useHistoryItems } from '../../core/history/hooks';
import { getHistoryItemsTableRows } from '../../core/history/table';
import FilesList from './FilesList';
import { useTableColumns } from './table';
import { useTranslation } from 'react-i18next';



const History = () => {
    const { filteredHistoryItems, isLoading, isError, filterFiles } = useHistoryItems();

    const tableRows = getHistoryItemsTableRows(filteredHistoryItems);
    const tableColumns = useTableColumns();
    const { t } = useTranslation();
    
    return (
        <div className={styles.container}>
            <Surface className={styles.historyList}>
                <SurfaceHeader
                    leftSection={<BackButton route='/'/>}
                    rightSection={<Input.Search placeholder={t('history.search.placeholder') || undefined} onSearch={filterFiles}/>}
                >
                    <h2>{t('history.title')}</h2>
                </SurfaceHeader>
                <SurfaceBody isLoading={isLoading} isError={isError}>
                    <Table 
                        size='small'
                        pagination={false}
                        dataSource={tableRows}
                        columns={tableColumns}
                        expandable={{
                            expandedRowRender: (r) => <FilesList files={r.files} archives={r.archives} expanded/>,
                            rowExpandable: (r) => shouldBeExpanded(r) ,
                        }}
                    />
                </SurfaceBody>
            </Surface>
        </div>
    );
};


export default History;