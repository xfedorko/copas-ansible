import React from 'react';
import { Button, Tag, Tooltip } from 'antd';
import { formatDate, getMonthAgo, getToday, getWeekAgo, getYesterday } from '../../utils';
import { RowData } from '../../core/history/model';
import Table, { ColumnsType } from 'antd/lib/table';
import { useNavigate } from 'react-router-dom';
import { archivesCount, getDurationString, importedFilesCount, importedFilesCountWithStatus, shouldBeExpanded, statusColor } from '../../core/history/utils';
import styles from './History.module.css';
import { useDuration } from '../../core/history/hooks';
import FilesList from './FilesList';
import { useTranslation } from 'react-i18next';

const DurationFinal: React.FC<{start: Date, end: Date}> = ({ start, end }) => {
    return <span>{getDurationString(start, end)}</span>;
};

const DurationProgress: React.FC<{start: Date}> = ({ start }) => {
    const duration = useDuration(start);
    return <span>{ duration }</span>;
};


export const useTableColumns = () => {
    const navigate = useNavigate();
    const { t } = useTranslation();
    const columns: ColumnsType<RowData> = [
        {
            title: t('history.columns.finished_time.name'),
            width: '10em',
            sorter: (a: RowData, b: RowData) => {
                if (!a.end && !b.end) return a.start.getTime() - b.start.getTime();
                if (!a.end) return 1;
                if (!b.end) return -1;
                return a.end.getTime() - b.end.getTime();
            },
            render: (item: RowData) => item.end ? formatDate(item.end) : '',
            filters: [
                {
                    text: t('history.columns.finished_time.filters.today'),
                    value: 'today',
                },
                {
                    text: t('history.columns.finished_time.filters.yesterday'),
                    value: 'yesterday',
                },
                {
                    text: t('history.columns.finished_time.filters.last_week'),
                    value: 'lweek',
                },
                {
                    text: t('history.columns.finished_time.filters.last_month'),
                    value: 'lmonth',
                },
            ],
            onFilter: (value, item: RowData) => {
                const d = item.end ? item.end : new Date();
                if (value === 'today') {
                    return getToday().getDate() === (d as Date).getDate();
                } else if (value === 'yesterday') {
                    return getYesterday().getDate() === (d as Date).getDate(); 
                } else if (value === 'lweek') {
                    return getWeekAgo() < (d as Date);
                } else if (value === 'lmonth') {
                    return getMonthAgo() < (d as Date);
                }
                return true;
            }
        },
        {
            title: t('history.columns.status.name'),
            width: '6em',
            render: (item) => {
                const pulsing = item.status === 'extracting' || item.status === 'importing';
                return (
                    <Tooltip title={item?.message}>
                        <Tag color={statusColor(item.status)} className={ pulsing ? styles['pulsing'] : '' }>
                            {t(`history.columns.status.values.${item.status}`)}
                        </Tag>
                    </Tooltip>
                );
            },
            filters: [
                {
                    text: t('history.columns.status.filters.failure'),
                    value: 'failure',
                },
                {
                    text: t('history.columns.status.filters.success'),
                    value: 'success',
                }
            ],
            onFilter: (value, item: RowData) => item.status === value,
        },
        {
            title: t('history.columns.duration.name'),
            width: '6em',
            render: (item) => {
                if (item.end) return <DurationFinal start={item.start} end={item.end} />;
                return <DurationProgress start={item.start} />;
            }
        },
        {
            title: t('history.columns.type.name'),
            filters: [
                {
                    text: t('history.columns.type.filters.manual'),
                    value: 'manual',
                },
                {
                    text: t('history.columns.type.filters.watchdog'),
                    value: 'automatic',
                }
            ],
            onFilter: (value, item: RowData) => item.origin === value,
            render: (item: RowData) =>  t(`history.columns.type.filters.${item.origin}`),
            width: '6em'
        },
        Table.EXPAND_COLUMN,
        {
            title:  t('history.columns.files.name'),
            render: (item) => {
                if (shouldBeExpanded(item)) return (<>
                    <span style={{marginRight: '1em'}}>{`${importedFilesCount(item)} imported, ${archivesCount(item)} archives`}</span>
                    {
                        ['success', 'failure', 'unsupported', 'unknown'].map(s => {
                            const count = importedFilesCountWithStatus(item, s);
                            if (count === 0) return null;
                            return (
                                <Tag key={s} color={statusColor(s)} style={{marginLeft: '0.5em'}}>
                                    { count }
                                </Tag>
                            );
                        })
                    }
                </>);
                return <FilesList files={item.files} archives={item.archives} />;
            }
        },
        {
            title: t('history.columns.config.name'),
            width: '8em',
            render: (item: RowData) => {
                return (
                    <Tooltip title={t('history.columns.config.button.tooltip')}>
                        <Button type='ghost' shape='round' onClick={() => navigate(`/config/detail/${item.configurationId}`)}>{t('history.columns.config.button.label')}</Button>
                    </Tooltip>
                );
            }
        }
    ];
    return columns;
};