import React, { useContext} from 'react';
import { useNavigate } from 'react-router-dom';
import ConfigSettingsEdit from '../../../components/ConfigSettings/ConfigSettingsEdit';
import { ImportContext } from '../../../core/import/context';
import { useTranslation } from 'react-i18next';

const ModifyConfigImport = () => {
    const { configData, setConfigData } = useContext(ImportContext);
    const navigate = useNavigate();
    const { t } = useTranslation();

    if (!configData.template) return null;

    return (
        <ConfigSettingsEdit
            title={t('import.modify_config.title') || undefined}
            initValues={configData?.contents}
            templateContents={configData.template?.contents}
            onConfirmSettings={(v) => {
                setConfigData({
                    ...configData,
                    contents: v
                });
                navigate('/import/summary');
            }}/>
    );
};

export default ModifyConfigImport;