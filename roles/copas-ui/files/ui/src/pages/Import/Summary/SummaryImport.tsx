import React from 'react';
import Config from './Config';
import Controls from './Controls';
import Files from './Files';
import styles from './SummaryImport.module.css';
import Watchdog from './Watchdog';

const SummaryImport = () => {
    return (
        <div className={styles.container}>
            <Files className={styles.files}/>
            <Config className={styles.config}/>
            <Watchdog className={styles.watchdog}/>
            <Controls/>
        </div>
    );
};

export default SummaryImport;