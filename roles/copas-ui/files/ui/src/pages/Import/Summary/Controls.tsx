import { Button, Checkbox, Form, Input } from 'antd';
import React, { HTMLProps, useContext, useState } from 'react';
import { toast } from 'react-toastify';
import Surface from '../../../components/Surface/Surface';
import { useForm } from 'antd/lib/form/Form';
import { useIsConfigNameUnique } from '../../../core/config/hooks';
import { ImportContext } from '../../../core/import/context';
import { useImportWithinContext } from '../../../core/import/hooks';
import { useTranslation } from 'react-i18next';

interface FormValues {
  name: string,
  note?: string
}

type ControlsProps = HTMLProps<HTMLDivElement>

const Controls: React.FC<ControlsProps> = (props) => {
    const { configData } = useContext(ImportContext);
    const [checked, setChecked] = useState(false);
    const { importWithinContext, isLoading } = useImportWithinContext();
    const [form] = useForm();
    const isConfigNameUnique = useIsConfigNameUnique();
    const { t } = useTranslation();

    const handleImport = () => {
        form.validateFields()
            .then((formValues: FormValues) => {
                form.resetFields();
                importWithinContext(checked ? { name: formValues.name, note: formValues.note } : undefined);
            })
            .catch((err: string) => {
                toast.error(err);
            });
    };

    const handleUniqueName = (whatever: any, name: string) => {
        if (!checked) return Promise.resolve();
        const unique = isConfigNameUnique(name);
        if (unique) return Promise.resolve();
        return Promise.reject();
    };

    return (
        <Surface {...props}>
            <Form
                form={form}
                layout='vertical'
                style={{padding: '2em 2em 0 2em'}}
            >
                {
                    configData.savedConfig === undefined && (
                        <>
                            <Form.Item>
                                <Checkbox checked={checked} onChange={e => setChecked(e.target.checked)}>{t('import.summary.form.save_config')}</Checkbox>
                            </Form.Item>
                            <Form.Item
                                name='name'
                                label={t('import.summary.form.name.label')}
                                rules={[
                                    {
                                        required: checked, 
                                        message: t('import.summary.form.name.required_error') || undefined},
                                    {
                                        validator: handleUniqueName,
                                        message: t('import.summary.form.name.unique_error') || undefined
                                    }
                                ]}>
                                <Input disabled={!checked}/>
                            </Form.Item>
                            <Form.Item
                                name='note'
                                label={t('import.summary.form.note.label')}>
                                <Input disabled={!checked}/>
                            </Form.Item>
                        </>
                    )
                }
                <Form.Item style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <Button type='primary' shape='round' size='large' disabled={isLoading} onClick={handleImport}>{t('import.summary.import_button.label')}</Button>
                </Form.Item>
            </Form>
        </Surface>
    );
};

export default Controls;