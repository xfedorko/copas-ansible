import { FileOutlined, FileZipOutlined } from '@ant-design/icons';
import { List, Tooltip } from 'antd';
import React, { HTMLProps, useContext, useMemo } from 'react';
import Surface from '../../../components/Surface/Surface';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import SurfaceHeader from '../../../components/Surface/SurfaceHeader';
import { ImportContext } from '../../../core/import/context';
import { useTranslation } from 'react-i18next';

interface DispplayFile {
    name: string,
    path: string,
    archived: boolean,
}

type FilesProps = HTMLProps<HTMLDivElement>

const Files: React.FC<FilesProps> = (props) => {
    const { selectedFiles } = useContext(ImportContext);
    const displayFiles = useMemo(() => {
        const displayFiles: DispplayFile[] = [];
        for (const sf of selectedFiles) {
            if (sf.type === 'file') {
                displayFiles.push({
                    name: sf.name,
                    path: sf.path,
                    archived: false
                });
            } else if (sf.type === 'dir') {
                if (!sf.contents) continue;
                for (const c of sf.contents.filter(c => c.selected)) {
                    displayFiles.push({
                        name: c.name,
                        path: sf.path === '/' ? `/${sf.name}` : `${sf.path}/${sf.name}`,
                        archived: false
                    });
                }
            } else if (sf.type === 'archive') {
                if (!sf.contents) continue;
                for (const c of sf.contents.filter(c => c.selected)) {
                    displayFiles.push({
                        name: c.name,
                        path: `${sf.path}/${sf.name}`,
                        archived: true
                    });
                }
            }
        }
        return displayFiles;
    }, [selectedFiles]);
    const { t } = useTranslation();

    return (
        <Surface {...props}>
            <SurfaceHeader>
                <h2>{t('import.summary.files.title')}</h2>
            </SurfaceHeader>
            <SurfaceBody>
                <List dataSource={displayFiles} 

                    renderItem={(item) => (
                        <List.Item style={{padding: '0.5em 0'}}>
                            <Tooltip title={item.path}>
                                <List.Item.Meta avatar={item.archived ? <FileZipOutlined/> : <FileOutlined/>} 
                                    title={item.name}/>
                            </Tooltip>                 
                        </List.Item>
                    )}
                />
            </SurfaceBody>
        </Surface>
    );
};

export default Files;