import { faEye } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { List, Tooltip } from 'antd';
import React, { HTMLProps, useContext } from 'react';
import Surface from '../../../components/Surface/Surface';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import SurfaceHeader from '../../../components/Surface/SurfaceHeader';
import { ImportContext } from '../../../core/import/context';
import { useTranslation } from 'react-i18next';

type WatchdogProps = HTMLProps<HTMLDivElement>

const Watchdog: React.FC<WatchdogProps> = (props) => {
    const { selectedFiles } = useContext(ImportContext);
    const { t } = useTranslation();
    return (
        <Surface {...props}>
            <SurfaceHeader><h2>{t('import.summary.watch.title')}</h2></SurfaceHeader>
            <SurfaceBody>
                <List 
                    dataSource={selectedFiles.filter(f => f.watch)}
                    renderItem={({ name, path }) => (
                        <List.Item>
                            <Tooltip title={path}>
                                <List.Item.Meta title={name} avatar={<FontAwesomeIcon icon={faEye}/>}/>
                            </Tooltip>
                        </List.Item>
                    )} />
                
            </SurfaceBody>
        </Surface>
    );
};

export default Watchdog;