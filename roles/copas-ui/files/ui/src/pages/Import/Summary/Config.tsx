import React, { HTMLProps, useContext } from 'react';
import Surface from '../../../components/Surface/Surface';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import SurfaceHeader from '../../../components/Surface/SurfaceHeader';
import ConfigSettings from '../../../components/ConfigSettings/ConfigSettings';
import { ConfigContext } from '../../../core/config/template/context';
import { ImportContext } from '../../../core/import/context';
import { useTranslation } from 'react-i18next';

type ConfigProps = HTMLProps<HTMLDivElement>

const Config: React.FC<ConfigProps> = (props) => {
    const { configData } = useContext(ImportContext);
    const { t } = useTranslation();

    if (!configData.template) return null;
    
    return (
        <Surface {...props}>
            <SurfaceHeader>
                <h2>{t('import.summary.config.title')}</h2>
            </SurfaceHeader>
            <SurfaceBody>
                <ConfigContext.Provider value={{ config: configData.contents || {} , updateConfig: () => {} }}>
                    <ConfigSettings templateContents={configData.template?.contents} readonly />
                </ConfigContext.Provider>
            </SurfaceBody>
        </Surface>
    );
};

export default Config;