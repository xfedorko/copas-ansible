import React, { useState } from 'react';
import { Outlet } from 'react-router-dom';
import ImportProgress from './ImportProgress/ImportProgress';
import styles from './Import.module.css';
import { ConfigImportData, SelectedFileInfo } from '../../core/import/model';
import { ImportContext } from '../../core/import/context';

const Import = () => {
    const [selectedFiles, setSelectedFiles] = useState<SelectedFileInfo[]>([]);
    const [configData, setConfigData] = useState<ConfigImportData>({ contents: {}});

    return (
        <ImportContext.Provider value={{
            selectedFiles, setSelectedFiles,
            configData, setConfigData}}>
            <div className={styles.container}>
                <ImportProgress className={styles.progress} />
                <Outlet/>
            </div>
        </ImportContext.Provider>
    );
};

export default Import;