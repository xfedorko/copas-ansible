import { PlusOutlined } from '@ant-design/icons';
import { Tooltip, Button } from 'antd';
import React, { HTMLProps, useState } from 'react';
import ConfigList from '../../../components/ConfigList/ConfigList';
import Surface from '../../../components/Surface/Surface';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import SurfaceHeader from '../../../components/Surface/SurfaceHeader';
import styles from './ChooseConfigImport.module.css';
import { useModuleInfoContext } from '../../../core/info/hooks';
import { useDuplicateSavedConfig, useChooseSavedConfig } from '../../../core/import/hooks';
import ChooseTemplateModal from '../../../components/Modals/ChooseTemplateModal';
import { useTranslation } from 'react-i18next';

export type ChooseConfigProps = HTMLProps<HTMLDivElement>

const ChooseConfigImport: React.FC<ChooseConfigProps> = ( props ) => {
    const { configTemplates } = useModuleInfoContext();
    const duplicateSavedConfig = useDuplicateSavedConfig();
    const chooseSavedConfig = useChooseSavedConfig();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { t } = useTranslation();
    return (
        <Surface className={styles.container} {...props}>
            <SurfaceHeader
                rightSection={
                    <Tooltip title={t('import.choose_config.new_button.tooltip')}>
                        <Button type='primary' shape='round' style={{marginLeft: '2em'}}
                            disabled={configTemplates.length === 0}
                            icon={<PlusOutlined/>} onClick={() => {
                                setIsModalOpen(true);
                            }}>{t('import.choose_config.new_button.label')}</Button>
                    </Tooltip>
                }
            >
                <h2>{t('import.choose_config.title')}</h2>
            </SurfaceHeader>
            <SurfaceBody>
                <ConfigList
                    actions={(item) => [
                        <Tooltip key={item.id} title={t('import.choose_config.duplicate_button.tooltip')}>
                            <Button type='ghost' shape='round' onClick={() => {
                                duplicateSavedConfig(item);
                            }}>{t('import.choose_config.duplicate_button.label')}</Button>
                        </Tooltip>,
                        <Tooltip key={item.id} title={t('import.choose_config.use_button.tooltip')}>                                
                            <Button type='ghost' shape='round' onClick={() => {
                                chooseSavedConfig(item);
                            }}>{t('import.choose_config.use_button.label')}</Button>
                        </Tooltip>
                    ]}
                />
            </SurfaceBody>
            {
                (configTemplates.length > 0) && (
                    <ChooseTemplateModal 
                        handleConfirm={() => setIsModalOpen(false)}
                        handleCancel={() => setIsModalOpen(false)}  
                        open={isModalOpen}/>
                )
            }
        </Surface>

    );
};

export default ChooseConfigImport;