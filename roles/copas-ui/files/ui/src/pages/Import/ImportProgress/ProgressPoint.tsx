import React from 'react';
import styles from './ProgressPoint.module.css';

const computeTextClass = (state: ProgressPointState) => {
    if (state === 'complete') return styles.completeText;
    else if (state === 'pending') return styles.pendingText;
    return styles.incompleteText;
};

export type ProgressPointState = 'complete' | 'pending' | 'incomplete'

interface ProgressPointProps {
    text: string,
    order: number,
    state: ProgressPointState,
    onClick: () => void,
}

const ProgressPoint: React.FC<ProgressPointProps> = ({ text, order, state, onClick }) => {
    return (
        <>
            { order > 1 && <div className={`${styles.progressLine} ${state === 'incomplete' ? styles[state] : styles['complete']}`}/> }
            <div className={`${styles.container} ${styles[state]}`} onClick={() => {
                if (state === 'complete') {
                    onClick();
                }
            }}>
                <span className={styles.progressNumber}>{order}</span>
                <div className={`${styles.progressText} ${computeTextClass(state)}`}>{text}</div>
            </div>
        </>
    );
};

export default ProgressPoint;
