import React, { useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import styles from './ImportProgress.module.css';
import ProgressPoint, { ProgressPointState } from './ProgressPoint';
import { useTranslation } from 'react-i18next';

interface Point {
    title: string,
    path: string
}

interface ImportProgressProps {
    className?: string,
}

const ImportProgress: React.FC<ImportProgressProps> = ({ className }) => {
    const location = useLocation();
    const navigate = useNavigate();
    const { t } = useTranslation();

    const points: Point[] = [
        {title: t('import.progress.select_files'), path: '/import'},
        {title: t('import.progress.choose_config'), path: '/import/config'},
        {title: t('import.progress.modify_config'), path: '/import/create'},
        {title: t('import.progress.summary'), path: '/import/summary'},
    ];

    const activeIndex = useMemo(() => {
        return points.map(c => c.path).indexOf(location.pathname);
    }, [location]);

    return (
        <div className={`${styles.container} ${className}`}>
            <div className={styles.progress}>
                {
                    points.map((p,i) => {
                        let state: ProgressPointState = 'incomplete';
                        if (activeIndex === i) {
                            state = 'pending';
                        } else if (activeIndex > i) {
                            state = 'complete';
                        }
                        return(<ProgressPoint key={p.title} text={p.title} order={i+1}
                            state={state} onClick={() => navigate(p.path)}/>);
                    })
                }
            </div>
        </div>
    );
};

export default ImportProgress;