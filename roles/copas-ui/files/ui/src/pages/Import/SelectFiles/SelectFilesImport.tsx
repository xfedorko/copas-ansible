import React, { HTMLProps, useContext, useRef, useState } from 'react';
import styles from './SelectFilesImport.module.css';
import SelectedFiles from './SelectedFiles/SelectedFiles';
import { Button, Tooltip } from 'antd';
import { ArrowDownOutlined, ArrowRightOutlined } from '@ant-design/icons';
import FileManager from '../../../components/FileManager/FileManager';
import { useWindowSize } from '../../../hooks';
import { ImportContext } from '../../../core/import/context';
import { FileInfo } from '../../../core/files/model';
import { PathContext } from '../../../core/files/context';
import { useTranslation } from 'react-i18next';

type SelectProps = HTMLProps<HTMLDivElement> 

const SelectFilesImport : React.FC<SelectProps> = ({ className }) => {
    const { selectedFiles, setSelectedFiles } = useContext(ImportContext);
    const { path, setPath } = useContext(PathContext);
    const [selectionSize, setSelectionSize] = useState(0);
    const selectionRef = useRef<FileInfo[]>();
    const { width } = useWindowSize();
    const { t } = useTranslation();

    const handleSelectionChange = (f: FileInfo[]) => {
        selectionRef.current = f;
        setSelectionSize(f.length);
    };

    const onFilesImported = () => {
        const currentPaths = selectedFiles.map(c => `${c.path}/${c.name}`);
        if (selectionRef.current) {
            const toAdd = selectionRef.current
                .filter(f => !currentPaths.find(e => `${path}/${f.name}` === e))
                .map(c => ({ name: c.name, type: c.type, path: path, loading: false }));
            for (const c of toAdd) {
                if (c.type === 'dir' || c.type === 'archive') {
                    c.loading = true;
                }
            }
            const tmpFiles = [...selectedFiles, ...toAdd];
            setSelectedFiles(tmpFiles);
        }
    };

    return (
        <div className={`${styles.container} ${className}`}>
            <FileManager className={styles.filemanager} readonly={true}
                onSelectionChange={handleSelectionChange} onPathChange={setPath} path={path}/>
            <div className={styles.controls}>
                <Tooltip title={t('import.select_files.pick_button.tooltip')}>
                    <Button icon={width < 850 ? <ArrowDownOutlined/> : <ArrowRightOutlined/>} block size='large' disabled={selectionSize === 0} onClick={onFilesImported}/>
                </Tooltip>
            </div>
            <SelectedFiles className={styles.selected} handleChangePath={(p) => setPath(p)}/>
        </div>
    );
};

export default SelectFilesImport;