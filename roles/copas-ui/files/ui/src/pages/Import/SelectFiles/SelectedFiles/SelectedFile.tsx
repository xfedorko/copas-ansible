import { faCaretDown, faCaretRight, faEye, faLocationCrosshairs, faSpinner, faXmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, useMemo, useState } from 'react';
import styles from './SelectedFile.module.css';
import { Button, Checkbox, Tooltip } from 'antd';
import { ImportContext } from '../../../../core/import/context';
import { fileColor, fileIcon, fullFileName } from '../../../../core/files/utils';
import { useFileContents } from '../../../../core/import/hooks';
import { useTranslation } from 'react-i18next';

export interface SelectedFileProps {
    handleChangePath?: (p: string) => void,
    idx: number
}

const SelectedFile: React.FC<SelectedFileProps> = ({ idx, handleChangePath }) => {
    const { selectedFiles, setSelectedFiles } = useContext(ImportContext);
    const { type, path, name, watch, contents }  = useMemo(() => {
        return selectedFiles[idx];
    }, [idx, selectedFiles]);
    const [expanded, setExpanded] = useState(false);
    const [selectAll, setSelectAll] = useState(true);
    const {isLoading, isFetching} = useFileContents(idx, fullFileName(path, name));
    const { t } = useTranslation();
    
    const onToggle = (i: number) => {
        if (!contents || contents?.length <= i) return;
        const isSelected = contents[i].selected;
        const tmpFiles = [...selectedFiles];
        tmpFiles[idx] = {
            name, path, type, watch
        };
        const tmpContents = [...contents];
        tmpContents[i].selected = !isSelected;
        tmpFiles[idx].contents = tmpContents;
        setSelectedFiles(tmpFiles);
        if (tmpFiles[idx].contents?.every(f => f.selected)) {
            setSelectAll(true);
        } else if (tmpFiles[idx].contents?.every(f => !f.selected)) {
            setSelectAll(false);
        }
    };

    const onWatchToggle = () => {
        const tmpFiles = [...selectedFiles];
        tmpFiles[idx] = {
            ...tmpFiles[idx], watch: !watch
        };
        setSelectedFiles(tmpFiles);
    };

    const onSelectAll = () => {
        if (!contents) return;
        const tmpFiles = [...selectedFiles];
        tmpFiles[idx] = {
            name, path, type, watch
        };
        tmpFiles[idx].contents = [...contents.map(c => ({name: c.name, selected: !selectAll}))];
        setSelectedFiles(tmpFiles);
        setSelectAll(!selectAll);
    };

    let expandedIcon;
    if (isLoading || (isFetching && type !== 'file' && !contents)) {
        expandedIcon = <FontAwesomeIcon icon={faSpinner} pulse/>;
    } else if (type !== 'file' && contents && contents.length > 0) {
        expandedIcon = <FontAwesomeIcon icon={expanded ? faCaretDown : faCaretRight}/>;
    }

    return (
        <>
            <div className={styles.item}>
                <Button type='text' onClick={() => handleChangePath?.(path)} icon={<FontAwesomeIcon icon={faLocationCrosshairs}/>}/>
                <Button disabled={type === 'file' || isLoading}  type='text'
                    onClick={() => {
                        setExpanded(!expanded);
                    }}
                    icon={expandedIcon}
                />
                <FontAwesomeIcon icon={fileIcon(type)} color={fileColor(type)}/>
                <Tooltip title={fullFileName(path,name)} placement='topLeft'>
                    <div className={styles.itemPath}>
                        {name}
                    </div>
                </Tooltip>

                { type === 'dir' && (
                    <Button type='text' style={{'opacity': watch ? '1' : '0.3'}} title={t('import.select_files.watch.tooltip') || undefined}
                        icon={<FontAwesomeIcon icon={faEye}/>} onClick={() => onWatchToggle()} />
                )}
                <div className={styles.itemFilecount}>
                    {
                        contents && <span>{contents.filter(c => c.selected).length} / {contents.length}</span>
                    }
                </div>
                { type !== 'file' && <Checkbox checked={selectAll} onChange={onSelectAll} />}
                <Button type='text' icon={<FontAwesomeIcon icon={faXmark}/>} onClick={() => {
                    const tmpFiles = selectedFiles.filter(c => `${c.path}${c.name}` !== `${path}${name}`);
                    setSelectedFiles(tmpFiles);
                }}/>
            </div>
            {
                contents && expanded && (
                    <div className={styles.expandable}>
                        {
                            contents.map((c,j) => (
                                <div key={c.name} className={styles.expandableItem}>
                                    <span className={styles.expandableItemText}>{c.name}</span>
                                    <Checkbox checked={c.selected} onChange={() => onToggle(j)} />
                                </div>
                            ))
                        }

                    </div>
                )
            }
        </>
    );
};

export default SelectedFile;