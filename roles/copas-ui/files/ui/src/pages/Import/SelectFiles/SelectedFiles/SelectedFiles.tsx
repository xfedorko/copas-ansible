import { faFileCircleCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, List } from 'antd';
import React, { useContext, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';
import Surface from '../../../../components/Surface/Surface';
import SurfaceBody from '../../../../components/Surface/SurfaceBody';
import SurfaceFooter from '../../../../components/Surface/SurfaceFooter';
import SurfaceHeader from '../../../../components/Surface/SurfaceHeader';
import SelectedFile from './SelectedFile';
import { ImportContext } from '../../../../core/import/context';
import { useTranslation } from 'react-i18next';


interface SelectedFilesProps {
    handleChangePath?: (p: string) => void,
    className?: string
}

const SelectedFiles: React.FC<SelectedFilesProps> = ({ className, handleChangePath }) => {
    const { selectedFiles } = useContext(ImportContext);
    const navigate = useNavigate();
    const selectedFilesCount = useMemo(() => {
        return selectedFiles.reduce((acc, cur) => {
            if (cur.type === 'file' || cur.type === 'slink') return acc + 1;
            if (cur.contents) return acc + cur.contents.filter(f => f.selected).length;
            return acc;
        }, 0);
    },[selectedFiles]);
    const { t } = useTranslation();

    return (
        <Surface className={className}>
            <SurfaceHeader>
                <h2>{t('import.select_files.title')}</h2>
            </SurfaceHeader>
            <SurfaceBody>
                {
                    <List dataSource={selectedFiles} itemLayout='vertical'
                        renderItem={(item, i) => (
                            <List.Item>
                                <SelectedFile idx={i} handleChangePath={handleChangePath}/>
                            </List.Item>
                        )}
                    />
                }
            </SurfaceBody>
            <SurfaceFooter>
                <Button type='primary' shape='round'
                    icon={<FontAwesomeIcon icon={faFileCircleCheck} style={{marginRight: '0.5em'}}/>}
                    style={{marginLeft: '2em'}}
                    disabled={selectedFilesCount === 0} onClick={() => navigate('/import/config')} >{t('words.confirm')}</Button>
            </SurfaceFooter>
        </Surface>

    );
};

export default SelectedFiles;