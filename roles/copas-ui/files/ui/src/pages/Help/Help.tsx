import React from 'react';
import ReactMarkdown from 'react-markdown';
import styles from './Help.module.css';
import Surface from '../../components/Surface/Surface';
import { Button, Tabs, TabsProps } from 'antd';
import SurfaceHeader from '../../components/Surface/SurfaceHeader';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { useModuleInfoContext } from '../../core/info/hooks';
import SurfaceBody from '../../components/Surface/SurfaceBody';
import { SERVER_URL } from '../../clients/url';
import { useTranslation } from 'react-i18next';


const Help = () => {
    const { helpSections } = useModuleInfoContext();
    const navigate = useNavigate();
    const { t } = useTranslation();

    const items: TabsProps['items'] = helpSections.map(s => ({
        key: s.name,
        label: s.name,
        children: (
            <ReactMarkdown className={styles.markdown} transformImageUri={uri => 
                uri.startsWith('http') ? uri : `${SERVER_URL}/analysis/static/help/${uri}`}>
                {s.contents}
            </ReactMarkdown>
        )
    }));

    if (!items) return null;

    return (
        <div className={styles.container}>
            <Surface className={styles.help}>
                <SurfaceHeader
                    leftSection={                   
                        <Button 
                            icon={<ArrowLeftOutlined />} 
                            type='ghost' shape='round'
                            onClick={() => navigate('/')}/>
                    }
                >
                    <h2>{t('help.title')}</h2>
               
                </SurfaceHeader>
                <SurfaceBody>
                    <Tabs defaultActiveKey={items[0].key} items={items} centered tabPosition='left' style={{height: '100%', marginRight: '1.5em'}} />
                </SurfaceBody>
               
            </Surface>
        </div>
    );
};

export default Help;