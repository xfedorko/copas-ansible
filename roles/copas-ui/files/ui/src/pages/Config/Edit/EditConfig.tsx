import React from 'react';
import { useParams } from 'react-router-dom';
import ConfigSettingsEdit from '../../../components/ConfigSettings/ConfigSettingsEdit';
import { useConfigItem, useConfigurationTitle, useUpdateSavedConfig } from '../../../core/config/hooks';
import { ConfigContents } from '../../../core/config/model';
import { useModuleInfoContext } from '../../../core/info/hooks';

const EditConfig = () => {
    const { configTemplates } = useModuleInfoContext();
    const { id } = useParams();
    const { isLoading, isError, configItem } = useConfigItem(parseInt(id || '0'));
    const { updateConfig } = useUpdateSavedConfig();
    const configTitle = useConfigurationTitle(configItem);
    const handleConfirmSettings = (configContents: ConfigContents) => {
        updateConfig({ id: parseInt(id || '0'), contents: configContents });
    };

    if (isError) return null;

    const template = configTemplates.find(t => t.contentsId === configItem?.templateContentsId);

    return (
        <ConfigSettingsEdit isLoading={isLoading} onConfirmSettings={handleConfirmSettings} templateContents={template?.contents} initValues={configItem?.configContents} title={configTitle}/>
    );
};

export default EditConfig;