import { PlusOutlined } from '@ant-design/icons';
import { Button, Popconfirm,  Tooltip } from 'antd';
import React, { HTMLProps, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ConfigList from '../../components/ConfigList/ConfigList';
import Surface from '../../components/Surface/Surface';
import SurfaceBody from '../../components/Surface/SurfaceBody';
import SurfaceHeader from '../../components/Surface/SurfaceHeader';
import styles from './Config.module.css';
import ChooseStaticTemplateModal from '../../components/Modals/ChooseStaticTemplateModal';
import BackButton from '../../components/Buttons/BackButton';
import { useDeleteSavedConfig } from '../../core/config/hooks';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';

export type ConfigProps = HTMLProps<HTMLDivElement>

const Config: React.FC<ConfigProps> = (props) => {
    const { configTemplates } = useModuleInfoContext();
    const { deleteSavedConfig } = useDeleteSavedConfig();
    const navigate = useNavigate();

    const [isModalOpen, setIsModalOpen] = useState(false);
    const { t } = useTranslation();

    return (
        <>
            <div className={`${styles.container}`} {...props} >
                <Surface className={styles.configList}>
                    <SurfaceHeader
                        leftSection={<BackButton route='/'/>}
                        rightSection={
                            <Tooltip title={t('config.add_button.tooltip')}>
                                <Button type='primary' shape='round' icon={<PlusOutlined/>}
                                    disabled={configTemplates.length === 0}
                                    onClick={() => {
                                        if (configTemplates.length === 1) {
                                            navigate(`create/${configTemplates[0].key}`);
                                        } else {
                                            setIsModalOpen(true);
                                        }
                                    }}>{t('config.add_button.label')}</Button>
                            </Tooltip>
                        }
                    >
                        <div style={{flexGrow: 1, display: 'flex', justifyContent: 'center'}}>
                            <h2>{t('config.title')}</h2>
                        </div>

                    </SurfaceHeader>
                    <SurfaceBody>
                        <ConfigList 
                            actions={(item) => [
                                <Tooltip key={item.id} title={t('config.edit_button.tooltip')}>
                                    <Button type='ghost' shape='round' onClick={() => navigate(`/config/edit/${item.configurationId}`)}>
                                        {t('config.edit_button.label')}
                                    </Button>
                                </Tooltip>,
                                <Tooltip key={item.id} title={t('config.remove_button.tooltip')}>
                                    <Popconfirm title={t('config.remove_button.modal.text')} 
                                        okText={t('words.yes')} cancelText={t('words.no')}
                                        onConfirm={() => deleteSavedConfig(item.name)}
                                    >
                                        <Button type='ghost' shape='round'>{t('config.remove_button.label')}</Button>
                                    </Popconfirm>
                                </Tooltip>
                            ]}
                        />
                    </SurfaceBody>
                </Surface>
            </div>
            {
                configTemplates.length > 0 && (
                    <ChooseStaticTemplateModal 
                        handleConfirm={templateId => {
                            setIsModalOpen(false);
                            navigate(`create/${templateId}`);
                        }}
                        handleCancel={() => setIsModalOpen(false)}  
                        open={isModalOpen}/>
                )
            }
        </>
    );
};

export default Config;
