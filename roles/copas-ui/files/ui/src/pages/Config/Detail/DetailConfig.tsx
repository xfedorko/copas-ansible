import React from 'react';
import { HTMLProps } from 'react';
import Surface from '../../../components/Surface/Surface';
import SurfaceHeader from '../../../components/Surface/SurfaceHeader';
import SurfaceBody from '../../../components/Surface/SurfaceBody';
import { useParams } from 'react-router-dom';
import styles from './DetailConfig.module.css';
import BackButton from '../../../components/Buttons/BackButton';
import ConfigSettings from '../../../components/ConfigSettings/ConfigSettings';
import { useConfigItem, useConfigurationTitle } from '../../../core/config/hooks';
import { useModuleInfoContext } from '../../../core/info/hooks';
import { ConfigContext } from '../../../core/config/template/context';

type DetailProps = HTMLProps<HTMLDivElement>

const DetailConfig: React.FC<DetailProps> = ({ ...rest }) => {
    const {  configTemplates } = useModuleInfoContext();
    const { id } = useParams();
    const { isLoading, isError, configItem } = useConfigItem(parseInt(id || '0'));
    const configTitle = useConfigurationTitle(configItem);
    const template = configTemplates.find(t => t.contentsId === configItem?.templateContentsId);

    return (
        <Surface className={`${styles.container} ${rest.className}`}  {...rest}>
            <SurfaceHeader leftSection={<BackButton/>}>
                <h2>{configTitle}</h2>
            </SurfaceHeader>
            <SurfaceBody isLoading={isLoading} isError={isError}>
                <ConfigContext.Provider value={{ config: configItem?.configContents || {}, updateConfig: () => {} }}>
                    <ConfigSettings templateContents={template?.contents} readonly/>
                </ConfigContext.Provider>
            </SurfaceBody>
        </Surface>
    );
};

export default DetailConfig;