import React, { useState } from 'react';
import { Form, Input, Modal } from 'antd';
import { useParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import ConfigSettingsEdit from '../../../components/ConfigSettings/ConfigSettingsEdit';
import { AxiosError } from 'axios';
import { useIsConfigNameUnique, useSaveConfig } from '../../../core/config/hooks';
import { useModuleInfoContext } from '../../../core/info/hooks';
import { useTranslation } from 'react-i18next';

interface FormValues {
    name: string,
    note?: string
}

const CreateConfig = () => {
    const { templateKey } = useParams();
    const { configTemplates } = useModuleInfoContext();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [config, setConfig] = useState<{[key: string]: any}>({});
    const [form] = Form.useForm();
    const { saveConfig } = useSaveConfig();
    const isConfigNameUnique = useIsConfigNameUnique();
    const { t } = useTranslation();
    const template = configTemplates.find(t => t.key === templateKey);

    if (!template) return null;
    
    const handleConfirmSettings = (config: {[key: string]: any}) => {
        setConfig(config);
        setIsModalOpen(true);
    };

    const handleOk = () => {
        form
            .validateFields()
            .then((val: FormValues) => {
                form.resetFields();
                saveConfig({ name: val.name, note: val.note, contents: config, templateContentsId: template?.contentsId });
                setIsModalOpen(false);
            })
            .catch((err: AxiosError<any>) => {
                toast.error(err.response?.data);
            });
    };

    const handleCancel = () => {
        setIsModalOpen(false);
    };

    const handleUniqueName = (whatever: any, name: string) => {
        const unique = isConfigNameUnique(name);
        if (unique) return Promise.resolve(true);
        return Promise.reject(false);
    };

    return (
        <>
            <ConfigSettingsEdit onConfirmSettings={handleConfirmSettings} templateContents={template.contents}/>
            <Modal title={t('config.create.title')} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                <Form
                    form={form}
                    layout='vertical'
                >
                    <Form.Item
                        name='name'
                        label={t('config.create.modal.name.label')}
                        rules={[
                            {required: true, message: t('config.create.modal.name.required_error') || undefined},
                            {
                                validator: handleUniqueName,
                                message: t('config.create.modal.name.unique_error') || undefined
                            }
                        ]}>
                        <Input/>
                    </Form.Item>
                    <Form.Item
                        name='note'
                        label={t('config.create.modal.note.label')}>
                        <Input/>
                    </Form.Item>
                </Form>
            </Modal>
        </>
    );
};

export default CreateConfig;