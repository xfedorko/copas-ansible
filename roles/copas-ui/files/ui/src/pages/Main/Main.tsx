import React, { Suspense } from 'react';
import { Outlet } from 'react-router-dom';
import Navbar from '../../components/Navbar/Navbar';
import styles from './Main.module.css';
import { Spin } from 'antd';

const Main = () => {
    return (
        <div className={styles.container}>
            <Navbar/>
            <Suspense fallback={<Spin tip='Loading' size='large' style={{ margin: 'auto auto'}}/>}>
                <Outlet/>
            </Suspense>
        </div>
    );
};

export default Main;