import React from 'react';
import styles from './Footer.module.css';
import { Popover, Tooltip, Typography } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircleInfo, faCircleQuestion } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import CopasLogo from '../../images/copas-logo.svg';
import CeritSCLogo from '../../images/MUNI-cerit-sc-RGB.png'; 
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';

const Footer = () => {
    const { moduleAuthorName, moduleAuthorEmail, moduleAuthorInstitution, helpSections } = useModuleInfoContext();
    const { t } = useTranslation();
    return (
        <div className={styles.footer}>
            <div className={styles['footer-left']}>
                <Popover
                    placement='topLeft'
                    title={t('home.info.title')}
                    content={
                        <div className={styles['info-popover']}>
                            <div className={styles['info-section-module']}>
                                <Typography className={styles['info-title']}>{t('home.info.module.title')}</Typography>
                                <Typography>{moduleAuthorName}</Typography>
                                <Typography>{moduleAuthorEmail}</Typography>
                                <Typography>{moduleAuthorInstitution}</Typography>
                            </div>
                            <div className={styles['info-section-copas']}>
                                <Typography className={styles['info-title']}>CoPAS</Typography>
                                <Typography>Tomáš Rebok, {t('home.info.copas.lead_title')}</Typography>
                                <Typography><a href='mailto:rebok@ics.muni.cz' style={{color: 'blue'}}>rebok@ics.muni.cz</a></Typography>
                                <Typography>{t('home.promotion.ics')}</Typography>
                                <Typography>{t('home.promotion.muni')}</Typography>
                                <br/>
                                <Typography>Ondřej Machala, {t('home.info.copas.developer_title')}</Typography>
                                <Typography><a href='mailto:ondra097@gmail.com' style={{color: 'blue'}}>ondra097@gmail.com</a></Typography>
                                <Typography>{t('home.info.copas.muni_faculty')}</Typography>
                                <Typography>{t('home.promotion.muni')}</Typography>
                            </div>
                        </div>} >
                    <div className={styles.helpContainer}>
                        <FontAwesomeIcon icon={faCircleInfo} size='3x' color='white'/>
                    </div>
                </Popover>
                {
                    helpSections.length > 0 && (
                        <Tooltip title={t('home.help.tooltip')} placement='topLeft'>
                            <Link to={'/help'} className={styles.helpContainer}>
                                <FontAwesomeIcon icon={faCircleQuestion} size='3x' color='white'/>
                            </Link>
                        </Tooltip>
                    )
                }
            </div>
            <div className={styles['footer-middle']}>
                <div className={styles['powered-by']}>
                    <Typography>Powered by</Typography>
                </div>
                <div className={styles['copas-logo']}>
                    <img className={styles['copas-logo-img']} src={CopasLogo} alt='CoPAS logo'/>
                </div>
                <div className={styles['copas-text']}>
                    <Typography>{t('home.logo.slogan')}</Typography>
                </div>
            </div>
            <div className={styles['footer-right']}>
                <div className={styles['cerit-logo']}>
                    <a href='https://cerit-sc.cz'>
                        <img src={CeritSCLogo} alt='MUNI CERIT-SC logo' width={220} height={91}/>
                    </a>
                </div>
            
                <div className={styles['footer-right-info']}>
                    <Typography>{t('home.promotion.ceritsc')}</Typography>
                    <Typography>{t('home.promotion.ics')}</Typography>
                    <Typography>{t('home.promotion.muni')}</Typography>
                    <Typography><a href='https://www.cerit-sc.cz' style={{color: 'blue', fontWeight: 'normal'}}>https://www.cerit-sc.cz</a></Typography>
                    <Typography><a href='https://ics.muni.cz/en' style={{color: 'blue', fontWeight: 'normal'}}>https://ics.muni.cz</a></Typography>
                </div>
            </div>
        </div>
    );
};

export default Footer;