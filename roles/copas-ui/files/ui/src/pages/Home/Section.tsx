import React from 'react';
import styles from './Section.module.css';
import { SERVER_URL } from '../../clients/url';
import { useModuleInfoContext } from '../../core/info/hooks';

type SectionType = 'left' | 'right'

interface SectionProps {
    type: SectionType,
    className?: string
}

const Section : React.FC<SectionProps> = ({ type, className }) => {
    const { sectionLeft, sectionRight } = useModuleInfoContext();
    const section = type === 'left' ? sectionLeft : sectionRight; 
    if (!section) return null;
    const { image, lines } = section;
    return (
        <div className={`${styles['section']} ${className}`}>
            <div className={styles['section-logo']}>
                { image && <img src={`${SERVER_URL}/analysis/static/${image}`} alt={`Logo of ${type} section`}/> }      
            </div>
            <div className={styles['section-text']}>
                { lines && lines.map((l, i) => {
                    if (l.type === 'text') return <span key={i}><strong>{l.content}</strong></span>;
                    if (l.type === 'link') return <span key={i}><a href={l.to} style={{color: 'blue'}}>{l.content}</a></span>;
                    if (l.type === 'mail') return <span key={i}><a href={`mailto:${l.to}`} style={{color: 'blue'}}>{l.content}</a></span>;
                    return null;
                })}
            </div>
        </div>
    );
};

export default Section;