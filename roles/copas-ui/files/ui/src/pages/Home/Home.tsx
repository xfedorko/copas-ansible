import React from 'react';
import { faClockRotateLeft, faComputer, faEye, faFileImport, faFolderTree, faGear, faMagnifyingGlassChart } from '@fortawesome/free-solid-svg-icons';
import styles from './Home.module.css';
import CopasLink from '../../components/Link/CopasLink';
import Section from './Section';
import Footer from './Footer';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';


const Home = () => {
    const { homeTitle, homeDescription, analysisUrl, moduleType } = useModuleInfoContext();
    const { t } = useTranslation();
    return (
        <div className={styles.container}>
      
            <div className={styles.titleSection}>
                <div className={styles.titleContainer}>
                    <h1 style={{textAlign: 'center'}}>{homeTitle}</h1>
                    <p>{homeDescription}</p>
                </div>
            </div>

            <div className={styles.mainLinkContainer} style={{gridRowEnd: moduleType === 'app' ? 'other-links' : undefined}}>
                <CopasLink type='main' to='/files' icon={faFolderTree} text={t('navigation.files')}/>
                {moduleType === 'analysis'  && <CopasLink type='main' to='/import' icon={faFileImport} text={t('navigation.import')}/>}
                <CopasLink type='main' to={analysisUrl} 
                    icon={moduleType === 'analysis' ? faMagnifyingGlassChart : faComputer} 
                    text={moduleType === 'analysis' ? t('navigation.analysis') : t('navigation.app')}/>
            </div>

            
            {
                moduleType === 'analysis' && (
                    <div className={styles.otherLinkContainer}>
                        <CopasLink type='other' to='/config' icon={faGear} text={t('navigation.config')}/>
                        <CopasLink type='other' to='/watch' icon={faEye} text={t('navigation.watch')}/>
                        <CopasLink type='other' to='/history' icon={faClockRotateLeft} text={t('navigation.history')}/>
                    </div>
                )
            }

            <Section type='left' className={styles['left-section']}/>
            <Section type='right' className={styles['right-section']}/>
     
            <Footer/>
        </div>
    );
};

export default Home;