import React, { useContext } from 'react';
import FileManager from '../../components/FileManager/FileManager';
import styles from './Files.module.css';
import { PathContext } from '../../core/files/context';

const Files = () => {
    const { path, setPath } = useContext(PathContext);
    return (
        <FileManager className={styles.contents} path={path} onPathChange={setPath}/>
    );
};

export default Files;