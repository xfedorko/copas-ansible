import i18n from 'i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { initReactI18next } from 'react-i18next';
import { retrieveClient } from './clients/retrieve';
import { operationClient } from './clients/operation';
import { defaultClient } from './clients/default';
import { uploadClient } from './clients/upload';

i18n.use(Backend).use(initReactI18next).use(LanguageDetector).init({
    fallbackLng: 'en',
    interpolation: {
        escapeValue: false
    }
});

i18n.on('languageChanged', (lng) => {
    retrieveClient.defaults.headers.common['Accept-Language'] = lng;
    operationClient.defaults.headers.common['Accept-Language'] = lng;
    defaultClient.defaults.headers.common['Accept-Language'] = lng;
    uploadClient.defaults.headers.common['Accept-Language'] = lng;
});

export default i18n;