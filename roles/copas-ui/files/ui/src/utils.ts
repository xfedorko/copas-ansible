export const formatDate = (date: Date) => date.toLocaleDateString('cs-CZ', { 
    year: 'numeric',
    month: '2-digit',
    day: '2-digit',
    hour: '2-digit',
    minute: '2-digit'
});

export const getToday = () => new Date();
export const getYesterday = () => {
    const d = new Date();
    d.setDate(d.getDate() - 1);
    return d;
};

export const getWeekAgo = () => {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth(), now.getDate() - 7);
};

export const getMonthAgo = () => {
    const now = new Date();
    return new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
};


export const getFilename = (path: string) => path.substring(path.lastIndexOf('/') + 1);

export const getFullPath = (path: string, file: string) => path === '/' ? `/${file}` : `${path}/${file}`;

const byteLevels = ['  B', 'KB', 'MB', 'GB'];

export const bytesToString = (b: number) => {
    let l = 0;
    while(b >= 1024 && ++l){
        b /= 1024;
    }
    return `${Math.ceil(b)}  ${byteLevels[l]}`;
};

export const underscoreToCamelcase = (str: string) => {
    let output = '';
    str.split('_').forEach((p, i) => {
        output += i > 0 ? p.charAt(0).toUpperCase() + p.slice(1) : p;
    });
    return output;
};

export const camelcaseToUnderscore = (str: string) => {
    let output = '';
    const strArray = [...str];
    strArray.forEach(c => {
        output += c === c.toUpperCase() && c !== c.toLowerCase() ? '_' + c.toLowerCase() : c;
    });
    return output;
};

export const updateKeys = (obj: any, updateKey: (k: string) => string, except?: string[]) => {
    if (obj === undefined || obj === null) return obj;
    if (Array.isArray(obj)) {
        const arr = obj as any[];
        const output: any[] = [];
        for (const x of arr) {
            output.push(typeof x === 'object' ? updateKeys(x, updateKey) : x);
        }
        return output;
    }

    if (typeof obj === 'object') {
        const output: { [key: string]: any } = {};
        for (const k in obj) {
            if (typeof obj[k] === 'object') {
                if (except?.includes(k)) {
                    output[updateKey(k)] = obj[k];
                } else {
                    output[updateKey(k)] = updateKeys(obj[k], updateKey);
                }
                
            } else {
                output[updateKey(k)] = obj[k];
            }
        }
        return output;
    }
    
    return obj;
};

export const makeCamelcase = (obj: any, except?: string[]) => {
    return updateKeys(obj, underscoreToCamelcase, except);
};

export const makeUnderscore = (obj: any, except?: string[]) => {
    return updateKeys(obj, camelcaseToUnderscore, except);
};