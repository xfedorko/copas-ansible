import axios, { AxiosError } from 'axios';
import { makeCamelcase, makeUnderscore } from '../utils';
import { SERVER_URL } from './url';



export const defaultClient = axios.create({
    baseURL: `${SERVER_URL}/api`,
    headers: {
        'Accept': 'application/json'
    }
});


export interface ErrorResponseData {
    error?: string
}

defaultClient.interceptors.response.use((res) => {
    res.data = makeCamelcase(res.data, ['contents', 'config_contents']);
    return res;
}, (err: AxiosError<ErrorResponseData>) => {
    if (err.response) {
        err.response.data = makeCamelcase(err.response.data);
    }
    return Promise.reject(err);
});


defaultClient.interceptors.request.use((res) => {
    res.data = makeUnderscore(res.data, ['contents', 'configContents']);
    return res;
}, (err) => {
    return Promise.reject(err);
});

