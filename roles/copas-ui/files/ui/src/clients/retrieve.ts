import axios, { AxiosError, AxiosInstance } from 'axios';
import { makeCamelcase, makeUnderscore } from '../utils';
import { SERVER_URL } from './url';
import { toast } from 'react-toastify';
import i18n from 'i18next';


export const retrieveClient: AxiosInstance = axios.create({
    baseURL: `${SERVER_URL}/api`,
    headers: {
        'Accept': 'application/json'
    }
});

interface ErrorResponseData {
    error?: string
}

retrieveClient.interceptors.response.use((res) => {
    res.data = makeCamelcase(res.data, ['contents', 'config_contents']);
    return res;
}, (err: AxiosError<ErrorResponseData>) => {
    if (err.response) {
        const data = err.response.data;
        err.response.data = makeCamelcase(err.response.data);
        if (data.error) {
            toast.error(`${i18n.t('messages.retrieve_unsuccessful')} - ${data.error}`);
        } else {
            toast.error(i18n.t('messages.retrieve_unsuccessful'));
        }
    }
    return Promise.reject(err);
});


retrieveClient.interceptors.request.use((res) => {
    res.data = makeUnderscore(res.data, ['contents', 'configContents']);
    return res;
}, (err) => {
    return Promise.reject(err);
});

