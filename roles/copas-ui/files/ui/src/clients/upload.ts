import axios from 'axios';
import { SERVER_URL } from './url';

export const uploadClient = axios.create({
    baseURL: `${SERVER_URL}/api`,
    headers: {
        'Content-Type': 'multipart/form-data'
    }
});