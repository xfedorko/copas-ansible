import { operationClient } from '../../clients/operation';
import { retrieveClient } from '../../clients/retrieve';
import { DeleteFilesDto, NewDirectoryDto, PathFilesDto } from './dto';


export const fetchPathFiles = async (path: string) => {
    const res = await retrieveClient.get<PathFilesDto>(`/files?path=${path}`);
    return res.data;
};

export const newDirectory = async (newDirectory: NewDirectoryDto) => {
    const res = await operationClient.post(`/files/directory?path=${newDirectory.path}&name=${newDirectory.name}`);
    return res.data;
};

export const deleteFiles = async (deleteFiles: DeleteFilesDto) => {
    if (deleteFiles.files.length === 0) return;
    const res = await operationClient.delete(`/files?path=${deleteFiles.path}&${deleteFiles.files.map(name => `name=${name}`).join('&')}`);
    return res.data;
};

