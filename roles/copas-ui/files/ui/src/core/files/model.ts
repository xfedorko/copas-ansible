
export type FileType = 'file' | 'dir' | 'slink' | 'archive'

export interface FileInfo {
    name: string,
    type: FileType,
    size: number,
    modified: number,
}

export interface FileFilter {
    name: string,
    apply: (f: FileInfo) => boolean
}

export type FileDisplayType = 'grid' | 'list'

export type FileManagerClipboardAction = 'copy' | 'move'

export interface FileManagerClipboard {
    action: FileManagerClipboardAction,
    paths: string[]
}
