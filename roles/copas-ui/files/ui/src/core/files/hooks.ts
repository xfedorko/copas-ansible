import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { Id, toast } from 'react-toastify';
import { uploadClient } from '../../clients/upload';
import { FileDisplayType, FileFilter,  FileInfo,  FileManagerClipboard } from './model';
import { findMatchingStrings } from './utils';
import { AxiosError } from 'axios';
import { hiddenFileFilter } from './filters';
import { deleteFiles, fetchPathFiles, newDirectory } from './api';
import { useTranslation } from 'react-i18next';

export const usePathFiles = (path: string) => {
    const query = useQuery(['path', path], () => fetchPathFiles(path), {
        retry: 0
    });
    return { ...query, files: query.data };
};

export const useNewDirectory = () => {
    const queryClient = useQueryClient();
    const mutation = useMutation(newDirectory, {
        onSuccess(res, vars) {
            queryClient.invalidateQueries(['path', vars.path]);
        }
    });
    return  { ...mutation, makeNewDirectory: mutation.mutate };
};

export const useDeleteFiles = () => {
    const queryClient = useQueryClient();
    const mutation = useMutation(deleteFiles, {
        onSuccess(res, vars) {
            queryClient.invalidateQueries(['path', vars.path]);
        }
    });
    return { ...mutation, deleteFiles: mutation.mutate };
};

export const useSelectedFiles = (files: FileInfo[]) => {
    const [selectionMask, setSelectionMask] = useState<boolean[]>(files.map(() => false));
    useEffect(() => {
        setSelectionMask(files.map(() => false));
    }, [files]);
    
    const selectedFiles = useMemo(() => {
        return files.filter((ff,i) => selectionMask[i]);
    }, [files, selectionMask]);

    return { selectedFiles, selectionMask, setSelectionMask };
};

export const useFileFilters = () => {
    const [filters, setFilters] = useState<FileFilter[]>([hiddenFileFilter]);

    const addFilter = (filter: FileFilter) => {
        const tmp = [...filters].filter(f => f.name !== filter.name);
        tmp.push(filter);
        setFilters(tmp);
    };

    const removeFilter = (name: string) => {
        setFilters([...filters].filter(f => f.name !== name));
    };

    return { filters, addFilter, removeFilter };
};

export const useFilteredFiles = (files: FileInfo[] | undefined, filters: FileFilter[]) => {
    const filteredFiles =  useMemo(() => {
        return files?.filter(f => {
            for (const c of filters) {
                if (!c.apply(f)) return false;
            }
            return true;
        }) || [];
    }, [files, filters]);

    return filteredFiles;
};

export const usePath = (path: string) => {
    const {files, isLoading, isError, refetch} = usePathFiles(path);
    const {filters, addFilter, removeFilter} = useFileFilters();
    const [displayType, setDisplayType] = useState<FileDisplayType>('list');
    const [clipboard, setClipboard] = useState<FileManagerClipboard>();
    const queryClient = useQueryClient();
    const filteredFiles = useFilteredFiles(files, filters);
    const { selectedFiles, selectionMask, setSelectionMask } = useSelectedFiles(filteredFiles);
    const { t } = useTranslation();

    const toastId = useRef<Id|null>(null);

    const upload = useCallback((uploadedFiles: FileList) => {
        const matches = findMatchingStrings(files?.map(f => f.name) || [], Array.from(uploadedFiles).map(f => f.name));
        if (matches.length > 0) {
            toast.error(t('messages.file_upload.file_overwrite_error'));
            return;
        }

        const totalFilesSize = Array.from(uploadedFiles).map(f => f.size).reduce((a,b) => a+b);
        const fileSizeThreshold = 20_000_000;

        const formData = new FormData();
        for (let i = 0; i < uploadedFiles.length; i++) { formData.append(`file${i}`, uploadedFiles[i]); }

        uploadClient.post(`/files?path=${path}`, formData, {
            onUploadProgress(pe: ProgressEvent) {
                const progress = (pe.loaded / pe.total);
                if (totalFilesSize >= fileSizeThreshold) {
                    if (toastId.current === null) {
                        toastId.current = toast(t('messages.file_upload.upload_in_progress'), { progress, hideProgressBar: false, progressStyle: {
                            background: 'grey'
                        }});
                    } else {
                        toast.update(toastId.current, { progress });
                        if (progress === 1 && totalFilesSize > 3 * fileSizeThreshold) {
                            toast.info(t('messages.file_upload.files_being_stored'));
                        }
                    }
                }
            }
        })
            .then(() => {
                toast.success(t('messages.file_upload.success'));
                queryClient.invalidateQueries(['path', path]);
            })
            .catch((err: AxiosError<any>) => {
                const error = err.response?.data?.error;
                if (error) toast.error(`${t('messages.file_upload.failure')}- ${error}`);
                else toast.error(t('messages.file_upload.failure'));
            })
            .finally(() => {
                if (toastId.current !== null) {
                    toast.done(toastId.current);
                    toastId.current = null;
                }
            });
    }, [path, files, queryClient]);

    return {
        files,
        filters,
        addFilter,
        removeFilter,
        filteredFiles,
        selectionMask,
        setSelectionMask,
        selectedFiles,
        displayType,
        setDisplayType,
        clipboard,
        setClipboard,
        isLoading,
        isError,
        refetch,
        upload
    };
};