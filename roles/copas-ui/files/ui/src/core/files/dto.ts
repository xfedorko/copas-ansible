import { FileInfo } from './model';


export type PathFilesDto = FileInfo[]

export interface NewDirectoryDto {
    path: string,
    name: string
}

export interface DeleteFilesDto {
    path: string,
    files: string[]
}