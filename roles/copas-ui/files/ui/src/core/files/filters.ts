import { FileFilter, FileInfo } from './model';

export const hiddenFileFilter: FileFilter = {
    name: 'hiddenFileFilter',
    apply: (f: FileInfo) => !f.name.startsWith('.')
};
