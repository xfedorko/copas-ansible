import { Dispatch, createContext } from 'react';
import { FileDisplayType, FileFilter, FileInfo, FileManagerClipboard } from './model';

const fileManagerDataDefaults: FileManagerData = {
    path: '/',
    onPathChange: () => {},
    files: [],
    filteredFiles: [],
    selectedFiles: [],
    selectionMask: [],
    setSelectionMask: () => {},
    filters: [], 
    addFilter: () => {},
    removeFilter: () => {},
    displayType: 'grid',
    setDisplayType: () => {},
    isLoading: false,
    isError: false,
    refetch: () => {},
    upload: () => {},
    setClipboard: () => {}
};

export interface FileManagerData {
    path: string,
    onPathChange: (newPath: string) => void,
    files: FileInfo[] | undefined,

    filteredFiles: FileInfo[],
    filters: FileFilter[],
    addFilter: (f: FileFilter) => void,
    removeFilter: (n: string) => void,

    selectionMask: boolean[],
    setSelectionMask: Dispatch<boolean[]>,
    selectedFiles: FileInfo[],

    displayType: FileDisplayType,
    setDisplayType: Dispatch<FileDisplayType>,

    clipboard?: FileManagerClipboard,
    setClipboard: Dispatch<FileManagerClipboard|undefined>,
    
    isLoading: boolean,
    isError: boolean,

    refetch: () => void,
    upload: (files: FileList) => void,
}

export const FileManagerContext = createContext<FileManagerData>(fileManagerDataDefaults);

interface PathData {
    path: string,
    setPath: Dispatch<string>
}

export const PathContext = createContext<PathData>({ 
    path: '',
    setPath: () => {}
});