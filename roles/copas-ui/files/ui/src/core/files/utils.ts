import { faFile, faFileZipper, faFolder } from '@fortawesome/free-solid-svg-icons';
import { FileType } from './model';

export const fullFileName = (path: string, name: string) => path.endsWith('/') ? `${path}${name}` : `${path}/${name}`;

export const fileColor = (fileType: FileType) => {
    if (fileType === 'dir') return '#ccc20e';
    if (fileType === 'slink') return '#d9997e';
    return 'grey';
};
  
export const fileIcon = (fileType: FileType) => {
    if (fileType === 'dir') return faFolder;
    if (fileType === 'archive') return faFileZipper;
    return faFile;
};

export const findMatchingStrings = (arr1: string[], arr2: string[]) => {
    const matches = [];
    for (const a of arr1) {
        for (const b of arr2) {
            if (a === b) matches.push(a);
        }
    }
    return matches;
};