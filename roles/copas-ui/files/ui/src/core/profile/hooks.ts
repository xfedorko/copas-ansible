import { useMutation, useQuery, useQueryClient } from 'react-query';
import { fetchActiveProfile, updateProfile } from './api';
import { ActiveProfileDto } from './dto';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';

export const useActiveProfile = () => {
    const query = useQuery<ActiveProfileDto>(['analysis', 'profile'], fetchActiveProfile, {
        retry: false
    });
    return { ...query, activeProfile: query.data };
};

export const useUpdateProfile = () => {
    const queryClient = useQueryClient();
    const { t } = useTranslation();
    const mutation = useMutation(updateProfile, {
        onSuccess: () => {
            toast.success(t('messages.update_profile_successful'));
        },
        onSettled: () => {
            queryClient.invalidateQueries(['analysis', 'profile']);
        }
    });
    return { ...mutation, updateProfile: mutation.mutate };
};