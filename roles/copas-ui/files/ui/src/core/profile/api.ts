import { defaultClient } from '../../clients/default';
import { operationClient } from '../../clients/operation';
import { ActiveProfileDto } from './dto';


export const fetchActiveProfile = async () => {
    const res = await defaultClient.get('/analysis/profile');
    return res.data;
};

export const updateProfile = async (profile: ActiveProfileDto) => {
    const res = await operationClient.post('analysis/profile', profile);
    return res.data;
};