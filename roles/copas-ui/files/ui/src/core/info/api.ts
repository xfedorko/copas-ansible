import { retrieveClient } from '../../clients/retrieve';

export const fetchModuleInfo = async () => {
    const res = await retrieveClient.get('/info');
    return res.data;
};