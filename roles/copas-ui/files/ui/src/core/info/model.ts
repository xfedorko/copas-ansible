import { StaticConfigTemplate } from '../config/template/model';

export type CopasModuleType = 'analysis' | 'app'

interface HelpSection {
    name: string,
    contents: string
}

type SectionLineType = 'text' | 'link' | 'mail'

interface SectionLine {
    type: SectionLineType,
    content: string,
    to?: string
}

interface Section {
    image?: string,
    lines?: SectionLine[]
}

export interface ActionItem {
    key: string,
    label: string
}

export interface AnalysisProfileOption {
    name: string,
    label: string
}

export interface AnalysisProfile {
    options: AnalysisProfileOption[]
}

export interface ModuleInfoData {
    containerName: string,
    moduleType: CopasModuleType,
    moduleName: string,
    moduleVersion: string,
    homeTitle: string,
    homeDescription: string,
    moduleAuthorName: string,
    moduleAuthorEmail: string,
    moduleAuthorInstitution: string,
    analysisUrl: string,
    analysisActions: ActionItem[],
    analysisProfile?: AnalysisProfile,
    configTemplates: StaticConfigTemplate[],
    helpSections: HelpSection[],
    background?: string,
    backgroundStyle?: any,
    sectionLeft?: Section,
    sectionRight?: Section,
    supportedLanguages: string[]
}