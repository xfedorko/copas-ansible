import { createContext } from 'react';
import { ModuleInfoData } from './model';

export const ModuleInfoContext = createContext<ModuleInfoData>({
    moduleType: 'analysis',
    moduleName: '',
    moduleVersion: '',
    containerName: '',
    homeTitle: '',
    homeDescription: '',
    moduleAuthorName: '',
    moduleAuthorEmail: '',
    moduleAuthorInstitution: '',
    analysisUrl: '',
    analysisActions: [],
    configTemplates: [],
    helpSections: [],
    supportedLanguages: []
});