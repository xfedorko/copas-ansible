import { useQuery } from 'react-query';
import { fetchModuleInfo } from './api';
import { ModuleInfoDto } from './dto';
import { useContext } from 'react';
import { ModuleInfoContext } from './context';

export const useModuleInfo = () => {
    const query = useQuery<ModuleInfoDto>('module-info', fetchModuleInfo, {
        refetchOnWindowFocus: false
    });
    return { ...query, moduleInfo: query.data };
};

export const useModuleInfoContext = () => {
    return useContext(ModuleInfoContext);
};