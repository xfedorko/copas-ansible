import { operationClient } from '../../clients/operation';
import { retrieveClient } from '../../clients/retrieve';
import { CreateWatchItemDto, WatchItemsDto } from './dto';

export const fetchWatchItems = async () => {
    const res = await retrieveClient.get<WatchItemsDto>('watch');
    return res.data;
};

export const createWatchItem = async (watchItem: CreateWatchItemDto) => {
    const res = await operationClient.post('watch', watchItem);
    return res.data;
};

export const deleteWatchItem = async (id: number) => {
    const res = await operationClient.delete(`watch?id=${id}`);
    return res.data;
};
