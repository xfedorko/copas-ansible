import { WatchItem } from './model';

export type WatchItemsDto = WatchItem[]

export interface CreateWatchItemDto {
    directoryPath: string, 
    configurationId: number
}