import { useMutation, useQuery, useQueryClient } from 'react-query';
import { createWatchItem, deleteWatchItem, fetchWatchItems } from './api';
import { useNavigate } from 'react-router-dom';


export const useWatchItems = () => {
    const query = useQuery('watch', fetchWatchItems,  {
        refetchOnWindowFocus: false
    });
    
    const watchItems = query.data?.map((d) => ({ 
        ...d,
        created: new Date(`${d.created}+00:00`)
    }));
    return { ...query, watchItems };
};

export const useCreateWatchItem = () => {
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    
    const mutation = useMutation(createWatchItem, {
        onSuccess() {
            queryClient.invalidateQueries('watch');
            navigate('/watch');
        }
    });

    return {...mutation, createWatchItem: mutation.mutate};
};

export const useDeleteWatchItem = () => {
    const queryClient = useQueryClient();
    const mutation = useMutation(deleteWatchItem, {
        onSuccess: () => {
            queryClient.invalidateQueries('watch');
        }
    });
    return { ...mutation, deleteWatchItem: mutation.mutate };
};