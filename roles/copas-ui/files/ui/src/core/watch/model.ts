export interface WatchItem {
    id: number,
    directoryPath: string,
    configurationId: number,
    created: Date
}