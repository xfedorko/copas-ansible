
export type ServiceStatus = 'up' | 'down'

export interface ServiceInfoItem {
    label: string,
    value: string | number
}

export interface ServiceInfo {
  name: string,
  status: ServiceStatus,
  info: ServiceInfoItem[]
}