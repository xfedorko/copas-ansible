import { defaultClient } from '../../clients/default';
import { ServiceStatusDto } from './dto';

export const fetchServiceStatus = async () => {
    const res = await defaultClient.get<ServiceStatusDto>('/status');
    return res.data;
};
