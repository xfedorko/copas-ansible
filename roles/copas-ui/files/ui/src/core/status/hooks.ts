import { useQuery } from 'react-query';
import { fetchServiceStatus } from './api';

export const useServiceStatus = () => {
    const query = useQuery('status', fetchServiceStatus, {
        refetchInterval: 10000,
        retry: false
    });
    return { ...query, serviceStatus: query.data };
};
