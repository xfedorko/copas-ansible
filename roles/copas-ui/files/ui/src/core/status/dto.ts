import { ServiceInfo } from './model';

export interface ServiceStatusDto {
    services?: ServiceInfo[]
}
