import { useCallback, useEffect, useState } from 'react';
import { ConfigContents } from '../model';
import { ConfigTemplateContents } from './model';
import { defaultConfigContents } from './utils';


export const useConfig = (initValues?: ConfigContents, templateContents?: ConfigTemplateContents) => {
    const [config, setConfig] = useState<ConfigContents|undefined>( initValues );

    useEffect(() => {
        if (initValues) {
            setConfig(initValues);
        }
        else if (templateContents) {
            setConfig(defaultConfigContents(templateContents));
        }
    },[templateContents, initValues, setConfig]);
      
    const updateConfig = useCallback((configKey: string, value: any) => {
        if (config) {
            const tmp = {...config};
            tmp[configKey] = value;
            setConfig(tmp);
        }
    }, [config, setConfig]);

    return { config, updateConfig };
};