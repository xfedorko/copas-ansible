import { ConfigContents } from '../model';
import { ConfigTemplateElement, ConfigTemplateControl, ConfigTemplateSection, ConfigTemplateContents, } from './model';

const updateDefaultValues = (element: ConfigTemplateElement, config: ConfigContents) => {
    if (element.contentType === 'control') {
        const control = element as ConfigTemplateControl;
        config[control.configKey] = control.defaultValue;
    } else if (element.contentType === 'section') {
        const section = element as ConfigTemplateSection;
        for (const c of section.contents) {
            updateDefaultValues(c, config);
        }
    }
};

export const defaultConfigContents = (templateContents: ConfigTemplateContents) => {
    const configContents: ConfigContents = {};
    for (const c of templateContents) {
        updateDefaultValues(c, configContents);
    }
    return configContents;
};