import { createContext } from 'react';
import { ConfigContents } from '../model';

export interface ConfigData {
    config: ConfigContents,
    updateConfig: (configKey: string, value: any) => void
}

export const ConfigContext = createContext<ConfigData>({
    config: {},
    updateConfig: () => {}
});