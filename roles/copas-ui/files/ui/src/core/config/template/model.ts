export type ConfigTemplateElementType = 'section' | 'control'

export interface BaseControlProps<T> {
    title: string,
    description?: string,
    configKey: string,
    readonly?: boolean,
    config: { [key: string]: any },
    updateConfig: (key: string, value: T) => void
}


export interface ConfigTemplateControl {
    controlType: string,
    title: string,
    configKey: string,
    description?: string,
    defaultValue?: any,
    [key: string]: any
}

export interface ConfigTemplateSection {
    title: string,
    contents: ConfigTemplateElement[]
}

export type ConfigTemplateElement = (ConfigTemplateSection | ConfigTemplateControl) & { contentType: ConfigTemplateElementType }

export type ConfigTemplateContents = ConfigTemplateElement[]

export interface StaticConfigTemplate {
    key: string,
    type: string,
    name: string,
    generation: boolean,
    contentsId: number,
    contents: ConfigTemplateContents,
    created: string
}