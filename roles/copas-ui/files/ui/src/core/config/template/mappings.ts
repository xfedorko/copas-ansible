import CheckboxControl from '../../../components/ConfigSettings/Controls/Checkbox';
import CodeControl from '../../../components/ConfigSettings/Controls/Code';
import FileControl from '../../../components/ConfigSettings/Controls/File';
import NumberControl from '../../../components/ConfigSettings/Controls/Number';
import RadioControl from '../../../components/ConfigSettings/Controls/Radio';
import SelectControl from '../../../components/ConfigSettings/Controls/Select';
import SliderControl from '../../../components/ConfigSettings/Controls/Slider';
import TextFieldControl from '../../../components/ConfigSettings/Controls/TextField';

export const templateControlMappings: { [key: string]: React.FC<any>} = {
    'checkbox': CheckboxControl,
    'code': CodeControl,
    'number': NumberControl,
    'slider': SliderControl,
    'text_field': TextFieldControl,
    'radio': RadioControl,
    'select': SelectControl,
    'file': FileControl
};