import { useMutation, useQuery, useQueryClient } from 'react-query';
import { createConfig, deleteSavedConfig, fetchConfig, fetchSavedConfigs, updateConfig } from './api';
import { ConfigItem, SavedConfigItem } from './model';
import { useNavigate } from 'react-router-dom';
import { ConfigItemDto } from './dto';
import { useCallback } from 'react';
import { useTranslation } from 'react-i18next';


export const useSavedConfigs = () => {
    const query = useQuery('config', fetchSavedConfigs, {
        refetchOnWindowFocus: false
    });

    const savedConfigItems: SavedConfigItem[] | undefined = query.data?.map((d: any) => ({ 
        ...d,
        modified: new Date(`${d.modified}+00:00`),
        created: new Date(`${d.created}+00:00`)
    }));

    return { ...query, savedConfigItems };
};

export const useIsConfigNameUnique = () => {
    const query = useSavedConfigs();

    const isConfigNameUnique = useCallback((name: string) => {
        const savedConfigNames = query.savedConfigItems?.map((i) => i.name);
        return !savedConfigNames?.includes(name);
    },[query.savedConfigItems]);
    
    return isConfigNameUnique;
};

export const useSaveConfig = () => {
    const navigate = useNavigate();
    const mutation = useMutation(createConfig, { 
        onSuccess: () => {
            navigate('/config');
        }
    });
    return { ...mutation, saveConfig: mutation.mutate };
};

export const useDeleteSavedConfig = () => {
    const queryClient = useQueryClient();
    const mutation = useMutation(deleteSavedConfig, {
        onSuccess: () => {
            queryClient.invalidateQueries('config');
        }
    });
    return { ...mutation, deleteSavedConfig: mutation.mutate };
};


export const useConfigItem = (id: number)  => {
    const query = useQuery<ConfigItemDto>(`config/${id}`, () => fetchConfig(id),  {
        refetchOnWindowFocus: false
    });
    return { ...query, configItem: query.data };
};

export const useUpdateSavedConfig = () => {
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const mutation = useMutation(updateConfig, {
        onSuccess(data, variables) {
            queryClient.invalidateQueries({ queryKey: `config/${variables.id}` });
            navigate('/config');
        }
    });
    return { ...mutation, updateConfig: mutation.mutate };
};

export const useConfigurationTitle = (configItem?: ConfigItem) => {
    const { t } = useTranslation();
    if (!configItem) return t('words.configuration');
    const { savedConfig, watchingDirectory } = configItem; 
    if (savedConfig) return `${t('words.configuration')} - ${savedConfig}`;
    if (watchingDirectory) return `${t('words.watch')} -  ${watchingDirectory}`;
    return t('words.configuration');
};