import { operationClient } from '../../clients/operation';
import { retrieveClient } from '../../clients/retrieve';
import { CreateConfigDto, SavedConfigItemsDto, UpdateSavedConfigDto } from './dto';
import { ConfigItem } from './model';

export const createConfig = async ({ name, note, contents, templateContentsId }: CreateConfigDto) => {
    const res = await operationClient.post('config/saved', { name, note, contents, templateContentsId});
    return res.data;
};

export const fetchSavedConfigs = async () => {
    const res = await retrieveClient.get<SavedConfigItemsDto>('config/saved');
    return res.data;
};

export const fetchConfig = async (id: number) => {
    const res = await retrieveClient.get<ConfigItem>(`config/${id}`);
    return res.data;
};

export const deleteSavedConfig = async (name: string) => {
    const res = await operationClient.delete(`config/saved?name=${name}`);
    return res.data;
};

export const updateConfig = async (config: UpdateSavedConfigDto) => {
    const res = await operationClient.patch(`config/${config.id}`, config.contents);
    return res.data;
};
