import { ConfigContents, ConfigItem, SavedConfigItem } from './model';

export interface CreateConfigDto {
    name: string,
    note?: string,
    contents: ConfigContents, 
    templateContentsId: number
}

export type SavedConfigItemsDto = SavedConfigItem[]

export type ConfigItemDto = ConfigItem

export interface UpdateSavedConfigDto {
    id: number,
    contents: ConfigContents
}