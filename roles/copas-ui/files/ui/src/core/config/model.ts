
export type ConfigContents = { [key:string]: any }

export interface ConfigItem {
    id: number
    configContents: ConfigContents,
    templateContentsId: number,
    watchingDirectory?: string,
    savedConfig?: string,
    created: Date,
    modified: Date
}

export interface SavedConfigItem {
    id: number,
    configurationId: number,
    name: string,
    note?: string,
    created: Date,
    modified: Date,
}


