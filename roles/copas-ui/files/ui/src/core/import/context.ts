import { createContext } from 'react';
import { ImportData } from './model';

export const ImportContext = createContext<ImportData>({
    selectedFiles: [],
    configData: {},
    setSelectedFiles: () => {},
    setConfigData: () => {},
});
