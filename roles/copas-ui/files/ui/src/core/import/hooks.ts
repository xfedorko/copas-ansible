import { useMutation, useQuery, useQueryClient } from 'react-query';
import { generateConfigContents, getFileInfo, importFiles } from './api';
import { useCallback, useContext, useMemo, useRef } from 'react';
import { Id, toast } from 'react-toastify';
import { useLocation, useNavigate } from 'react-router-dom';
import { AxiosError } from 'axios';
import { ImportContext } from './context';
import { fullFileName } from '../files/utils';
import { ImportDto } from './dto';
import { ConfigItem, SavedConfigItem } from '../config/model';
import { useModuleInfoContext } from '../info/hooks';
import { retrieveClient } from '../../clients/retrieve';
import { useTranslation } from 'react-i18next';


export const useImport = () => {
    const navigate = useNavigate();
    const queryClient = useQueryClient();
    const location = useLocation();
    const toastId = useRef<Id|undefined>();
    const { t } = useTranslation();
    const mutation = useMutation(importFiles, {
        onSuccess: () => {
            if (toastId.current) {
                toast.update(toastId.current, {
                    type: 'success',
                    render: () => t('messages.import.success'),
                    isLoading: false,
                    autoClose: 5000
                });
                if (location.pathname === window.location.pathname) {
                    navigate('/');
                }
            }
        },
        onError: (err: AxiosError<any>) => {
            if (toastId.current) {
                const error = err.response?.data?.error;
                toast.update(toastId.current, {
                    type: 'error',
                    render: () => error ? `${t('messages.import.failure')}: ${error}` : t('messages.import.failure'),
                    isLoading: false,
                    autoClose: 5000 
                });
            }
        },
        onSettled: () => {
            queryClient.invalidateQueries('history');
        },
        onMutate: () => {
            toastId.current = toast.info(t('messages.import.progress'), { isLoading: true, autoClose: false });
        }
    });

    return { ...mutation, importFiles: mutation.mutate };
};

export const useSelectedFilePaths =  () => {
    const { selectedFiles } = useContext(ImportContext);

    const res = useMemo(() => {
        const files = [
            ...selectedFiles
                .filter(f => f.type === 'file')
                .map(f => fullFileName(f.path, f.name)),
            ...selectedFiles
                .filter(f => f.type === 'dir')
                .map(f => (f.contents || [] )
                    .filter(c => c.selected).map(c => fullFileName(fullFileName(f.path, f.name),c.name))
                )
                .flat(1)
        ];
  
        const archives = selectedFiles
            .filter(f => f.type === 'archive')
            .map(a => ({ 
                path: fullFileName(a.path, a.name), 
                contents: (a.contents || [])
                    .filter(c => c.selected)
                    .map(c => c.name)
            }));

        const watch = selectedFiles.filter(f => f.watch).map(f => fullFileName(f.path, f.name));

        return { files, archives, watch };
    },[selectedFiles]);

    return res;
};

export const useImportWithinContext = () => {
    const { configData } = useContext(ImportContext);
    const { files, archives, watch } = useSelectedFilePaths();
    const mutation = useImport();

    const importWithinContext = (saveInfo: {name:string, note?: string} | undefined) => {   
        if (!configData.template) {
            return;
        }
    
        const importData: ImportDto = {
            files,
            archives,
            config: configData.contents || {},
            template: {
                type: configData.template.type,
                key: configData.template.key,
                contentsId: configData.template.contentsId
            },
            watch
        };
      
        if (configData.savedConfig === undefined && saveInfo) {
            importData.save = saveInfo;
        }
        mutation.importFiles(importData);
    };
    return { ...mutation, importWithinContext };
};


export const useDuplicateSavedConfig = () => {
    const { configTemplates } = useModuleInfoContext();
    const { setConfigData } = useContext(ImportContext);
    const navigate = useNavigate();
    const { t } = useTranslation();

    const duplicateSavedConfig = useCallback((item: SavedConfigItem) => {
        retrieveClient.get(`config/${item.configurationId}`).then(r => {
            const json = r.data as ConfigItem;
            const template = configTemplates.find(t => t.contentsId === json.templateContentsId);
            if (template) {
                setConfigData({
                    template: template,
                    contents: json.configContents
                });
                navigate('/import/create');
            } else {
                toast.error(t('messages.invalid_template'));
            }
        });
    },[navigate, setConfigData, configTemplates]);

    return duplicateSavedConfig;
};

export const useChooseSavedConfig = () => {
    const { configTemplates } = useModuleInfoContext();
    const { setConfigData } = useContext(ImportContext);
    const navigate = useNavigate();
    const { t } = useTranslation();

    const chooseSavedConfig = useCallback((item: SavedConfigItem) => {
        retrieveClient.get(`config/${item.configurationId}`).then(r => {
            const json = r.data as ConfigItem;
            const template = configTemplates.find(t => t.contentsId === json.templateContentsId);
            if (template) {
                setConfigData({
                    savedConfig: item,
                    template: template,
                    contents: json.configContents
                });
                navigate('/import/summary');
            } else {
                toast.error(t('messages.invalid_template'));
            }
        });
    },[navigate, setConfigData, configTemplates]);

    return chooseSavedConfig;
};

export const useGenerateConfigContents = (templateKey: string, path: string) => {
    const { configTemplates } = useModuleInfoContext();
    const { setConfigData } = useContext(ImportContext);
    const navigate = useNavigate();

    const query = useQuery(['config', 'generate', templateKey, path],() => generateConfigContents({ templateKey, path }), {
        onSuccess: (data) => {
            setConfigData({
                template: configTemplates.find(t => t.key === templateKey),
                contents: data
            });
            navigate('/import/create');
        },
        enabled: false,
        refetchOnWindowFocus: false,
        retry: 0
    });

    return { ...query };
};

export const useFileContents = (idx: number, path: string) => {
    const { selectedFiles, setSelectedFiles } = useContext(ImportContext);
    const selectedFile = selectedFiles[idx];
    const query = useQuery(['import', 'info', path], () => getFileInfo({path}),{
        onSuccess: (res) => {
            const tmpFiles = [...selectedFiles];
            tmpFiles[idx].contents = res.contents.map(c => ({ name: c, selected: true }));
            setSelectedFiles(tmpFiles);
        },
        enabled: selectedFile.type === 'dir' || selectedFile.type === 'archive' || selectedFile.contents !== undefined
    });
    return query;
};