import { Dispatch } from 'react';
import { ConfigContents, SavedConfigItem } from '../config/model';
import { StaticConfigTemplate } from '../config/template/model';
import { FileType } from '../files/model';

export type ConfigMode = 'save' | 'update' | 'noaction'

export interface ContentFileInfo {
    name: string,
    selected: boolean
}

export interface SelectedFileInfo {
    path: string,
    name: string,
    type: FileType,
    watch?: boolean
    contents?: ContentFileInfo[],
}

export interface ConfigImportData {
    savedConfig?: SavedConfigItem,
    template?: StaticConfigTemplate,
    contents?: ConfigContents
}

export interface ImportData {
    selectedFiles: SelectedFileInfo[],
    setSelectedFiles: Dispatch<SelectedFileInfo[]>,
    configData: ConfigImportData,
    setConfigData: Dispatch<ConfigImportData>
}
