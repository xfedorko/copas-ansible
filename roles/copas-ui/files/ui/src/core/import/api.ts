import { defaultClient } from '../../clients/default';
import { operationClient } from '../../clients/operation';
import { retrieveClient } from '../../clients/retrieve';
import { ConfigContents } from '../config/model';
import { GenerateConfigContents, ImportDto, ImportFileInfoDto } from './dto';

export const importFiles = async (data: ImportDto) => {
    const res = await defaultClient.post('import', data);
    return res.data;
};

export const generateConfigContents = async ({ templateKey, path }: GenerateConfigContents) => {
    const res = await operationClient.get<ConfigContents>(`config/generate?key=${templateKey}&path=${path}`);
    return res.data;
};

export const getFileInfo = async ({ path }: { path: string}) => {
    const res = await retrieveClient.get<ImportFileInfoDto>(`/import?file=${path}`);
    return res.data;
};