import { ConfigContents } from '../config/model';

export interface ArchiveImport {
    path: string,
    contents: string[]
}

export interface StaticTemplateImport {
    type: string,
    key: string,
    contentsId: number
}


export interface DynamicTemplateImport {
    type: string,
    key: string,
    contents: string
}

export interface SaveConfigInfo {
    name: string,
    note?: string
}

export interface ImportDto {
    files: string[],
    archives: ArchiveImport[],
    config: ConfigContents,
    template: StaticTemplateImport | DynamicTemplateImport,
    watch: string[],
    save?: SaveConfigInfo
}

export interface GenerateConfigContents {
    path: string,
    templateKey: string
}

export interface ImportFileInfoDto {
    contents: string[]
}