import { retrieveClient } from '../../clients/retrieve';
import { HistoryItemDto } from './dto';

export const fetchHistoryItems = async () => {
    const res = await retrieveClient.get<HistoryItemDto>('history');
    return res.data;
};