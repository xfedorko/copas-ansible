import { RowData, HistoryItem } from './model';

export const importedFilesCount = (row: RowData) => row.files.length + 
    row.archives.reduce((acc, arch) => acc + arch.contents.length, 0);
export const archivesCount = (row: RowData) => row.archives.length;
export const shouldBeExpanded = (row: RowData) => row.files.length > 1 || row.archives.length > 0; 
export const archivePaths= (item: HistoryItem) => item.archives.map(a => a.archivePath);
export const importedFilePaths = (item: HistoryItem) => [...item.files.map(f => f.path), ...item.archives.map(a => a.contents).flat(1).map(f => f.path)];

export const importedFilesCountWithStatus = (row: RowData, status: string) => 
    row.files.filter(f => f.status === status).length +
    row.archives.map(a => a.contents).flat(1).filter(a => a.status === status).length;

export const statusColor = (status: string) => {
    if (status === 'success') return 'green';
    if (status === 'failure') return 'red';
    if (status === 'unknown') return 'purple';
    return undefined;
};

export const getDurationString = (start: Date, end: Date) => {
    const seconds = Math.floor((end.getTime() - start.getTime()) / 1000);
    const minutes = Math.floor(seconds / 60);
    const hours = Math.floor(minutes / 60);

    if (seconds < 60) return `${seconds}s`;
    if (minutes < 60) return `${minutes}m ${seconds % 60}s`;
    return `${hours}h ${Math.round(minutes % 60)}m`;
};