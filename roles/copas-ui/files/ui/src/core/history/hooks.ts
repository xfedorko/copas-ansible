import { useQuery } from 'react-query';
import { fetchHistoryItems } from './api';
import { useEffect, useState } from 'react';
import { HistoryItem } from './model';
import { archivePaths, getDurationString, importedFilePaths } from './utils';

export const useHistoryItemsQuery = () => {
    const query = useQuery('history', fetchHistoryItems);
    return { ...query, historyItems: query.data };
};

export const useHistoryItems = () => {
    const query = useHistoryItemsQuery();
    const [filteredHistoryItems, setFilteredHistoryItems] = useState<HistoryItem[]>();

    useEffect(() => {
        setFilteredHistoryItems(query.historyItems);
    }, [query.historyItems]);

    const filterFiles = (text: string) => {
        setFilteredHistoryItems(query.historyItems?.filter(hi => {
            const paths = [...importedFilePaths(hi), ...archivePaths(hi)];
            return paths.some(p => p.includes(text));
        }));
    };

    return { ...query, filteredHistoryItems, filterFiles };
};


export const useDuration = ( start: Date ) => {
    const [now, setNow] = useState(new Date());
    useEffect(() => {
        const id = setInterval(() => setNow(new Date()), 1000);
        return () => clearInterval(id);
    }, [setNow]);
    return getDurationString(start, now);
};
