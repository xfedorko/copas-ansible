import { HistoryItem, RowData } from './model';

export const getHistoryItemsTableRows = (historyItems?: HistoryItem[]): RowData[] | undefined => {
    return historyItems?.map(hi => ({
        key: hi.id,
        start: new Date(`${hi.start}+00:00`),
        end: hi.end ? new Date(`${hi.end}+00:00`) : undefined,
        status: hi.status,
        message: hi.message,
        origin: hi.origin,
        files: hi.files,
        archives: hi.archives,
        configurationId: hi.configurationId
    }));
};