export type ImportStatus = 'success' | 'failure' | 'unknown' | 'unsupported'

export interface FileImportStatus {
    path: string,
    status: ImportStatus
}

export interface ArchiveImportStatus {
    archivePath: string,
    contents: FileImportStatus[]
}

export interface HistoryItem {
    id: number,
    configurationId: number,
    origin: string,
    status: ImportStatus,
    message?: string,
    files: FileImportStatus[],
    archives: ArchiveImportStatus[],
    start: string,
    end?: string
}

export interface RowData {
    key: number,
    start: Date,
    end?: Date,
    status: ImportStatus,
    message?: string,
    origin: string,
    files: FileImportStatus[],
    archives: ArchiveImportStatus[],
    configurationId: number
}