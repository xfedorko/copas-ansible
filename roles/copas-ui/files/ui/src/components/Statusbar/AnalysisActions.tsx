import React from 'react';
import { Button, Popover, Space } from 'antd';
import { UpCircleOutlined } from '@ant-design/icons';
import AnalysisActionButton from './AnalysisActionButton';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';

const AnalysisActions = () => {
    const moduleInfo = useModuleInfoContext();
    const { t } = useTranslation();
    if (moduleInfo.analysisActions.length === 0) return null;
    return (
        <Popover title={t('statusbar.actions.title')} content={<Space direction="vertical" style={{ width: '100%' }}>
            {
                moduleInfo.analysisActions.map(a => (
                    <AnalysisActionButton key={a.key} action={a}/>
                ))
            }
        </Space>}>
            <Button size='small' type='text' icon={<UpCircleOutlined/>}/>
        </Popover>
    );
};

export default AnalysisActions;