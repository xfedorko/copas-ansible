import React from 'react';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Statusbar.module.css';
import { Popover } from 'antd';
import { useServiceStatus } from '../../core/status/hooks';
import { ServiceInfo } from '../../core/status/model';
import { useTranslation } from 'react-i18next';
import { AxiosError } from 'axios';
import { ErrorResponseData } from '../../clients/default';


const ServiceStatusInfo: React.FC<ServiceInfo> = ({ status, info }) => {
    if (status === 'down') return <span>Service can&#39;t be reached</span>;
    return <table>
        <tbody>
            {
                info.map(({ label, value }) => (
                    <tr key={label}>
                        <td>{label}</td>
                        <td style={{width: '2em'}}></td>
                        <td>{value}</td>
                    </tr>
                ))
            }
        </tbody>
    </table>;
};

const ServicesStatus = () => {
    const { isError, error, isLoading, serviceStatus } = useServiceStatus();
    const { t } = useTranslation();
    if (isLoading) {
        return <FontAwesomeIcon icon={faSpinner} size='sm' spin={true}/>;
    }
    if (isError) {
        const axiosError = error as AxiosError<ErrorResponseData>;
        if (axiosError?.response?.data?.error) {
            return <span className={styles['status-message']}>{axiosError?.response?.data?.error}</span>;
        } else {
            return <span className={styles['status-message']}>{t('statusbar.status.server_unreachable')}</span>;
        }

    }
    
    const services = serviceStatus?.services;

    if (!services) return null;

    return (
        <>
            {
                services.map(s => {
                    return (
                        <Popover
                            key={s.name}
                            placement='topLeft'
                            title={s.name}
                            content={<ServiceStatusInfo name={s.name} status={s.status === 'up' ? 'up' : 'down'} info={s.info || {}} />} >
                            <div className={styles.service}>
                                <div className={`${styles['service-status']} ${s.status === 'up' ? styles['service-status-up'] : styles['service-status-down']}`}/>
                                <span className={styles['service-name']}>{s.name}</span>
                            </div>
                        </Popover>
                    );
                })
            }
        </>
    );
};

export default ServicesStatus;