import React from 'react';
import ModuleInfo from './ModuleInfo';
import ServicesStatus from './ServicesStatus';
import styles from './Statusbar.module.css';
import AnalysisActions from './AnalysisActions';
import ProfileSelect from './ProfileSelect';
import { useModuleInfoContext } from '../../core/info/hooks';

interface StatusbarProps {
    className?: string
}

const Statusbar: React.FC<StatusbarProps> = ({className}) => {
    const { analysisProfile } = useModuleInfoContext();
    return (
        <div className={`${styles.container} ${className}`}>
            <div className={styles['service-section']}>
                <ServicesStatus/>
                <AnalysisActions/>
                { analysisProfile && <ProfileSelect/> }
            </div>
            <div className={styles['contact-section']}>
                <a href='https://copas.cerit-sc.cz'>https://copas.cerit-sc.cz</a>
                <a href='mailto:rebok@ics.muni.cz'>rebok@ics.muni.cz</a>
            </div>
            <div className={styles['info-section']}>
                <ModuleInfo/>
            </div>
        </div>
    );
};

export default Statusbar;