import React, { useState } from 'react';
import { Button } from 'antd';
import { toast } from 'react-toastify';
import { ActionItem } from '../../core/info/model';
import { operationClient } from '../../clients/operation';
import { useTranslation } from 'react-i18next';

interface AnalysisActionButtonProps {
    action: ActionItem
}

const AnalysisActionButton: React.FC<AnalysisActionButtonProps> = ({ action: {key, label} }) => {
    const [loading, setLoading] = useState(false);
    const { t } = useTranslation();
    return (
        <Button block loading={loading} size='small' type='primary' onClick={() => {
            setLoading(true);
            operationClient.post('analysis/action', {
                key
            }).then(r => {
                const error = r.data?.error;
                const message = r.data?.message;
                if (error) toast.error(error);
                else if (message) toast.success(message);
                else toast.success(t('messages.action_succesful'));
            }).finally(() => setLoading(false));
        }}>{label}</Button>
    );
};

export default AnalysisActionButton;