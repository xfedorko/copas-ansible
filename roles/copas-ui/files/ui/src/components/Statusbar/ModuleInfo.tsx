
import React from 'react';
import { useWindowSize } from '../../hooks';
import { useModuleInfo, useModuleInfoContext } from '../../core/info/hooks';
import { Select } from 'antd';
import { useTranslation } from 'react-i18next';
import Flags from 'country-flag-icons/react/3x2';


const getFlagLabel = (code: string) => {
    if (code === 'en') return <Flags.GB width={18}/>;
    if (code === 'de') return <Flags.DE width={18}/>;
    if (code === 'cs') return <Flags.CZ width={18}/>;
    return code;
};


const ModuleInfo = () => {
    const { moduleName, moduleVersion, containerName, supportedLanguages } = useModuleInfoContext();
    const { refetch } = useModuleInfo();
    const { width } = useWindowSize();
    const info = width < 650 ? containerName : `${containerName} \xa0\xa0\xa0\xa0 [ ${moduleName} : ${moduleVersion} ]`;
    const { i18n } = useTranslation('home');
    const currentLanguage = supportedLanguages.includes(i18n.language) ? i18n.language : 'en';
    const languageOptions = supportedLanguages.map(l => ({ label: getFlagLabel(l), value: l }));

    return (
        <>
            <Select
                size='small'
                value={currentLanguage}
                onChange={(val) => {
                    i18n.changeLanguage(val).then(() => refetch());
                }}
                options={languageOptions}
            />
            <span>{info}</span>
        </>

    );
};

export default ModuleInfo;