import React from 'react';
import { Select } from 'antd';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useActiveProfile, useUpdateProfile } from '../../core/profile/hooks';
import { useTranslation } from 'react-i18next';

const ProfileSelect = () => {
    const { analysisProfile } = useModuleInfoContext();
    const { activeProfile, isLoading } = useActiveProfile();
    const { updateProfile, isLoading: updating } = useUpdateProfile();
    const { t } = useTranslation();
    if (!analysisProfile || analysisProfile.options.length === 0) return null;
    const options = analysisProfile.options.map(o => ({ label: o.label, value: o.name })).reverse();
    return (
        <div>
            <span>{t('statusbar.profile.label')}: </span>
            <Select
                size='small'
                loading={isLoading || updating}
                disabled={isLoading || updating}
                style={{ width: 100 }}
                value={activeProfile?.name || null}
                onChange={(val) => updateProfile({ name: val })}
                options={options}
            />
        </div>
    );
};

export default ProfileSelect;