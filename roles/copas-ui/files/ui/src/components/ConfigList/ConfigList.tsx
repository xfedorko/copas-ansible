import { faCog } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { List } from 'antd';
import React, { HTMLProps, ReactNode } from 'react';
import styles from './ConfigList.module.css';
import { useSavedConfigs } from '../../core/config/hooks';
import { SavedConfigItem } from '../../core/config/model';

const getDate = (item: SavedConfigItem) => item.modified.toLocaleString('cs-CZ', { year: 'numeric', month: '2-digit', day: '2-digit', hour: '2-digit', minute: '2-digit'});

export interface ConfigListProps extends HTMLProps<HTMLDivElement>  {
    header?: ReactNode,
    actions?: (item: SavedConfigItem) => ReactNode[] | undefined
}

const ConfigList: React.FC<ConfigListProps> = ({ header, actions, className }) => {
    const { savedConfigItems, isLoading } = useSavedConfigs();
    return (
        <List className={className} dataSource={savedConfigItems} itemLayout='horizontal' loading={isLoading} style={{minHeight: '100%'}}
            header={header} 
            renderItem={item => (
                <List.Item actions={actions ? actions(item) : undefined}>
                    <List.Item.Meta 
                        avatar={<FontAwesomeIcon icon={faCog} size='2x'/>} 
                        title={item.name} 
                        description={getDate(item)} />
                    <div className={styles.note}>{item.note}</div> 
                </List.Item>
            )}/>
    );
};

export default ConfigList;