import React from 'react';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { To, useNavigate } from 'react-router-dom';

interface BackButtonProps {
    route?: To
}

const BackButton: React.FC<BackButtonProps> = ({ route }) => {
    const navigate = useNavigate();
    return (
        <Button 
            icon={<ArrowLeftOutlined />} 
            type='ghost' shape='round'
            onClick={() => route ? navigate(route) : navigate(-1)} />
    );
};

export default BackButton;