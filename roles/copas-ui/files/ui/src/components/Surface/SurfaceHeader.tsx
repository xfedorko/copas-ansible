import React, { HTMLProps, ReactNode } from 'react';
import styles from './Surface.module.css';

export interface SurfaceHeaderProps extends HTMLProps<HTMLDivElement> {
  leftSection?: ReactNode,
  rightSection?: ReactNode,
}


const SurfaceHeader: React.FC<SurfaceHeaderProps> = ({ children, leftSection, rightSection, ...rest }) => {
    return (
        <header className={styles.header} {...rest}>
            <div className={styles.leftSection}>
                {leftSection}
            </div>
            <div className={styles.centerSection}>
                {children}
            </div>
            <div className={styles.rightSection}>
                {rightSection}
            </div>
        </header>
    );
};

export default SurfaceHeader;