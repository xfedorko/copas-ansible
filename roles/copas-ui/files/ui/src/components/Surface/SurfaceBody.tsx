import React, { HTMLProps } from 'react';
import styles from './Surface.module.css';
import Loading from '../Loading/Loading';
import { Alert, } from 'antd';

export interface SurfaceBodyProps extends HTMLProps<HTMLDivElement> {
  isLoading?: boolean,
  isError?: boolean
}


const SurfaceBody: React.FC<SurfaceBodyProps> = ({ isLoading, isError, children, ...rest }) => {
    return (
        <div className={styles.body} {...rest}>
            {isLoading && <Loading iconSize='3x'/>}
            {!isLoading && isError && <Alert type='error' style={{marginTop: '1em'}} message='Failed to load the data'/>}
            {!isLoading && !isError && children}
        </div>
    );
};

export default SurfaceBody;