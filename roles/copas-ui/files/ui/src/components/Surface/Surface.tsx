import React, { HTMLProps } from 'react';
import styles from './Surface.module.css';

export type SurfaceProps = HTMLProps<HTMLDivElement>

const Surface: React.FC<SurfaceProps> = ({ children, className, ...rest}) => {
    return (
        <div className={`${styles.container} ${className}`} {...rest}>
            {children}
        </div>
    );
};

export default Surface;