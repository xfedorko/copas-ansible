import React, { HTMLProps } from 'react';
import styles from './Surface.module.css';

export type SurfaceFooterProps = HTMLProps<HTMLDivElement>


const SurfaceFooter: React.FC<SurfaceFooterProps> = ({ children, ...rest }) => {
    return (
        <footer className={styles.footer} {...rest}>
            {children}
        </footer>
    );
};

export default SurfaceFooter;