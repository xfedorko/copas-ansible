import React from 'react';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import styles from './CopasLink.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, NavLink } from 'react-router-dom';
import { useWindowSize } from '../../hooks';

type LinkType = 'main' | 'other' | 'navigation'

interface CopasLinkProps {
    type: LinkType
    to: string,
    icon: IconDefinition,
    text: string,
}

const CopasLink: React.FC<CopasLinkProps> = ({ type, to, icon, text }) => {

    const { width } = useWindowSize();

    const isSmallScreen = width < 650;

    const children = (
        <>
            {
                isSmallScreen ? (
                    <FontAwesomeIcon icon={icon} size={'2x'}/>
                ) : (
                    <FontAwesomeIcon icon={icon} size={type === 'main' ? '5x' : '3x'}/>
                )
            }

            <span className={styles.text}>{text}</span>
        </>
    );

    const classes = `${styles.container} ${styles[type]}`;

    if (to.startsWith('http://') || to.startsWith('https://')) {
        return (
            <a href={to} className={classes}>
                {children}
            </a>
        );
    } else if (type === 'navigation') {
        return (
            <NavLink to={to} className={({ isActive }) => `${classes} ${(isActive ? styles.containerActive : styles.containerInactive)}`}>
                {children}
            </NavLink>
        );
    }
  
    return (
        <Link to={to} className={classes}>
            {children}
        </Link>
    );
};

export default CopasLink;