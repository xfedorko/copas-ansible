import { faBars, faClockRotateLeft, faComputer, faEye, faFileImport, faFolderTree, faGear, faHome, faMagnifyingGlassChart } from '@fortawesome/free-solid-svg-icons';
import React, { useState } from 'react';
import styles from './Navbar.module.css';
import CopasLink from '../Link/CopasLink';
import { Drawer, Menu, MenuProps } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useWindowSize } from '../../hooks';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';

const Navbar : React.FC = () => {
    const moduleInfo = useModuleInfoContext();
    const [drawerVisible, setDrawerVisible] = useState(false);
    const navigate = useNavigate();
    const location = useLocation();
    const { width } = useWindowSize();
    const isSmallScreen = width < 650;
    const { t } = useTranslation();

    const analysisItems: MenuProps['items'] = [
        {
            key: 'home',
            label: t('navigation.home')
        },
        {
            key: 'files',
            label: t('navigation.files')
        },
        {
            key: 'import',
            label: t('navigation.import')
        },
        {
            key: 'analysis',
            label: (
                <a href={moduleInfo.analysisUrl}>
                    {t('navigation.analysis')}
                </a>
            )
        },
        {
            key: 'config',
            label: t('navigation.config')
        },
        {
            key: 'watch',
            label: t('navigation.watch')
        },
        {
            key: 'history',
            label: t('navigation.history')
        },
    ];

    const appItems: MenuProps['items'] = [
        {
            key: 'home',
            label: t('navigation.home')
        },
        {
            key: 'files',
            label: t('navigation.files')
        },
        {
            key: 'analysis',
            label: (
                <a href={moduleInfo.analysisUrl}>
                    {t('navigation.app')}
                </a>
            )
        }
    ];

    return (
        <nav className={styles.container}>
            {
                !isSmallScreen ? (
                    <>
                        <CopasLink type='navigation' to='/' icon={faHome} text={t('navigation.home')}/>
                        <CopasLink type='navigation' to='/files' icon={faFolderTree} text={t('navigation.files')}/>
                        { moduleInfo.moduleType === 'analysis' && <CopasLink type='navigation' to='/import' icon={faFileImport} text={t('navigation.import')}/>}
                        <CopasLink type='navigation' to={moduleInfo.analysisUrl} 
                            icon={moduleInfo.moduleType === 'analysis' ? faMagnifyingGlassChart : faComputer} 
                            text={moduleInfo.moduleType === 'analysis' ? t('navigation.analysis') : t('navigation.app')}/>
                        { moduleInfo.moduleType === 'analysis' && <CopasLink type='navigation' to='/config' icon={faGear} text={t('navigation.config')}/>}
                        { moduleInfo.moduleType === 'analysis' && <CopasLink type='navigation' to='/watch' icon={faEye} text={t('navigation.watch')}/>}
                        { moduleInfo.moduleType === 'analysis' && <CopasLink type='navigation' to='/history' icon={faClockRotateLeft} text={t('navigation.history')}/>}
                    </>
                ) : (
                    <>
                        <div className={styles['menu-container']}>
                            <FontAwesomeIcon icon={faBars} size='3x' onClick={() => setDrawerVisible(!drawerVisible)}/>
                        </div>
                        <Drawer
                            title='CoPAS'
                            placement='left'
                            open={drawerVisible}
                            closable={true}
                            onClose={() => setDrawerVisible(false)}
                        >
                            <Menu
                                onClick={(e) => {
                                    if (e.key === 'home') {
                                        navigate('/');
                                    } else if (e.key !== 'analysis') {
                                        navigate(`/${e.key}`);
                                    }
                                    setDrawerVisible(false);
                                }}
                                defaultOpenKeys={[location.pathname]}
                                mode='vertical'
                                items={moduleInfo.moduleType === 'analysis' ? analysisItems : appItems}
                            />
                        </Drawer>
                    </>
                )
            }

        </nav>
    );
};

export default Navbar;