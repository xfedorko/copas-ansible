import React, { useContext } from 'react';
import { Collapse } from 'antd';
import styles from './ConfigSettings.module.css';
import { ConfigTemplateControl, ConfigTemplateElement, ConfigTemplateSection } from '../../core/config/template/model';
import { ConfigContext } from '../../core/config/template/context';
import { templateControlMappings } from '../../core/config/template/mappings';

interface TemplateElementProps {
    readonly?: boolean
}
  
const TemplateElement: React.FC<TemplateElementProps & ConfigTemplateElement> = ({ contentType, readonly, ...rest }) => {
    const { config, updateConfig } = useContext(ConfigContext);
    if (contentType === 'control') {
        const { controlType } = rest as ConfigTemplateControl;
        const TemplateControl = templateControlMappings[controlType];
        if (TemplateControl === undefined) return null;
        return <TemplateControl {...rest} readonly={readonly} config={config} updateConfig={updateConfig} />;
    }
    if (contentType === 'section') {
        const section = rest as ConfigTemplateSection;
        return (
            <Collapse>
                <Collapse.Panel header={section.title} key={section.title}>
                    <div className={styles['contents-container']}>
                        {
                            section.contents.map((c, j) => (
                                <TemplateElement key={j} readonly={readonly} {...c} />
                            ))
                        }
                    </div>
                </Collapse.Panel>
            </Collapse>
        );
    }
    return null;
};

export default TemplateElement;