import React, { HTMLProps } from 'react';
import Surface from '../Surface/Surface';
import styles from './ConfigSettingsEdit.module.css';
import { Button } from 'antd';
import SurfaceHeader from '../Surface/SurfaceHeader';
import SurfaceBody from '../Surface/SurfaceBody';
import SurfaceFooter from '../Surface/SurfaceFooter';
import ConfigSettings from './ConfigSettings';
import { ConfigContents } from '../../core/config/model';
import { ConfigTemplateContents } from '../../core/config/template/model';
import { ConfigContext } from '../../core/config/template/context';
import { useConfig } from '../../core/config/template/hooks';
import BackButton from '../Buttons/BackButton';
import { useTranslation } from 'react-i18next';

export interface ConfigSettingsEditProps extends HTMLProps<HTMLDivElement>  {
  title?: string,
  initValues?: ConfigContents,
  templateContents?: ConfigTemplateContents,
  onConfirmSettings?: (values: ConfigContents) => void,
  isLoading?: boolean
}

const ConfigSettingsEdit: React.FC<ConfigSettingsEditProps> = ({ title, initValues, templateContents, onConfirmSettings, isLoading = false, ...rest }) => {
    const {config, updateConfig} = useConfig(initValues, templateContents);
    const { t } = useTranslation();

    return (
        <Surface className={styles.container} {...rest}>
            <SurfaceHeader leftSection={<BackButton/>}>
                <h2>{title ? title : t('words.configuration')}</h2>                  
            </SurfaceHeader>
            <SurfaceBody isLoading={!config || isLoading} isError={!templateContents}>
                {
                    config && (
                        <ConfigContext.Provider value={{config, updateConfig}}>
                            <ConfigSettings templateContents={templateContents}/>
                        </ConfigContext.Provider>     
                    )
                }
            </SurfaceBody>
            <SurfaceFooter>
                <Button type='primary' shape='round' size='large' disabled={!config} onClick={() => {
                    if (config) onConfirmSettings?.(config);
                }}>{t('words.confirm')}</Button>
            </SurfaceFooter>
        </Surface>
    );
};

export default ConfigSettingsEdit;