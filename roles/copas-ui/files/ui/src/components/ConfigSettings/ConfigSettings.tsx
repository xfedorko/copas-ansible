import React from 'react';
import styles from './ConfigSettings.module.css';
import { ConfigTemplateContents } from '../../core/config/template/model';
import TemplateElement from './TemplateElement';

interface ConfigSettingsProps {
  templateContents?: ConfigTemplateContents,
  readonly?: boolean,
  className?: string
}

export const ConfigSettings: React.FC<ConfigSettingsProps> = ({ templateContents, className, readonly = false }) => {
    if (!templateContents) return null;
    return (
        <div className={`${className} ${styles.container} ${styles['contents-container']}`}>
            {
                templateContents.map((c, j) => {
                    return <TemplateElement key={j} readonly={readonly} {...c} />;
                })
            }
        </div>
    );
};

export default ConfigSettings;