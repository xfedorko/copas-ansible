import React from 'react';
import { Select } from 'antd';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

interface SelectOption {
    value: any,
    label: string
}

export interface SelectControlProps extends BaseControlProps<any> {
    options: SelectOption[]
}

const SelectControl: React.FC<SelectControlProps> = (props) => {
    const { configKey, title, description, readonly = false, config, updateConfig, options } = props;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Select
                value={config[configKey]}
                style={{ width: '50%' }}
                onChange={(val: any) => {
                    if (!readonly) updateConfig(configKey, val);
                }}
                options={options}
            />
        </ControlWrapper>
    );
};

export default SelectControl;