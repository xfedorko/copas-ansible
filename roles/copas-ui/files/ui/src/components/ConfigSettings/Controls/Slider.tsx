import React from 'react';
import { Slider } from 'antd';
import type { SliderMarks } from 'antd/es/slider';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

export interface SliderControlProps extends BaseControlProps<number> {
    min?: number,
    max?: number
}

const SliderControl: React.FC<SliderControlProps> = (props) => {
    const { configKey, title, description, min = 0, max = 100, readonly = false, config, updateConfig } = props;
    const marks: SliderMarks = {};
    marks[min] = min;
    marks[max] = max;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Slider min={min} max={max} value={config[configKey]} marks={marks}
                onChange={(n) => {
                    if (!readonly) updateConfig(configKey, n);
                }}
                style={{'width': '50%'}} />
        </ControlWrapper>
    );
};

export default SliderControl;