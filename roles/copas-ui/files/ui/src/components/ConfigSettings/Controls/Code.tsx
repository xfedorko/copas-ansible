import React from 'react';
import CodeMirror from '@uiw/react-codemirror';
import ControlWrapper from './ControlWrapper';
import { logstash } from 'codemirror-lang-logstash';
import { json } from '@codemirror/lang-json';
import { xml } from '@codemirror/lang-xml';
import { BaseControlProps } from '../../../core/config/template/model';

const getLanguageExtension = (lang?: string) => {
    if (lang === 'json') return json();
    if (lang === 'logstash') return logstash();
    if (lang === 'xml') return xml();
};

interface CodeControlProps extends BaseControlProps<string> {
    lang?: string
}

const CodeControl : React.FC<CodeControlProps> = (props) => {
    const { configKey, lang, title, description, readonly = false, config, updateConfig } = props;

    const onChange = (value: string) => {
        updateConfig(configKey, value);
    };

    const extensions = [];
    const languageExtension = getLanguageExtension(lang);
    if (languageExtension) extensions.push(languageExtension);

    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <CodeMirror
                value={config[configKey]}
                height='32em'
                width='100%'
                onChange={onChange}
                editable={!readonly}
                extensions={extensions}
                style={{ border: '1px solid #BBB' }}
            />
        </ControlWrapper>
    );
};

export default CodeControl;