import React from 'react';
import { Checkbox } from 'antd';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

interface CheckboxControlProps extends BaseControlProps<boolean> {
    label?: string
}

const CheckboxControl: React.FC<CheckboxControlProps> = (props) => {
    const { configKey, title, description, readonly = false, config, updateConfig, label } = props;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Checkbox checked={config[configKey]} onChange={(e) => {
                if (!readonly) updateConfig(configKey, e.target.checked);
            }}>
                { label }      
            </Checkbox>
        </ControlWrapper>
    );
};

export default CheckboxControl;