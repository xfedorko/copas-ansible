import React from 'react';
import { InputNumber } from 'antd';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

export interface NumberControlProps extends BaseControlProps<number> {
    min?: number,
    max?: number
}

const NumberControl: React.FC<NumberControlProps> = (props) => {
    const { configKey, title, description, min, max, readonly = false, config, updateConfig } = props;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <InputNumber readOnly={readonly}
                value={config[configKey]} min={min} max={max}
                onChange={(e) => {
                    if (!readonly) updateConfig(configKey, e);
                }}
            />
        </ControlWrapper>
    );
};

export default NumberControl;