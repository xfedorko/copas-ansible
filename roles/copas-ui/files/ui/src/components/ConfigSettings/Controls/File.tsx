import React, { useContext, useState } from 'react';
import ControlWrapper from './ControlWrapper';
import { Button, Input, Modal, Row } from 'antd';
import FileManager from '../../FileManager/FileManager';
import { getFullPath } from '../../../utils';
import { PathContext } from '../../../core/files/context';
import { BaseControlProps } from '../../../core/config/template/model';
import { FileType } from '../../../core/files/model';

interface FileControlProps extends BaseControlProps<string> {
    fileType?: FileType
}

const FileControl : React.FC<FileControlProps> = (props) => {
    const { configKey, title, description, readonly = false, config, updateConfig, fileType } = props;
    const [isModalOpen, setIsModalOpen] = useState(false);
    const { path } = useContext(PathContext);
    const [activePath, setActivePath] = useState(path);
    const [selectedPath, setSelectedPath] = useState<string>();

    const showModal = () => {
        setIsModalOpen(true);
    };
  
    const handleOk = () => {
        if (selectedPath) {
            updateConfig(configKey, selectedPath);
        }

        setIsModalOpen(false);
    };
  
    const handleCancel = () => {
        setIsModalOpen(false);
    };

    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Row>
                <Input readOnly={true} value={config[configKey]} style={{ width: '80%' }} />
                {!readonly && <Button onClick={showModal} style={{ marginLeft: '1em' }}>Pick</Button> }
            </Row>
            <Modal title='Choose a file' open={isModalOpen} onOk={handleOk} okButtonProps={{ disabled: selectedPath === undefined }} 
                onCancel={handleCancel} destroyOnClose>
                <FileManager path={activePath} onPathChange={setActivePath} readonly={true} simple={true} backButton={false}
                    style={{height: '30em'}}
                    onSelectionChange={(files) => {
                        if (files && files.length > 0 && (!fileType || files[0].type === fileType)) {
                            setSelectedPath(getFullPath(activePath, files[0].name));
                        } else {
                            setSelectedPath(undefined);
                        }
                    }}
                />    
            </Modal>
        </ControlWrapper>
    );
};

export default FileControl;