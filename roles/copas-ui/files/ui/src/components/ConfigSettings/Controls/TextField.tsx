import React from 'react';
import { Input } from 'antd';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

export type TextFieldControlProps = BaseControlProps<string>

const TextFieldControl: React.FC<TextFieldControlProps> = (props) => {
    const { configKey, title, description, readonly = false, config, updateConfig } = props;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Input readOnly={readonly} value={config[configKey]} style={{width: '50%'}}
                onChange={(e) => {
                    if (!readonly) updateConfig(configKey, e.target.value);
                }}/>
        </ControlWrapper>
    );
};

export default TextFieldControl;