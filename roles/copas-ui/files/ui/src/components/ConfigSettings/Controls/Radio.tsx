import React from 'react';
import { Radio, Space } from 'antd';
import ControlWrapper from './ControlWrapper';
import { BaseControlProps } from '../../../core/config/template/model';

interface RadioOption {
    value: any,
    label: string
}

export interface RadioControlProps extends BaseControlProps<any> {
    options: RadioOption[]
}

const RadioControl: React.FC<RadioControlProps> = (props) => {
    const { configKey, title, description, readonly = false, config, updateConfig, options } = props;
    return (
        <ControlWrapper title={title} description={description} readonly={readonly}>
            <Radio.Group
                onChange={(e) => {
                    if (!readonly) updateConfig(configKey, e.target.value);
                }} 
                value={config[configKey]}>
                <Space direction="vertical">
                    {
                        options.map(o => (
                            <Radio key={o.value} value={o.value}>{o.label}</Radio>
                        ))
                    }
                </Space>
            </Radio.Group>
        </ControlWrapper>
    );
};

export default RadioControl;