import React, { ReactNode } from 'react';
import styles from './ControlWrapper.module.css';

interface ControlWrapperProps {
    title: string,
    description?: string,
    readonly?: boolean,
    children: ReactNode
}

const ControlWrapper: React.FC<ControlWrapperProps> = ({ title, description, readonly, children }) => {
    return (
        <div className={styles.controlVertical}>
            <strong>{title}</strong>
            { description && !readonly && <p>{ description }</p> }
            { children }
        </div>
    );
};

export default ControlWrapper;