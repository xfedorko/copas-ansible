import { faHouse } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext } from 'react';
import styles from './Pathbar.module.css';
import BackButton from '../../Buttons/BackButton';
import { FileManagerContext } from '../../../core/files/context';

function* takeUntil(s: string, arr: string[]) {
    for (const e of arr) {
        yield e;
        if (e === s) break;
    }
}

const pathDirs = (path: string) => path.split('/').filter(pd => pd.length !== 0);

interface PathbarProps {
    className?: string
    backButton?: boolean
}

const Pathbar: React.FC<PathbarProps> = ({ className, backButton = true }) => {
    const { path, onPathChange } = useContext(FileManagerContext);
    return (
        <div className={`${styles.container} ${className}`}>
            { backButton && <BackButton /> }
            <div className={styles.pathsection}>
                <FontAwesomeIcon className={styles.homeIcon} icon={faHouse} onClick={() => {
                    onPathChange('/');
                }} />
                {
                    pathDirs(path).map((pd,i) => (
                        <React.Fragment key={pd+i}>
                            <span className={styles.slash}>/</span>
                            <span className={styles.pathElement} onClick={() => {
                                onPathChange(`/${[...takeUntil(pd, pathDirs(path))].join('/')}`);
                            }}
                            >{pd}</span>
                        </React.Fragment>
                    ))
                }
            </div>
        </div>
    );
};

export default Pathbar;