import { faFolderPlus } from '@fortawesome/free-solid-svg-icons';
import React, { ChangeEvent, useContext, useState } from 'react';
import ToolbarButton from './ToolbarButton';
import { Input, Modal } from 'antd';
import { FileManagerContext } from '../../../../core/files/context';
import { useNewDirectory } from '../../../../core/files/hooks';
import { useTranslation } from 'react-i18next';

const NewDirButton: React.FC = () => {
    const { path } = useContext(FileManagerContext);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [directoryName, setDirectoryName] = useState('');
    const { t } = useTranslation();

    const { makeNewDirectory } = useNewDirectory();

    const showModal = () => setIsModalOpen(true);
    const handleClose = () => setIsModalOpen(false);
    const handleOk = () => {
        makeNewDirectory({
            path,
            name: directoryName
        });
        setIsModalOpen(false);
    };
    const handleInputChange = (e: ChangeEvent<HTMLInputElement>) => setDirectoryName(e.target.value);
    
    return (
        <>
            <ToolbarButton text={t('files.toolbar.buttons.newdir')}icon={faFolderPlus} onClick={showModal} />
            <Modal title={t('files.toolbar.modals.newdir.title')} open={isModalOpen} onOk={handleOk} onCancel={handleClose}>
                <Input onChange={handleInputChange} />
            </Modal>
        </>
    );
};

export default NewDirButton;
