import React from 'react';
import BackButton from './Buttons/BackButton';
import DeleteButton from './Buttons/DeleteButton';
import NewDirButton from './Buttons/NewDirButton';
import UploadButton from './Buttons/UploadButton';
import styles from './Toolbar.module.css';
import HiddenFileCheckbox from './HiddenFileCheckbox';
import FileDisplayType from './FileDisplayType';
import FileSearch from './FileSearch';

interface ToolbarProps {
    className?: string,
    readonly?: boolean,
    simple?: boolean
}

const Toolbar: React.FC<ToolbarProps> = ({ className, readonly = false, simple = false }) => {
    return (
        <div className={`${styles.container} ${className}`}>
            <div className={styles.buttonGroup}>
                <BackButton/>
                { readonly || <NewDirButton/> }
                { readonly || <UploadButton/> }
                { readonly || <DeleteButton/> }
            </div>
            <div className={styles.toggleGroup}>
                <HiddenFileCheckbox/>
                {!simple && <FileDisplayType/>}
            </div>
            <div className={styles.searchGroup}>
                <FileSearch/>
            </div>
        </div>
    );
};

export default Toolbar;