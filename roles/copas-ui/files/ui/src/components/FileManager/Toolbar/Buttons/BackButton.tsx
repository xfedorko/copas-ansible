import { faArrowLeftLong } from '@fortawesome/free-solid-svg-icons';
import React, { useContext } from 'react';
import ToolbarButton from './ToolbarButton';
import { FileManagerContext } from '../../../../core/files/context';
import { useTranslation } from 'react-i18next';


const BackButton: React.FC = () => {
    const { path, onPathChange } = useContext(FileManagerContext);
    const { t } = useTranslation();
    
    const handleOnClick = () => {        
        onPathChange(path.lastIndexOf('/') === 0 ? '/' : path.substring(0, path.lastIndexOf('/')));
    };

    return (
        <ToolbarButton text={t('files.toolbar.buttons.back')} icon={faArrowLeftLong} onClick={handleOnClick} disabled={path === '/'} />
    );
};

export default BackButton;
