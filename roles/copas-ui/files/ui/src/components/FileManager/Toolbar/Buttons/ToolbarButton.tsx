import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { MouseEventHandler } from 'react';
import styles from './ToolbarButton.module.css';

interface ToolbarButtonProps {
    text: string,
    icon: IconDefinition,
    onClick?: MouseEventHandler<HTMLDivElement>,
    disabled?: boolean,
}

const ToolbarButton: React.FC<ToolbarButtonProps> = ({ text, icon, onClick, disabled = false }) => {
    return (
        <div className={`${styles.container} ${disabled ? styles.containerDisabled : ''}`} onClick={onClick}>
            <FontAwesomeIcon className={styles.icon} icon={icon} size='2x'/>
            <span className={styles.text}>{text}</span>
        </div>
    );
};

export default ToolbarButton;
