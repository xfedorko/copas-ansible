import { Radio } from 'antd';
import React, { useContext } from 'react';
import { FileManagerContext } from '../../../core/files/context';
import { useTranslation } from 'react-i18next';

const FileDisplayType = () => {
    const { displayType, setDisplayType } = useContext(FileManagerContext);
    const { t } = useTranslation();
    return (
        <Radio.Group onChange={e => setDisplayType(e.target.value)} value={displayType}>
            <Radio value='grid'>{t('files.toolbar.display_mode.grid')}</Radio>
            <Radio value='list'>{t('files.toolbar.display_mode.list')}</Radio>
        </Radio.Group>
    );
};

export default FileDisplayType;