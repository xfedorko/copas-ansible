import { faTrash } from '@fortawesome/free-solid-svg-icons';
import React, { useContext, useEffect, useState } from 'react';
import ToolbarButton from './ToolbarButton';
import { Modal } from 'antd';
import { FileManagerContext } from '../../../../core/files/context';
import { useDeleteFiles } from '../../../../core/files/hooks';
import { useTranslation } from 'react-i18next';

const DeleteButton = () => {
    const { path, selectedFiles } = useContext(FileManagerContext);
    const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
    const { deleteFiles } = useDeleteFiles();
    const { t } = useTranslation();

    useEffect(() => {
        const onDeleteDown = (e: KeyboardEvent) => {
            if (e.key === 'Delete' && selectedFiles.length > 0) showModal();
        };
        document.addEventListener('keydown', onDeleteDown);
        return () => {
            document.removeEventListener('keydown', onDeleteDown);
        };
    }, [selectedFiles]);
  
    const showModal = () => {
        setIsDeleteModalOpen(true);
    };

    const handleOk = () => {
        deleteFiles({
            path,
            files: selectedFiles.map(f => f.name)
        });
        setIsDeleteModalOpen(false);
    };

    const handleCancel = () => {
        setIsDeleteModalOpen(false);
    };


    return (
        <>
            <ToolbarButton text={t('files.toolbar.buttons.delete')} icon={faTrash} disabled={selectedFiles.length === 0} onClick={showModal}/>
            <Modal title={`${t('files.toolbar.modals.delete.title')} (${selectedFiles.length}) `} 
                open={isDeleteModalOpen} 
                onOk={handleOk}
                onCancel={handleCancel}
            >
                <div style={{maxHeight: '20em', overflowY: 'scroll'}}>
                    {
                        selectedFiles.map(f => <p key={f.name}>{f.name}</p>)
                    }
                </div>
            </Modal>
        </>
    );
};

export default DeleteButton;