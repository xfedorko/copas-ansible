import React, { useContext } from 'react';
import { Input } from 'antd';
import { FileManagerContext } from '../../../core/files/context';
import { useTranslation } from 'react-i18next';

const { Search } = Input;

const FileSearch: React.FC = () => {
    const { addFilter } = useContext(FileManagerContext);
    const { t } = useTranslation();
    return (
        <Search placeholder={t('files.toolbar.search.placeholder') || undefined} onSearch={(val) => {
            addFilter({
                name: 'searchFilter',
                apply: (f) => f.name.toLowerCase().includes(val)
            });
        }} style={{ width: 200 }} />
    );
};

export default FileSearch;
