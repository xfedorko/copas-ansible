import { faFileArrowUp } from '@fortawesome/free-solid-svg-icons';
import React, { ChangeEvent, useContext } from 'react';
import ToolbarButton from './ToolbarButton';
import { FileManagerContext } from '../../../../core/files/context';
import { useTranslation } from 'react-i18next';

const UploadButton = () => {
    const { upload } = useContext(FileManagerContext);
    const { t } = useTranslation();

    return (
        <>
            <label htmlFor='fileInput'>
                <ToolbarButton text={t('files.toolbar.buttons.upload')} icon={faFileArrowUp} disabled={false} />
            </label>
            <input id='fileInput' type='file' multiple hidden onChange={(e: ChangeEvent<HTMLInputElement>) => {
                if (e.target.files !== null && e.target.files.length > 0) {
                    upload(e.target.files);
                }
            }}/>
        </>
    );
};

export default UploadButton;