import { Checkbox } from 'antd';
import React, { useContext } from 'react';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';
import { hiddenFileFilter } from '../../../core/files/filters';
import { FileManagerContext } from '../../../core/files/context';
import { useTranslation } from 'react-i18next';

const HiddenFileCheckbox = () => {
    const { addFilter, removeFilter } = useContext(FileManagerContext);
    const { t } = useTranslation();
    
    const onChange = (e: CheckboxChangeEvent) => {
        if (e.target.checked) {
            removeFilter(hiddenFileFilter.name);
        } else {
            addFilter(hiddenFileFilter);
        }
    };
    return (
        <Checkbox onChange={onChange}>{t('files.toolbar.hidden_files')}</Checkbox>
    );
};

export default HiddenFileCheckbox;