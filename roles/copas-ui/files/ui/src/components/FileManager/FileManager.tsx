import React, { HTMLProps, useEffect } from 'react';
import Contents from './Contents/Contents';
import styles from './FileManager.module.css';
import Pathbar from './Pathbar/Pathbar';
import Statusbar from './Statusbar/Statusbar';
import Toolbar from './Toolbar/Toolbar';
import { FileInfo } from '../../core/files/model';
import { usePath } from '../../core/files/hooks';
import { FileManagerContext } from '../../core/files/context';

interface FileManagerProps extends HTMLProps<HTMLDivElement> {
  path: string,
  onPathChange: (newPath: string) => void
  readonly?: boolean,
  onSelectionChange?: (files: FileInfo[]) => void,
  backButton?: boolean,
  simple?: boolean
}

const FileManager: React.FC<FileManagerProps> = ({ path, onPathChange, onSelectionChange, readonly = false, backButton = true, simple = false, className, ...rest }) => {
    const pathData = usePath(path);

    useEffect(() => {
        onSelectionChange?.(pathData.selectedFiles);
    }, [pathData.selectedFiles, onSelectionChange]);

    return (
        <FileManagerContext.Provider value={ {...pathData, path, onPathChange}}>
            <div className={`${styles.container} ${className}`} {...rest}>
                <Pathbar backButton={backButton}/>
                <Toolbar readonly={readonly} simple={simple}/>
                <Contents readonly={readonly}/>
                <Statusbar/>
            </div>
        </FileManagerContext.Provider>

    );
};

export default FileManager;