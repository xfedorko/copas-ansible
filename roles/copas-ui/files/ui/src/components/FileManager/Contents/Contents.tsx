import { faUpload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useContext, MouseEvent, useEffect, useRef } from 'react';
import styles from './Contents.module.css';
import { useDropFiles } from './hooks';
import { List } from 'antd';
import { bytesToString, formatDate, getFullPath } from '../../../utils';
import { SERVER_URL } from '../../../clients/url';
import { defaultClient } from '../../../clients/default';
import { toast } from 'react-toastify';
import { useQueryClient } from 'react-query';
import { AxiosError } from 'axios';
import { useWindowSize } from '../../../hooks';
import { fileColor, fileIcon } from '../../../core/files/utils';
import { FileManagerContext } from '../../../core/files/context';
import { FileInfo } from '../../../core/files/model';

const getColsNum = (readonly: boolean, windowWidth: number) => {
    if (!readonly) return windowWidth > 650 ? 8 : 4;
    if (windowWidth < 650) return 3;
    if (windowWidth < 1200) return 4;
    return 6;
};

interface ContentsProps {
    readonly?: boolean
}

const Contents: React.FC<ContentsProps> = ({ readonly = false }) => {
    const { path, onPathChange, upload, isLoading, filteredFiles, displayType, selectionMask, setSelectionMask, selectedFiles, clipboard, setClipboard } = useContext(FileManagerContext);
    const { drop, isDragOver } = useDropFiles((f) => upload(f));
    const lastSelectedFileRef = useRef<number>();
    const queryClient = useQueryClient();
    const { width } = useWindowSize();

    useEffect(() => {
        const onActionKey = (e: KeyboardEvent) => {
            if (e.key === 'a' && e.ctrlKey) {
                e.stopPropagation();
                e.preventDefault();
                setSelectionMask(selectionMask.map(() => true));
            }

            if ((e.key === 'c' || e.key === 'x') && e.ctrlKey) {
                if (selectedFiles.length > 0) {
                    e.stopPropagation();
                    e.preventDefault();
                    setClipboard(
                        {
                            action: e.key === 'c' ? 'copy' : 'move',
                            paths: selectedFiles.map(f => getFullPath(path, f.name))
                        }
                    );
                }
            }

            if (e.key === 'v' && e.ctrlKey) {
                e.stopPropagation();
                e.preventDefault();

                if (clipboard && clipboard.paths.length > 0) {
                    defaultClient
                        .post(`/files/${clipboard.action}?destination=${path}&${clipboard.paths.map(p => `path=${p}`).join('&')}`)
                        .then(() => {
                            queryClient.invalidateQueries(['path', path]);
                            clipboard.paths.forEach(p => {
                                queryClient.invalidateQueries(['path', p]);
                            });
                            setClipboard(undefined);
                        })
                        .catch((e: AxiosError<any>) => {
                            const error = e.response?.data?.error;
                            if (error) {
                                toast.error(`Failed to ${clipboard.action} files - ${error}`);
                            } else {
                                toast.error(`Failed to ${clipboard.action} files`);
                            }
                        });
                }                
            }
        };
        document.addEventListener('keydown', onActionKey);
        return () => document.removeEventListener('keydown', onActionKey);
    }, [selectionMask, setSelectionMask, selectedFiles, path, clipboard, setClipboard, queryClient]);


    const handleClickFileItem = (e: MouseEvent<HTMLElement>, fileIndex: number) => {
        e.stopPropagation();
        setSelectionMask(selectionMask.map((s, i) => {
            if (e.shiftKey && lastSelectedFileRef.current !== undefined) {
                return (lastSelectedFileRef.current <= i && i <= fileIndex)
                || (lastSelectedFileRef.current >= i && i >= fileIndex);
            }
            lastSelectedFileRef.current = fileIndex;
            if (e.ctrlKey) return i === fileIndex ? !s : s;
            return i === fileIndex;
        }));

    };

    const handleClickBackground = () => setSelectionMask(selectionMask.map(() => false));

    const handleDoubleClickFileItem = ({ type, name }: FileInfo) => {
        if (type !== 'dir') {
            window.open(`${SERVER_URL}/show${path}/${name}`);
        } else {
            onPathChange(path !== '/' ? `${path}/${name}` : `/${name}`);
        }
    };

    const typeClass = displayType === 'grid' ? styles.containerGrid : styles.containerList;
    const dragClass = isDragOver ? styles.dragOver : '';

    return (
        <div ref={!readonly ? drop : undefined} onClick={handleClickBackground}
            className={`${styles.container} ${typeClass} ${dragClass}`}>
            {
                !readonly && isDragOver ? (
                    <FontAwesomeIcon icon={faUpload} size="10x"/>
                ) : (
                    <List 
                        grid={ displayType === 'grid' ? {gutter: 12, column: getColsNum(readonly, width)} : undefined }
                        dataSource={filteredFiles}
                        className={styles['container-list']}
                        size='default'
                        loading={isLoading}
                        renderItem={(fileItem, i) => {
                            if (displayType === 'grid') return (
                                <List.Item>
                                    <div
                                        className={`${styles['file-item-grid']} ${selectionMask[i] && styles['file-selected']}`} 
                                        onClick={(e) => handleClickFileItem(e, i)} 
                                        onDoubleClick={() => handleDoubleClickFileItem(fileItem)}>
                                        <FontAwesomeIcon
                                            icon={fileIcon(fileItem.type)} 
                                            color={fileColor(fileItem.type)}
                                            size='3x'/>
                                        <span className={styles['file-item-name']}>{fileItem.name}</span>
                                    </div>
                                </List.Item>
                            );
                            return (<List.Item style={{padding: '0'}}>
                                <div
                                    className={`${styles['file-item-list']} ${selectionMask[i] && styles['file-selected']}`} 
                                    onClick={(e) => handleClickFileItem(e, i)} 
                                    onDoubleClick={() => handleDoubleClickFileItem(fileItem)}>
                                    <FontAwesomeIcon
                                        className={styles.listType}
                                        icon={fileIcon(fileItem.type)} 
                                        color={fileColor(fileItem.type)}
                                        size='1x'/>
                                    <span className={styles.listName}>{fileItem.name}</span>
                                    <span className={styles.listSize}>{fileItem.type === 'file' ? bytesToString(fileItem.size) : ''}</span>
                                    <span className={styles.listModified}>{formatDate(new Date(fileItem.modified * 1000))}</span>
                                </div>
                            </List.Item>
                            );
                        }}/>
                )
            }
        </div>
    );
};

export default Contents;