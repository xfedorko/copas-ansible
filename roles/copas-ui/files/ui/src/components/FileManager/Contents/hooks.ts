import { useEffect, useRef, useState } from 'react';

export const useDropFiles = (handleUpload: (files: FileList) => void) => {
    const [isDragOver, setIsDragOver] = useState(false);
    const drop = useRef<HTMLDivElement>(null);
    useEffect(() => {
        const handleDragOver = (e: DragEvent) => {
            e.preventDefault();
            e.stopPropagation();
        };

        const handleDrop = (e: DragEvent) => {
            e.preventDefault();
            e.stopPropagation();
            setIsDragOver(false);
            if (e.dataTransfer === null) return;
            const {files} = e.dataTransfer;
            if (files && files.length) {
                handleUpload(files);
            }
        };
        const handleDragEnter = (e: DragEvent) => {
            e.preventDefault();
            e.stopPropagation();
            setIsDragOver(true);
        };
        const handleDragLeave = (e: DragEvent) => {
            e.preventDefault();
            e.stopPropagation();
            setIsDragOver(false);
        };

        const current = drop.current;
        current?.addEventListener('dragover', handleDragOver);
        current?.addEventListener('dragenter', handleDragEnter);
        current?.addEventListener('dragleave', handleDragLeave);
        current?.addEventListener('drop', handleDrop);
        return () => {
            current?.removeEventListener('dragover', handleDragOver);
            current?.removeEventListener('dragenter', handleDragEnter);
            current?.removeEventListener('dragleave', handleDragLeave);   
            current?.removeEventListener('drop', handleDrop);
        };
    }, [handleUpload]);
    return { drop, isDragOver };  
};
