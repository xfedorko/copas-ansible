import React, { useContext } from 'react';
import styles from './Statusbar.module.css';
import { FileManagerContext } from '../../../core/files/context';
import { useTranslation } from 'react-i18next';
import { Spin } from 'antd';

export interface StatusbarProps {
    className?: string,
}

const Statusbar: React.FC<StatusbarProps> = ({ className }) => {
    const { isLoading, filteredFiles, selectedFiles } = useContext(FileManagerContext);
    const { t } = useTranslation();
    
    return (
        <div className={`${styles.container} ${className}`}>
            {isLoading ? (
                <Spin size='small' />
            ) : (
                <>
                    {selectedFiles.length > 0 && <span>{selectedFiles.length} {t('files.statusbar.selected')}</span>}
                    <span>{filteredFiles.length} {t('files.statusbar.total')}</span>
                </>

            )}
        </div>
    );
};

export default Statusbar;