import { SizeProp } from '@fortawesome/fontawesome-svg-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { HTMLProps } from 'react';
import styles from './Loading.module.css';

export interface LoadingProps extends HTMLProps<HTMLDivElement> {
 iconSize?: SizeProp
}

const Loading: React.FC<LoadingProps> = ({ iconSize = '1x', ...rest }) => {
    return (
        <div className={styles.container} {...rest}>
            <FontAwesomeIcon icon={faSpinner} size={iconSize} pulse/>
        </div>
    );
};

export default Loading;