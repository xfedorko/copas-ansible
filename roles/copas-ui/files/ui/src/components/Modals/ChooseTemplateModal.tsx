import { Button, Modal, Select } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { useModuleInfoContext } from '../../core/info/hooks';
import { ImportContext } from '../../core/import/context';
import { useNavigate } from 'react-router-dom';
import { useGenerateConfigContents, useSelectedFilePaths } from '../../core/import/hooks';
import { useTranslation } from 'react-i18next';

interface ChooseTemplateModalProps {
    handleConfirm: () => void,
    handleCancel: () => void,
    open: boolean
}

const ChooseTemplateModal: React.FC<ChooseTemplateModalProps> = ({ handleConfirm, handleCancel, open }) => {
    const { setConfigData } = useContext(ImportContext);
    const { files } = useSelectedFilePaths();
    const { configTemplates: staticTemplates } = useModuleInfoContext();
    const [selectedTemplate, setSelectedTemplate] = useState(staticTemplates[0]);
    const navigate = useNavigate();
    const { isLoading, refetch: generateConfigContents } = useGenerateConfigContents(selectedTemplate.key, files[0] || '');
    const { t } = useTranslation();

    useEffect(() => {
        if (staticTemplates.length === 1 && open) {
            setConfigData({
                template: staticTemplates[0]
            });
            navigate('/import/create');
        }
    }, [staticTemplates, navigate, setConfigData, open]);

    const onOk = () => {
        handleConfirm();
        setConfigData({ template: selectedTemplate });
        navigate('/import/create');
    };

    return (
        <Modal title={t('other.modals.choose_template.title')}
            open={open}
            onCancel={handleCancel} 
            onOk={onOk}
            okText={t('words.ok')}
            cancelText={t('words.cancel')}>
            <Select
                defaultValue={selectedTemplate.key}
                style={{width: '20em'}}
                onChange={(val => {
                    const selected = staticTemplates.find(t => t.key === val);
                    if (selected) {
                        setSelectedTemplate(selected);
                    }                    
                })}
                options={staticTemplates.map(t => ({ value: t.key, label: t.name }))}
            />
            {
                selectedTemplate.generation && (
                    <Button style={{float: 'right' }} 
                        type='primary' loading={isLoading} shape='round' 
                        disabled={files.length === 0}
                        onClick={() => generateConfigContents()}>{t('other.modals.choose_template.generate')}</Button>
                )
            }
        </Modal>
    );
};

export default ChooseTemplateModal;