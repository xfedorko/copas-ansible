import { Modal, Select } from 'antd';
import React, { useState } from 'react';
import { useModuleInfoContext } from '../../core/info/hooks';
import { useTranslation } from 'react-i18next';

interface ChooseStaticTemplateModalProps {
    handleConfirm: (templateKey: string) => void,
    handleCancel: () => void,
    open: boolean,
}

const ChooseStaticTemplateModal: React.FC<ChooseStaticTemplateModalProps> = ({ handleConfirm, handleCancel, open }) => {
    const moduleInfo = useModuleInfoContext();
    const [selectedTemplateKey, setSelectedTemplateKey] = useState(moduleInfo.configTemplates[0].key);
    const { t } = useTranslation();
    
    return (
        <Modal 
            title={t('other.modals.choose_template.title')} 
            open={open} 
            onCancel={handleCancel} 
            onOk={() =>handleConfirm(selectedTemplateKey)}
            okText={t('words.ok')}
            cancelText={t('words.cancel')}>
            <Select
                defaultValue={selectedTemplateKey}
                style={{width: '20em'}}
                onChange={(val => setSelectedTemplateKey(val))}
                options={moduleInfo.configTemplates.map(t => ({ value: t.key, label: t.name }))}
            />
        </Modal>
    );
};

export default ChooseStaticTemplateModal;