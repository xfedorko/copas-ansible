const CracoLessPlugin = require('craco-less');

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: { '@primary-color': '#e47878' },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
    eslint: null
};