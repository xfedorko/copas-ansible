# CopAS UI - the client Single Page Application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). A [Typescript](https://www.typescriptlang.org/) is used as a programming language for the project. [Ant Desgin](https://ant.design/) is used as the main component library to simplify building the interface. 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### `npm run lint`
Runs the ESLint tool on the `src` directory of a project helping identify and report issues in code that may cause bugs or reduce the maintainability of the codebase.

### `npm run lint-fix`
Same as the `npm run lint` except it also it will also appy the suggestions directly to the code.


## Project structure
The *src* directory contains all the source code for the UI.

- **clients**: Place for the HTTP clients used for communicating with the backend are placed.
- **components**: Place for most React components.
- **core**: Most of the core logic (models, hooks, api calls) is placed here. The subdirectories are structure by the functionality they provide in the UI.
- **pages**: Place for the Page components. The UI uses [React Router](https://reactrouter.com/en/main) to handle client routing.
- **images**: Some important images like the CopAS logo are placed here.
- **App.tsx**: The main App component, where the router is placed.
- **i18n.ts**: Internationalization configuration implemented using [i18next](https://www.i18next.com/).
