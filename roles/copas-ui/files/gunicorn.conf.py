workers = '2'
bind = '0.0.0.0:8080'
timeout = '10800'
accesslog = '/var/log/copas/ui-access.log'
errorlog = '/var/log/copas/ui-error.log'